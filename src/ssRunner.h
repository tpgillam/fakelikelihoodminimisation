#ifndef SS_RUNNER_H
#define SS_RUNNER_H

#include "ComplexEfficiencyInfo.h"

void printFakeMLE(const ComplexEfficiencyInfo& efficiencyInfo, std::istream& stream);

#endif // SS_RUNNER_H
