#ifndef BASIC_FAKE_NLL_H
#define BASIC_FAKE_NLL_H

// Notes to self
// "ETight" / "ELoose" refers to *event level* looseness
//
#include "Minuit2/FCNGradientBase.h"

#include <cmath>

#include "BackgroundComponent.h"
#include "Components.h"
#include "Configs.h"

#include <boost/random/mersenne_twister.hpp>

namespace LikelihoodComputationMode {
  enum e {
    FULL, TIGHT_LOOSE, TIGHT, FULL_NOTIGHT
  };
}


// class BasicFakeNLL : public ROOT::Minuit2::FCNGradientBase { 
class BasicFakeNLL : public ROOT::Minuit2::FCNBase { 
  public: 
    BasicFakeNLL(unsigned int nOmegaHat, unsigned int nLeptons);
    BasicFakeNLL(unsigned int nOmegaHat, unsigned int nLeptons, unsigned int nRealEff, unsigned int nFakeEff);
    void initialise(unsigned int nOmegaHat, unsigned int nLeptons, unsigned int nRealEff, unsigned int nFakeEff);

    ~BasicFakeNLL();

    inline virtual bool CheckGradient() const { return false; }

    inline void likelihoodComputationMode(LikelihoodComputationMode::e computationMode) { m_likelihoodComputationMode = computationMode; }

    void addComponent(ComponentFactoryPtr factory);

    inline unsigned int nComponents() const { return m_components.size(); }
    inline BackgroundComponent *componentAtIndex(unsigned int index) { return m_components[index]; }
    inline const BackgroundComponent *componentAtIndex(unsigned int index) const { return m_components[index]; }

    void addEvent(const std::vector<unsigned int> &omegaHat,
        const std::vector<TLConfig::e> &tlConfig);

    inline unsigned int nObsTight() const { return nObsForEventTL(TLConfig::TIGHT); }
    inline unsigned int nObsTightLoose() const { return nObsForEventTL(TLConfig::LOOSE); }

    void bindCategoriesToRealEfficiencies(const std::vector<unsigned int>& categoryToRealEff);
    void bindCategoriesToFakeEfficiencies(const std::vector<unsigned int>& categoryToFakeEff);

    // TODO expand to accept covariance (or at least correlated uncertainties)
    void setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);
    void setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);

    virtual double operator() (const std::vector<double> &x) const;
    void setParameters(const std::vector<double> & x, bool computeGradients=false) const;
    double valueAtCurrentPosition() const;
    double valueForPoissonTerms() const;
    virtual std::vector<double> Gradient(const std::vector<double> &params) const;
    double Up() const { return 0.5; }

    inline unsigned int nQ() const { return m_nQ; }

    void printObsN() const;

    inline const std::vector<double> &ratesByQ() const { return m_ratesByQ; }
    double rateTight() const;
    double rateLoose() const;

    void throwAndStoreToys(const std::vector<double> &parameters);
    void storeCurrentParametersAsAsimov();

    inline void enableAsimovMode() { m_enableAsimovMode = true; }
    inline void disableAsimovMode() { m_enableAsimovMode = false; }

    inline std::vector<double> asimovNObsByQ() const { return m_asimovNObsByQ; } 
    inline double asimovNObsLoose() const { return m_asimovNObsLoose; } 
    inline double asimovNObsTight() const { return m_asimovNObsTight; } 

    void setAsimovObsNByQ(const std::vector<double> &asimovNObsByQ);
    void setAsimovObsNLoose(double asimovNObsLoose);
    void setAsimovObsNTight(double asimovNObsTight);

    unsigned int nRealEff() const { return m_epsr.size(); }
    unsigned int nFakeEff() const { return m_epsf.size(); }


  private:
    void computeQIsTightList();
    double poissonNLL(double nObs, double rate) const;
    std::vector<double> poissonNLLGradient(double nObs, double rate, const std::vector<double> &rateGradient) const;
    unsigned int nObsForEventTL(TLConfig::e tlConfig) const;
    unsigned int qForConfig(const std::vector<unsigned int> &omegaHat,
        const std::vector<TLConfig::e> &tlConfig);
    void extractParameters(const std::vector<double> &x) const;
    unsigned int extractEfficiencies(const std::vector<double> &x) const;
    void computeRhos(bool computeGradients=false) const;

    double nObsForQMaybeAsimov(unsigned int q) const;
    double nObsTightMaybeAsimov() const;
    double nObsLooseMaybeAsimov() const;
    double rateForQ(unsigned int q) const;
    std::vector<double> rateForQGradient(unsigned int q) const;
    std::vector<double> rateTightGradient() const;
    std::vector<double> rateLooseGradient() const;


  private: 
    mutable unsigned int m_nParameters;
    LikelihoodComputationMode::e m_likelihoodComputationMode;
    unsigned int m_nOmegaHat;
    unsigned int m_nLeptons;
    unsigned int m_nQ;

    std::vector<double> m_nObsByQ;
    std::vector<double> m_asimovNObsByQ;
    mutable std::vector<double> m_ratesByQ;
    bool m_enableAsimovMode;

    mutable std::vector<bool> m_qIsTight;

    mutable std::vector<double> m_epsf;
    mutable std::vector<double> m_epsr;

    bool m_usingEpsfPriors;
    std::vector<double> m_epsfMeans;
    std::vector<double> m_epsfUncs;

    bool m_usingEpsrPriors;
    std::vector<double> m_epsrMeans;
    std::vector<double> m_epsrUncs;

    mutable std::vector<BackgroundComponent*> m_components;

    double m_nObsTight;
    double m_nObsLoose;
    double m_asimovNObsTight;
    double m_asimovNObsLoose;

    boost::random::mt19937 m_rng;

    std::vector<unsigned int> m_categoryToRealEff;
    std::vector<unsigned int> m_categoryToFakeEff;
};

struct NLoptData {
  std::vector<double> allParams;
  std::vector<unsigned int> freeParamIndices;
  
  BasicFakeNLL *basicFakeNLL;
};

extern unsigned int nBasicFakeNLLCalls;
double nloptBasicFakeNLL(const std::vector<double> &x, std::vector<double> &grad, void *data);

#endif // BASIC_FAKE_NLL_H
