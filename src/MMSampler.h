#ifndef MM_SAMPLER_H
#define MM_SAMPLER_H

#include "ComplexEfficiencyInfo.h"
#include "Configs.h"
#include "MatrixMethod.h"

#include <iostream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>

class MMSampler {
  public:
    MMSampler(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);
    ~MMSampler();

    void grabObservedEvents(std::istream &stream = std::cin);

    // Draw the specified number of samples and print output
    // If nSamples == -1, keep going ad infinitum
    void drawSamples(int nSamples = -1);


  private:
    // Delete any previously allocated distribution objects
    void cleanDistributions();

    // Precompute lookups for indices to lepton config vectors
    void computeLookups();

    // Initialise the generators that will produce random numbers
    void initialiseRandomGenerators();
    
    // Store a random draw from the given prior on the efficiencies. Return the
    // weight corresponding to how many tries were made for each efficiency
    double drawEfficiencies();

    // Store a random draw for the given category weights -- keep trying
    // as much is reasonable until a valid combination is found.
    // If it absolutely cannot be found (e.g. due to horrid efficiency choices),
    // return false, else return true.
    //
    // Place the number of attempts made in nAttempts
    bool drawCategoryRates(unsigned int ohIndex, unsigned int* nAttempts);

    // Returns true if the rates in m_rates are consistent with the 
    // pointed to by ohIndex, otherwise returns false.
    bool ratesSelfConsistent(unsigned int ohIndex);

    // Compute the inverse factor involved in the matrix method
    float inverseFactor(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig, 
        const std::vector<double>& effReals,
        const std::vector<double>& effFakes
        ) const;

    // Compute the rate of tight & fake event using the most
    // recently drawn category rates
    double computeFakeTightRate(unsigned int ohIndex);


  private:
    unsigned int m_nLeptons;
    GeneralisedMatrixMethod::MatrixMethod m_matrixMethod;
    const ComplexEfficiencyInfo& m_efficiencyInfo;

    // The number of event categories, just by lepton category
    unsigned int m_nEventCategories;

    // The number of tight / loose categories
    unsigned int m_nTLConfigs;

    // Observed event counts, indexed event category, then tight/loose category
    std::vector<std::vector<int> > m_nObs;

    // For a given 1st index, specify the corresponding lepton category config
    std::vector<OHConfigVector> m_ohConfigs;

    // For a given 2nd index, specify the corresponding tl config
    std::vector<TLConfigVector> m_tlConfigs;

    // Random number generator
    boost::random::mt19937 m_rng;

    // Efficiency distributions
    // TODO no correlation in here at the moment:
    //
    // In general, draw a vector x of standard normal variates, then compute
    // the draw we want using
    //     mu +  A * x
    // where A^T A = \Sigma [covariance matrix]
    // e.g. A is cholesky decomposition of the covariance matrix
    std::vector<boost::random::normal_distribution<> *> m_realEffDists;
    std::vector<boost::random::normal_distribution<> *> m_fakeEffDists;

    // Temporary efficiency storage
    std::vector<double> m_realEffs;
    std::vector<double> m_fakeEffs;

    // Rate distributions -- same indexing as for m_nObs
    std::vector<std::vector<boost::random::gamma_distribution<> *> > m_rateDists;

    // Temporary rate storage for given event category, indexed over TL config
    std::vector<double> m_rates;
};


#endif // MM_SAMPLER_H
