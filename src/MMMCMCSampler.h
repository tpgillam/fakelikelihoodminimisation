#ifndef MM_MCMC_SAMPLER_H
#define MM_MCMC_SAMPLER_H

#include "ComplexEfficiencyInfo.h"
#include "Configs.h"

#include <iostream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>

// Produce draws from the marginal likelihood of the fake-and-tight event rate,
// using an importance sampling methodology
class MMMCMCSampler {
  public:
    MMMCMCSampler(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);
    ~MMMCMCSampler();

    void grabObservedEvents(std::istream &stream = std::cin);

    // Draw the specified number of samples and print output
    // If nSamples == -1, keep going ad infinitum
    void drawSamples(unsigned int nSamples = -1);


  private:
    // Delete any previously allocated distribution objects
    void cleanDistributions();

    // Precompute lookups for indices to lepton config vectors
    void computeLookups();

    // Initialise the generators that will produce random numbers
    void initialiseRandomGenerators();
    
    // Store a random draw from the given prior on the efficiencies. Return the
    // log of the weight corresponding to how many tries were made for each
    // efficiency
    double drawEfficiencies();

    // Store an importance-sampled draw on the real & fake index rates. Since we
    // are not drawing from the *actual* distribution, also return the
    // log of the appropriate importance sampling weighting factor
    double drawCategoryRatesRF(unsigned int ohIndex);

    // Update the "current efficiencies" for this event category
    void updateCurrentEfficiencies(unsigned int ohIndex);

    // Update the rates in m_ratesTL given the values currently in m_ratesRF
    void updateRatesTL();

    // Return the (log of the) importance sampling reweighting factor for the
    // rates currently stored in m_ratesRF and m_ratesTL
    double samplingLogWeightForCurrentRates(unsigned int ohIndex) const;

    // Compute the matrix element involved in the matrix method
    double matrixElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;

    // Compute the rate of tight & fake event for this category using the most
    // recently drawn category rates and current efficiencies
    double computeFakeTightRate();

    // Return true if we should regard this configuration as representing
    // a fake event
    bool isThisAFakeEvent(const RFConfigVector& rfConfig);
    
    // Return true if we should regard this configuration as representing
    // a tight event
    bool isThisATightEvent(const TLConfigVector& tlConfig);


  private:
    unsigned int m_nLeptons;
    const ComplexEfficiencyInfo& m_efficiencyInfo;

    // The number of event categories, just by lepton category
    unsigned int m_nEventCategories;

    // The number of tight / loose combinations, and real / fake combinations
    unsigned int m_nTLConfigs;
    unsigned int m_nRFConfigs;

    // Observed event counts, indexed event category, then tight/loose category
    std::vector<std::vector<int> > m_nObs;

    // For a given 1st index, specify the corresponding lepton category config
    std::vector<OHConfigVector> m_ohConfigs;

    // For a given 2nd index, specify the corresponding tl config
    std::vector<TLConfigVector> m_tlConfigs;
    
    // For a given 2nd index, specify the corresponding rf config
    std::vector<RFConfigVector> m_rfConfigs;

    // Random number generator
    boost::random::mt19937 m_rng;

    // Efficiency distributions
    // TODO no correlation in here at the moment:
    //
    // In general, draw a vector x of standard normal variates, then compute
    // the draw we want using
    //     mu +  A * x
    // where A^T A = \Sigma [covariance matrix]
    // e.g. A is cholesky decomposition of the covariance matrix
    std::vector<boost::random::normal_distribution<> *> m_realEffDists;
    std::vector<boost::random::normal_distribution<> *> m_fakeEffDists;

    // The proposed efficiencies
    std::vector<double> m_realEffs;
    std::vector<double> m_fakeEffs;

    // Efficiency values index over leptons for the current event category being
    // considered
    std::vector<double> m_lepRealEffs;
    std::vector<double> m_lepFakeEffs;

    // Rate distributions -- same indexing as for m_nObs
    std::vector<std::vector<boost::random::gamma_distribution<> *> > m_rateDists;

    // Temporary rate storage for given event category, indexed over *RF*
    // config. These are MCMC proposed values
    std::vector<double> m_ratesRF;

    // Temporary rate storage for given event category, indexed over *TL*
    // config. These will always be the values computed from m_ratesRF
    std::vector<double> m_ratesTL;

    // Store the log of the probability (up to a constant) of the previous draw
    // under the distributions
    double prevLogProbability;

    // Store the previous fake and tight rate in case we need to store it
    double prevFakeTightRate;
};


#endif // MM_MCMC_SAMPLER_H
