#ifndef CLS_CALCULATOR_H
#define CLS_CALCULATOR_H

#include "MiniMinimum.h"
#include <cmath>
#include <memory>
#include <utility>

class CLsCalculator {
  public:
    CLsCalculator();
    ~CLsCalculator() { }

    virtual double q0() = 0;
    virtual double qMu(double mu, bool asimovMode = false, 
        std::shared_ptr<MiniMinimum> startingPoint=0) = 0;
    virtual unsigned int nObsTot() const = 0;
    virtual unsigned int nObsTight() const = 0;
    virtual double estimateSigmaFromAsimovDataset(double mu, double muPrime) = 0;
    virtual double muHat() const = 0;

    virtual void startImportanceSamplingRun() = 0;

    double approxDiscoverySig();

    double approxP0();
    double approxPMu(double mu, double muPrime, bool useCachedSqrtQMu=false);

    double approxCLs(double mu);
    double approxCLsb(double mu);

    double approxCLsUL(double percentage, double precision = 0.01);
    double approxCLsbUL(double percentage, double precision = 0.01);

    double approx95PercentCLsUL(double precision = 0.01);
    double approx95PercentCLsbUL(double precision = 0.01);

    virtual double toysPMu(double mu, double muPrime, unsigned int numToys) = 0;
    virtual double toysPMuImportanceSampling(double mu, double muPrime, unsigned int numToys) = 0;

    double toysCLs(double mu);
    double toysCLsb(double mu);
    double toysCLsUL(double percentage, unsigned int numToys = 5000, double precision = 0.01);
    double toysCLsbUL(double percentage, unsigned int numToys = 5000, double precision = 0.01);
    double toys95PercentCLsUL(unsigned int numToys = 5000, double precision = 0.01);
    double toys95PercentCLsbUL(unsigned int numToys = 5000, double precision = 0.01);
    inline void setNumToys(double numToys) { m_numToys = numToys; }

    void clearCLsComputationCaches();
    inline const std::vector<double> &allXVals() const { return m_allXVals; }
    inline const std::vector<double> &allYVals() const { return m_allYVals; }

  private:
    class ULEval {
      public:
        ULEval(CLsCalculator *clsCalc, double (CLsCalculator::*objective)(double), double target)
          : m_clsCalc(clsCalc), m_objective(objective), m_target(target) { }
        double operator()(double mu);
        double evaluateWithoutSubtraction(double mu);

        inline double target() const { return m_target; }

      private:
        CLsCalculator *m_clsCalc;
        double (CLsCalculator::*m_objective)(double);
        double m_target;
    };


    class ULTol {
      public:
        ULTol(double precision)
          : m_precision(precision) { }
        inline double operator()(double a, double b) const {
          return (std::abs(a-b) < m_precision);
        }
      private:
        double m_precision;
    };

  private:
    double findReasonableBracketMax(ULEval &eval, double startingValue);
    double findUpperLimit(ULEval &eval, double precision);

    double maxMuValue() const;
    void resetNewtonRaphsonGillamCache();
    double newtonRaphsonGillam(ULEval &eval, double startingPoint, 
        bool clsMode=false, bool parentRun=true);
    void outlierCorrectedLinearFit(std::vector<double> &xVals, std::vector<double> &yVals,
        double &slope, double &intercept, bool clsMode);
    void linearFit(const std::vector<double> &xVals, const std::vector<double> &yVals,
        double &slope, double &intercept);
    double nextStartingPosition(double proposedStartingPosition) const;

  private:
    double m_sqrtQMuCache;
    unsigned int m_numToys;

    std::vector<double> m_xValsTooSmall;
    std::vector<double> m_xValsTooBig;
    std::vector<double> m_allXVals;
    std::vector<double> m_allYVals;
    mutable bool m_justHitLowerBound;
    unsigned int m_numNewtonRaphsonIterations;

    std::vector<std::pair<double, double>> m_toysPMuMuCache;
    double m_approxCLsULCache;
    double m_approxCLsbULCache;
    double m_toysCLsbULCache;
};

#endif // CLS_CALCULATOR_H
