#include "llhMinimising.h"
#include "BasicFakeNLL.h"
#include "utils.h"
#include "Minimiser.h"
#include "MatrixMethodApplicator.h"
#include "CLsCalculatorFakeNLL.h"
#include "Components.h"

#include "Minuit2/MnMigrad.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnPrint.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

#include "TGraph.h"
#include "TAxis.h"
#include "TGraph2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TError.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/replace.hpp>

void makeGraphs(const std::shared_ptr<MiniMinimum> &minimum, Minimiser &minimiser, const std::string &dirname)
{
  const ROOT::Minuit2::MnUserParameters &userPars = minimum->UserParameters();
  unsigned int nPars = userPars.Parameters().size();
  std::vector<double> defaultParams;
  std::vector<Param> params;
  for (unsigned int i = 0; i < nPars; ++i) {
    std::string name(userPars.GetName(i));
    std::string displayName(name);
    boost::algorithm::replace_all(displayName, "_", "");
    boost::algorithm::replace_all(displayName, "sigmaT", "#sigma_{T");
    boost::algorithm::replace_all(displayName, "pi", "#pi_{");
    boost::algorithm::replace_all(displayName, "beta", "#beta_{");
    boost::algorithm::replace_all(displayName, "eps", "#varepsilon_{");
    displayName += "}";
    bool isUnbounded = boost::starts_with(name, "sigmaT");
    defaultParams.push_back(userPars.Value(i));
    if (isUnbounded) {
      double upperBound = 1.2 * minimiser.nObsTot();
      params.push_back(Param(i, name, displayName, upperBound));
    } else {
      params.push_back(Param(i, name, displayName));
    }
  }

  if (dirname != "") {
    boost::filesystem::create_directory(dirname);
  }

  TCanvas *canvas = new TCanvas();
  canvas->SetCanvasSize(1000, 1000);
  BasicFakeNLL *fcn = minimiser.getFcn();
  for (auto it1 = params.begin(); it1 != params.end(); ++it1) {
    draw1DComparison(fcn, defaultParams, *it1, 100.0, canvas, dirname);
    for (auto it2 = params.begin(); it2 != params.end(); ++it2) {
      if (it1->index == it2->index) continue;
        draw2DComparison(fcn, defaultParams, *it1, *it2, 100.0, canvas, dirname);
    }
  }
  delete canvas;
}

void writeSummaryFile(const std::shared_ptr<MiniMinimum> &minimum, Minimiser &minimiser, const std::string &dirname,
    const std::string &filename)
{
  boost::filesystem::create_directory(dirname);
  std::ofstream fileStream("./"+dirname+"/"+filename);
  minimiser.printMinimum(minimum, fileStream);
  fileStream.flush();
  fileStream.close();
}

void draw1DComparison(const BasicFakeNLL *fcn, const std::vector<double>
    &defaultParams, const Param &var1,
    double maxNLL, TCanvas *canvas, const std::string &dirname, const std::string &outputName)
{
  std::vector<double> params(defaultParams);

  const unsigned int nPointsOnSide(100);
  const unsigned int nPoints(nPointsOnSide);

  bool usingCustomCanvas(false);
  if (!canvas) {
    canvas = new TCanvas();
    usingCustomCanvas = true;
    canvas->SetCanvasSize(1000, 1000);
  }

  double *xArr = new double[nPoints];
  double *zArr = new double[nPoints];
  double x, z;
  unsigned int i(0);
  double maxz(0.0);
  std::vector<unsigned int> iForMax;
  std::vector<double> xForMax;
  for (unsigned int ix = 0; ix < nPointsOnSide; ++ix) {
    x = ix * var1.maxValue / (double) nPointsOnSide;
    params[var1.index] = x;
    z = (*fcn)(params);

    if (maxNLL > 0 && z > maxNLL) {
      z = maxNLL;
    }

    if (isinf(z)) {
      iForMax.push_back(i);
      xForMax.push_back(x);
      ++i;
      continue;
    } else if (z > maxz) {
      maxz = z;
    }

    // std::cout << i << ": " << x << " " << y << " " << z << std::endl;
    xArr[i] = x;
    zArr[i] = z;
    ++i;
  }

  for (unsigned int i = 0; i < iForMax.size(); ++i) {
    unsigned int idx = iForMax[i];
    xArr[idx] = xForMax[i];
    zArr[idx] = maxz;
    std::cout << "Filling max at: " << xArr[idx] << std::endl;
  }

  std::ostringstream nameStream, titleStream;
  nameStream << "grph_" << var1.name;
  titleStream << "NLL;" << var1.label << ";-log(L)";
  std::string name = nameStream.str();
  std::string title = titleStream.str();
  TGraph *graph = new TGraph(nPoints, xArr, zArr);
  graph->SetName(name.c_str());
  graph->SetTitle(title.c_str());
  graph->Draw("acp");
  graph->GetXaxis()->SetRangeUser(0.0, var1.maxValue);
  graph->Draw("acp");

  std::ostringstream outputPathStream;
  if (dirname != "") {
    outputPathStream << dirname;
  } else {
    outputPathStream << "output";
  }
  outputPathStream << "/";
  if (outputName == "") {
    outputPathStream << nameStream.str() << ".png";
  } else {
    outputPathStream << outputName;
  }
  canvas->SaveAs(outputPathStream.str().c_str());

  delete [] xArr;
  delete [] zArr;
  delete graph;
  if (usingCustomCanvas) delete canvas;
}

void draw2DComparison(const BasicFakeNLL *fcn, const std::vector<double>
    &defaultParams, const Param &var1, const Param &var2,
    double maxNLL, TCanvas *canvas, const std::string &dirname, const std::string &outputName)
{
  std::vector<double> params(defaultParams);

  const unsigned int nPointsOnSide(30);
  const unsigned int nPoints(nPointsOnSide*nPointsOnSide);

  bool usingCustomCanvas(false);
  if (!canvas) {
    canvas = new TCanvas();
    usingCustomCanvas = true;
    canvas->SetCanvasSize(1000, 1000);
  }

  double *xArr = new double[nPoints];
  double *yArr = new double[nPoints];
  double *zArr = new double[nPoints];
  double x, y, z;
  unsigned int i(0);
  double maxz(0.0);
  std::vector<unsigned int> iForMax;
  std::vector<double> xForMax;
  std::vector<double> yForMax;
  for (unsigned int ix = 0; ix < nPointsOnSide; ++ix) {
    for (unsigned int iy = 0; iy < nPointsOnSide; ++iy) {
      x = ix * var1.maxValue / (double) nPointsOnSide;
      y = iy * var2.maxValue / (double) nPointsOnSide;
      params[var1.index] = x;
      params[var2.index] = y;
      z = (*fcn)(params);

      if (maxNLL > 0 && z > maxNLL) {
        z = maxNLL;
      }

      if (isinf(z)) {
        iForMax.push_back(i);
        xForMax.push_back(x);
        yForMax.push_back(y);
        ++i;
        continue;
      } else if (z > maxz) {
        maxz = z;
      }

      // std::cout << i << ": " << x << " " << y << " " << z << std::endl;
      xArr[i] = x;
      yArr[i] = y;
      zArr[i] = z;
      ++i;
    }
  }

  for (unsigned int i = 0; i < iForMax.size(); ++i) {
    unsigned int idx = iForMax[i];
    xArr[idx] = xForMax[i];
    yArr[idx] = yForMax[i];
    zArr[idx] = maxz;
    std::cout << "Filling max at: " << xArr[idx] << ", " << yArr[idx] << std::endl;
  }

  std::ostringstream nameStream, titleStream;
  nameStream << "grph_" << var1.name << "_" << var2.name;
  titleStream << "NLL;" << var1.label << ";" << var2.label;
  std::string name = nameStream.str();
  std::string title = titleStream.str();
  TGraph2D *graph = new TGraph2D(name.c_str(), title.c_str(), nPoints, xArr, yArr, zArr);
  graph->Draw("cont4z");

  std::ostringstream outputPathStream;
  if (dirname != "") {
    outputPathStream << dirname;
  } else {
    outputPathStream << "output";
  }
  outputPathStream << "/";
  if (outputName == "") {
    outputPathStream << nameStream.str() << ".png";
  } else {
    outputPathStream << outputName;
  }
  canvas->SaveAs(outputPathStream.str().c_str());

  delete [] xArr;
  delete [] yArr;
  delete [] zArr;
  delete graph;
  if (usingCustomCanvas) delete canvas;
}

void draw2DComparisonMinimisingEverywhere(Minimiser &minimiser, 
    const Param &var1, const Param &var2, 
    double maxNLL, TCanvas *canvas,
    const std::string &dirname, const std::string &outputName)
{
  if (dirname != "") {
    boost::filesystem::create_directory(dirname);
  }

  const unsigned int nPointsOnSide(40);
  const unsigned int nPoints(nPointsOnSide*nPointsOnSide);

  bool usingCustomCanvas(false);
  if (!canvas) {
    canvas = new TCanvas();
    usingCustomCanvas = true;
    canvas->SetCanvasSize(1000, 1000);
  }

  double *xArr = new double[nPoints];
  double *yArr = new double[nPoints];
  double *zArr = new double[nPoints];
  double x, y, z;
  unsigned int i(0);
  double maxz(0.0);
  std::vector<unsigned int> iForMax;
  std::vector<double> xForMax;
  std::vector<double> yForMax;
  for (unsigned int ix = 0; ix < nPointsOnSide; ++ix) {
    for (unsigned int iy = 0; iy < nPointsOnSide; ++iy) {
      x = ix * var1.maxValue / (double) nPointsOnSide;
      y = iy * var2.maxValue / (double) nPointsOnSide;
      minimiser.fix(var1.name, x);
      minimiser.fix(var2.name, y);
      z = minimiser.getDecentNLL(20);
      std::string filename("summary_"+toStr(x)+"_"+toStr(y)+".txt");
      writeSummaryFile(minimiser.bestUniqueMinimumByRates(), minimiser, dirname, filename);
      minimiser.clearSavedMinima();

      if (maxNLL > 0 && z > maxNLL) {
        z = maxNLL;
      }

      if (isinf(z)) {
        iForMax.push_back(i);
        xForMax.push_back(x);
        yForMax.push_back(y);
        ++i;
        continue;
      } else if (z > maxz) {
        maxz = z;
      }

      // std::cout << i << ": " << x << " " << y << " " << z << std::endl;
      xArr[i] = x;
      yArr[i] = y;
      zArr[i] = z;
      ++i;
    }
  }
  minimiser.releaseAll();

  for (unsigned int i = 0; i < iForMax.size(); ++i) {
    unsigned int idx = iForMax[i];
    xArr[idx] = xForMax[i];
    yArr[idx] = yForMax[i];
    zArr[idx] = maxz;
    std::cout << "Filling max at: " << xArr[idx] << ", " << yArr[idx] << std::endl;
  }

  std::ostringstream nameStream, titleStream;
  nameStream << "grphHandy_" << var1.name << "_" << var2.name;
  titleStream << "NLL;" << var1.label << ";" << var2.label;
  std::string name = nameStream.str();
  std::string title = titleStream.str();
  TGraph2D *graph = new TGraph2D(name.c_str(), title.c_str(), nPoints, xArr, yArr, zArr);
  graph->Draw("cont4z");

  std::ostringstream outputPathStream;
  if (dirname != "") {
    outputPathStream << dirname;
  } else {
    outputPathStream << "output";
  }
  outputPathStream << "/";
  if (outputName == "") {
    outputPathStream << nameStream.str() << ".png";
  } else {
    outputPathStream << outputName;
  }
  canvas->SaveAs(outputPathStream.str().c_str());

  delete [] xArr;
  delete [] yArr;
  delete [] zArr;
  delete graph;
  if (usingCustomCanvas) delete canvas;
}

void testMinimiser()
{
  Minimiser minimiser(2, 2);
  minimiser.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  minimiser.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  std::ifstream events("eventsFR100.txt", std::ifstream::binary);
  minimiser.grabObservedEvents(events);
  events.close();
  minimiser.addFakeComponent("F");
  minimiser.addRealComponent("R");

  for (unsigned int i = 0; i < 1; ++i) {
    minimiser.randomiseStartingPoint();
    // minimiser.printStartingParameters();
    minimiser.minimise();
    // minimiser.printCurrentMinimum();
  }
  return;
  std::cout << minimiser.nUniqueMinima() << std::endl;
  std::cout << minimiser.nUniqueMinimaByRates() << std::endl;
  minimiser.printUniqueMinimaByRates();
  minimiser.printMinimum(minimiser.bestUniqueMinimumByRates());
  unsigned int minimumIndex(0); 
  for (auto minimum : minimiser.uniqueMinimaByRates()) {
    std::string dirname("output_"+toStr(minimumIndex));
    makeGraphs(minimum, minimiser, dirname);
    writeSummaryFile(minimum, minimiser, dirname);
    ++minimumIndex;
  }
}

void testMakeHandySigmaFSigmaRPlot()
{
  Minimiser minimiser(2, 2);
  minimiser.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  minimiser.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  std::ifstream events("eventsFR100.txt", std::ifstream::binary);
  minimiser.grabObservedEvents(events);
  events.close();
  minimiser.addFakeComponent("F");
  minimiser.addRealComponent("R");

  double upperBound = 1.2 * minimiser.nObsTot();
  Param sigmaTF(minimiser.parameterIndex("sigmaT_F"), "sigmaT_F", "#sigma_{TF}", upperBound);
  Param sigmaTR(minimiser.parameterIndex("sigmaT_R"), "sigmaT_R", "#sigma_{TR}", upperBound);

  draw2DComparisonMinimisingEverywhere(minimiser, sigmaTF, sigmaTR, 100, NULL, "moo", "sigmaTFsigmaTRMinimisedEverywhere.png");
}

void testMatrixMethod(std::istream &stream)
{
  MatrixMethodApplicator mm(2);

  // mm.setRealEfficiencyPriors({0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  // mm.setFakeEfficiencyPriors({0.1, 0.2, 0.1, 0.3, 0.2, 0.1, 0.4, 0.1}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  // // FIXME -- these are the wrong fake efficiencies
  // // mm.setFakeEfficiencyPriors({0.05, 0.1, 0.2, 0.3, 0.3, 0.2, 0.3, 0.0}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});

  mm.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  mm.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  std::cout << "Go matrix method!" << std::endl;
  mm.grabObservedEvents(stream);
  std::cout << std::endl;

  ComponentWithPrior::BasicPrior priorFake({mm.predictedWeight(), mm.predictedUncertainty()});
  // ComponentWithPrior::BasicPrior priorIrredTight({124, 5});

  simpleCLsForMMLikeThings(stream, priorFake);

  // unsigned int N = 300;
  // clsCalc.setNumToys(50);
  // double low = 0.0;
  // double high = 15.0;
  // for (unsigned int i = 0; i < N; ++i) {
  //   double mu = low + i * (high - low) / N;
  //   std::cout << mu << "\t" << clsCalc.approxCLsb(mu) << "\t" << clsCalc.approxCLs(mu) << "\t" << clsCalc.toysCLsb(mu) << "\t" << clsCalc.toysCLs(mu) << std::endl;
  // }
    // const auto &minimum = clsCalc.qMuConditionalMinimum();
    // const ROOT::Minuit2::MnUserParameters &userParams = minimum->UserParameters();
    // std::cout << mu << "\t" << clsCalc.qMu(mu) << "\t" << minimum->minuitMinimum()->Fval() << "\t" << userParams.Value("sigmaT_F") << "\t" << clsCalc.approxPMu(mu, mu) << "\t" << clsCalc.toysPMu(mu, mu, 50) << std::endl;
    // std::cout << mu << "\t" << clsCalc.qMu(mu) << "\t" << minimum->minuitMinimum()->Fval() << "\t" << userParams.Value("sigmaT_F") << "\t" << clsCalc.approxPMu(mu, mu) << std::endl;
  // }
}

void testPseudoMM(std::istream &stream)
{
  MatrixMethodApplicator mm(2);

  // mm.setRealEfficiencyPriors({0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  // mm.setFakeEfficiencyPriors({0.1, 0.2, 0.1, 0.3, 0.2, 0.1, 0.4, 0.1}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  mm.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  mm.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  std::cout << "Go likelihood method!" << std::endl;
  mm.grabObservedEvents(stream);
  std::cout << std::endl;

  // One doesn't need to do any fine binning for MM fits

  // FIXME Ugly hack to give uninformative prior!!
  ComponentWithPrior::BasicPrior priorFake({0, 10.0});
  simpleCLsForMMLikeThings(stream, priorFake);
}

void simpleCLsForMMLikeThings(std::istream &stream, const ComponentWithPrior::BasicPrior &priorFake, bool doToys)
{
  ComponentFactoryPtr factoryPtr(new ComponentWithPriorFactory(priorFake));

  Minimiser minimiser(1, 1, LikelihoodComputationMode::TIGHT);
  stream.clear();
  stream.seekg(0, std::ios::beg);
  minimiser.grabObservedEvents(stream);
  minimiser.addRealComponent("FakeMM", factoryPtr);
  minimiser.addRealComponent("R");

  CLsCalculatorFakeNLL clsCalc(minimiser, "sigmaT_R");
  std::cout << "sigmaDisc          = " << clsCalc.approxDiscoverySig() << std::endl;
  std::cout << "q0                 = " << clsCalc.q0() << std::endl;
  std::cout << "qMu0               = " << clsCalc.qMu(0.0) << std::endl;
  std::cout << "pMu(0, 0)          = " << clsCalc.approxPMu(0.0, 0.0) << std::endl;
  clsCalc.clearCLsComputationCaches();
  std::cout << "95\% CLsb UL        = " << clsCalc.approx95PercentCLsbUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  std::cout << "95\% CLs UL         = " << clsCalc.approx95PercentCLsUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  if (doToys) {
    std::cout << "95\% CLsb UL (toys) = " << clsCalc.toys95PercentCLsbUL() << std::endl;
    std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
    std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
    std::cout << "95\% CLs UL (toys)  = " << clsCalc.toys95PercentCLsUL() << std::endl;
    std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
    std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  }
}


void roughEstimateOfTightFakeWithNLL(std::istream &stream)
{
  // // Minimiser minimiser(8, 2, LikelihoodComputationMode::FULL_NOTIGHT);
  // Minimiser minimiser(8, 2);

  // minimiser.setRealEfficiencyPriors({0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  // minimiser.setFakeEfficiencyPriors({0.1, 0.2, 0.1, 0.3, 0.2, 0.1, 0.4, 0.1}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});
  // // FIXME -- these are the wrong fake efficiencies
  // // minimiser.setFakeEfficiencyPriors({0.05, 0.1, 0.2, 0.3, 0.3, 0.2, 0.3, 0.0}, {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05});

  Minimiser minimiser(2, 2);
  minimiser.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  minimiser.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  minimiser.grabObservedEvents(stream);
  minimiser.addFakeComponent("F");
  minimiser.addRealComponent("R");

  // ComponentWithPrior::BasicPrior priorIrredTight({124, 5});
  // ComponentFactoryPtr irredFactoryPtr(new ComponentWithPriorFactory(priorIrredTight));
  // minimiser.addRealComponent("irredBkg", irredFactoryPtr);
  
  minimiser.getDecentNLL(30, 2);
  const auto &minimum = minimiser.bestUniqueMinimumByRates();


  (*minimiser.getFcn())(minimum->minuitMinimum()->UserParameters().Params());
  std::cout << "Go likelihood method!" << std::endl;
  std::cout << "Num observed tight: " << minimiser.getFcn()->nObsTight() << std::endl;
  std::cout << "Fake rate:          " << minimiser.component("F")->rate() << std::endl;
  std::cout << "Real rate:          " << minimiser.component("R")->rate() << std::endl;
  std::cout << "Fake tight rate:    " << minimiser.component("F")->rateTight() << " +/- " << minimum->UserParameters().Error("sigmaT_F") << std::endl;
  std::cout << "Real tight rate:    " << minimiser.component("R")->rateTight() << " +/- " << minimum->UserParameters().Error("sigmaT_R") << std::endl;
  std::cout << std::endl;

  // // Simplified, "reduced matrix method" (Method C)
  // ComponentWithPrior::BasicPrior priorFake({minimiser.component("F")->rateTight(), minimum->UserParameters().Error("sigmaT_F")});
  // simpleCLsForMMLikeThings(stream, priorFake);

  // Method B
  CLsCalculatorFakeNLL clsCalc(minimiser, "sigmaT_R");
  std::cout << "sigmaDisc          = " << clsCalc.approxDiscoverySig() << std::endl;
  std::cout << "q0                 = " << clsCalc.q0() << std::endl;
  std::cout << "qMu0               = " << clsCalc.qMu(0.0) << std::endl;
  std::cout << "pMu(0, 0)          = " << clsCalc.approxPMu(0.0, 0.0) << std::endl;
  clsCalc.clearCLsComputationCaches();
  std::cout << "95\% CLsb UL        = " << clsCalc.approx95PercentCLsbUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  std::cout << "95\% CLs UL         = " << clsCalc.approx95PercentCLsUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  std::cout << "95\% CLsb UL (toys) = " << clsCalc.toys95PercentCLsbUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;
  std::cout << "95\% CLs UL (toys)  = " << clsCalc.toys95PercentCLsUL() << std::endl;
  std::cout << "    xVals: " << clsCalc.allXVals() << std::endl;
  std::cout << "    yVals: " << clsCalc.allYVals() << std::endl;

  // unsigned int N = 300;
  // clsCalc.setNumToys(50);
  // double low = 0.0;
  // double high = 15.0;
  // for (unsigned int i = 0; i < N; ++i) {
  //   double mu = low + i * (high - low) / N;
  //   double approxPMuMu = clsCalc.approxPMu(mu, mu);
  //   double approxPMu0 = clsCalc.approxPMu(mu, 0.0);
  //   double approxCLs = approxPMuMu/approxPMu0;
  //   double toysPMuMu = clsCalc.toysPMu(mu, mu, 50);
  //   double toysPMu0 = clsCalc.toysPMu(mu, 0.0, 50);
  //   double toysCLs = toysPMuMu/toysPMu0;
  //   std::cout << mu << "\t" << approxPMuMu << "\t" << approxCLs << "\t" << toysPMuMu << "\t" << toysCLs << "\t" << approxPMu0 << "\t" << toysPMu0 << std::endl;
  //   // std::cout << mu << "\t" << clsCalc.approxCLsb(mu) << "\t" << clsCalc.approxCLs(mu) << "\t" << clsCalc.toysCLsb(mu) << "\t" << clsCalc.toysCLs(mu) << std::endl;
  // }
  // unsigned int N = 10;
  // clsCalc.setNumToys(50);
  // double low = 6.4;
  // double high = 7.0;
  // for (unsigned int i = 0; i < N; ++i) {
  //   double mu = low + i * (high - low) / N;
  //   // std::cout << mu << "\t" << clsCalc.approxCLs(mu) << "\t" << clsCalc.toysCLs(mu) << "\t" << clsCalc.approxCLsb(mu) << "\t" << clsCalc.toysCLsb(mu) << std::endl;
  //   const auto &minimum = clsCalc.qMuConditionalMinimum();
  //   const ROOT::Minuit2::MnUserParameters &userParams = minimum->UserParameters();
  //   std::cout << mu << "\t" << clsCalc.qMu(mu) << "\t" << minimum->minuitMinimum()->Fval() << "\t" << userParams.Value("sigmaT_F") << "\t" << clsCalc.approxPMu(mu, mu) << "\t" << clsCalc.toysPMu(mu, mu, 50) << std::endl;
  //   // std::cout << mu << "\t" << clsCalc.qMu(mu) << "\t" << minimum->minuitMinimum()->Fval() << "\t" << userParams.Value("sigmaT_F") << "\t" << clsCalc.approxPMu(mu, mu) << std::endl;
  // }
}

void testMinimisationSpeedNearMinimum(std::istream &stream)
{
  Minimiser minimiser(2, 2);
  minimiser.setRealEfficiencyPriors({0.8, 0.9}, {0.01, 0.1});
  minimiser.setFakeEfficiencyPriors({0.1, 0.2}, {0.01, 0.1});
  minimiser.grabObservedEvents(stream);
  minimiser.addFakeComponent("F");
  minimiser.addRealComponent("R");

  minimiser.fix("sigmaT_R", 4.9);
  // minimiser.fix("sigmaT_R", 2.9);

  // ComponentWithPrior::BasicPrior priorIrredTight({124, 5});
  // ComponentFactoryPtr irredFactoryPtr(new ComponentWithPriorFactory(priorIrredTight));
  // minimiser.addRealComponent("irredBkg", irredFactoryPtr);
  
  minimiser.getDecentNLL(100, 2, 0.01);
  const auto &minima = minimiser.uniqueMinima();
  for (const auto &mini : minima) {
    std::cout << *mini->minuitMinimum();
    std::cout << minimiser.getFcn()->Gradient(mini->params()) << std::endl << std::endl;
  }

  // const auto &minimum = minimiser.bestUniqueMinimumByRates();
  // std::cout << "RANDOM STARTING POINT" << std::endl;
  // std::cout << *minimum->minuitMinimum() << std::endl;

  minima[0]->UserParameters().SetValue("sigmaT_F", minima[0]->UserParameters().Value("sigmaT_F")*0.9);
  minima[0]->UserParameters().SetValue("beta_F_0", 0.2);
  minimiser.setStartingPoint(minima[0]);
  const auto &minimum2 = minimiser.minimise(0);
  std::cout << "SENSIBLE STARTING POINT" << std::endl;
  std::cout << *minimum2->minuitMinimum();
  std::cout << minimiser.getFcn()->Gradient(minimum2->params()) << std::endl << std::endl;
  // minimiser.setStartingPoint(minima[0]);

  // const auto &minimum3 = minimiser.minimiseLocallyWithNLopt();
  // std::cout << minimum3;
  // std::cout << minimiser.getFcn()->Gradient(minimum3->params()) << std::endl << std::endl;

  // for (unsigned int i = 0; i < 20; ++i) {
  //   minimiser.randomiseStartingPoint();
  //   const auto &minimum = minimiser.minimiseLocallyWithNLopt();
  //   std::cout << minimum;
  //   std::cout << minimiser.getFcn()->Gradient(minimum->params()) << std::endl << std::endl;
  // }
  // std::cout << minimiser.nUniqueMinimaByRates() << std::endl;
  // std::cout << minimiser.nUniqueMinima() << std::endl;
}


int main() 
{ 
  gErrorIgnoreLevel = 3000;

  std::ostringstream stream;
  stream << std::cin.rdbuf();
  
  // std::ifstream events("eventsFR100.txt", std::ifstream::binary);
  // std::ifstream events("loadsOfEvents.txt", std::ifstream::binary);
  // std::ifstream events("aFewEvents.txt", std::ifstream::binary);
  // stream << events.rdbuf();
  // events.close();

  std::string input = stream.str();
  std::istringstream readStream(input);

  // testMatrixMethod(readStream);

  std::cout << std::endl << std::endl;

  readStream.clear();
  std::istringstream readStream2(input);
  // testPseudoMM(readStream2);
  roughEstimateOfTightFakeWithNLL(readStream2);

  // testMinimisationSpeedNearMinimum(readStream2);

  // testMinimiser();
  // testMakeHandySigmaFSigmaRPlot();

  return 0;
}
