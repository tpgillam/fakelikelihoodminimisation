#include "Minimiser.h"
#include "Configs.h"
#include "utils.h"

#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <boost/algorithm/string/predicate.hpp>

#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnPrint.h"

Minimiser::Minimiser(unsigned int nOmegaHat, unsigned int nLeptons,
                     LikelihoodComputationMode::e computationMode)
    : m_likelihoodComputationMode(computationMode),
      m_nOmegaHat(nOmegaHat),
      m_nLeptons(nLeptons),
      m_nObsTot(0),
      m_fcn(nOmegaHat, nLeptons),
      m_minimum(NULL),
      m_nRealEff(nOmegaHat),
      m_nFakeEff(nOmegaHat)
{
  initialiseEfficiencies(computationMode, nOmegaHat, nOmegaHat);

  // Ensure the map has sensible default values in this case
  for (unsigned int i = 0; i < nOmegaHat; ++i) {
    m_categoryToRealEff.push_back(i);
    m_categoryToFakeEff.push_back(i);
  }
  m_fcn.bindCategoriesToRealEfficiencies(m_categoryToRealEff);
  m_fcn.bindCategoriesToFakeEfficiencies(m_categoryToFakeEff);
}

Minimiser::Minimiser(unsigned int nOmegaHat, unsigned int nLeptons,
                     unsigned int nRealEff, unsigned int nFakeEff,
                     LikelihoodComputationMode::e computationMode)
    : m_likelihoodComputationMode(computationMode),
      m_nOmegaHat(nOmegaHat),
      m_nLeptons(nLeptons),
      m_nObsTot(0),
      m_fcn(nOmegaHat, nLeptons, nRealEff, nFakeEff),
      m_minimum(NULL),
      m_nRealEff(nRealEff),
      m_nFakeEff(nFakeEff)
{
  initialiseEfficiencies(computationMode, nRealEff, nFakeEff);
}

void Minimiser::initialiseEfficiencies(LikelihoodComputationMode::e computationMode, unsigned int nRealEff, unsigned int nFakeEff)
{
  bool fixEpsilons = (computationMode == LikelihoodComputationMode::TIGHT);
  // Important - they *must* be added in this order!
  for (unsigned int i = 0; i < nFakeEff; ++i) {
    std::string epsfName("epsf"+toStr(i));
    m_upar.Add(epsfName, 0.0, 0.1, 0.0, 1.0);
    nlOptLowerLimits.push_back(0.0); nlOptUpperLimits.push_back(1.0);
    if (fixEpsilons) {
      m_upar.Fix(epsfName);
    }
  }
  
  // Important - they *must* be added in this order!
  for (unsigned int i = 0; i < nRealEff; ++i) {
    std::string epsrName("epsr"+toStr(i));
    m_upar.Add(epsrName, 0.0, 0.1, 0.0, 1.0);
    nlOptLowerLimits.push_back(0.0); nlOptUpperLimits.push_back(1.0);
    if (fixEpsilons) {
      m_upar.Fix(epsrName);
    }
  }
  m_fcn.likelihoodComputationMode(computationMode);
}


Minimiser::~Minimiser()
{
  clearSavedMinima();
}

void Minimiser::clearSavedMinima()
{
  m_uniqueMinima.clear();
  m_uniqueMinimaByRates.clear();
}

void Minimiser::grabObservedEvents(std::istream &stream)
{
  std::string lineInput;
  std::vector<std::string> elems;
  std::vector<unsigned int> omegaHat(m_nLeptons, 0);
  std::vector<TLConfig::e> tlConfig(m_nLeptons);
  using namespace TLConfig;
  while (std::getline(stream, lineInput)) {
    split(lineInput, ' ', elems); 
    for (unsigned int i = 0; i < m_nLeptons; ++i) {
      if (elems[i] == "1") {
        tlConfig[i] = TIGHT;
      } else {
        tlConfig[i] = LOOSE;
      }
      omegaHat[i] = std::stoi(elems[m_nLeptons+i]);
    }
    m_fcn.addEvent(omegaHat, tlConfig);
    ++m_nObsTot;
  }
}


void Minimiser::bindCategoriesToRealEfficiencies(const std::vector<unsigned int>& categoryToRealEff)
{
  m_categoryToRealEff = categoryToRealEff;
  m_fcn.bindCategoriesToRealEfficiencies(m_categoryToRealEff);
}

void Minimiser::bindCategoriesToFakeEfficiencies(const std::vector<unsigned int>& categoryToFakeEff)
{
  m_categoryToFakeEff = categoryToFakeEff;
  m_fcn.bindCategoriesToFakeEfficiencies(m_categoryToFakeEff);
}


void Minimiser::setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_fcn.setRealEfficiencyPriors(means, uncs);
  for (unsigned int i = 0; i < nRealEff(); ++i) {
    m_upar.SetValue("epsr"+toStr(i), means[i]);
    m_upar.SetError("epsr"+toStr(i), uncs[i]);
  }
}

void Minimiser::setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_fcn.setFakeEfficiencyPriors(means, uncs);
  for (unsigned int i = 0; i < nFakeEff(); ++i) {
    m_upar.SetValue("epsf"+toStr(i), means[i]);
    m_upar.SetError("epsf"+toStr(i), uncs[i]);
  }
}

void Minimiser::addFakeComponent(const std::string &label, ComponentFactoryPtr factory)
{
  bool fixPis = false;
  bool fixBetas = false;
  addComponent(label, factory, fixPis, fixBetas);
}

void Minimiser::addRealComponent(const std::string &label, ComponentFactoryPtr factory)
{
  bool fixPis = true;
  bool fixBetas = (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT);
  addComponent(label, factory, fixPis, fixBetas);
}

void Minimiser::addComponent(const std::string &label, ComponentFactoryPtr factory, bool fixPis, bool fixBetas)
{
  if (std::find(m_componentLabels.begin(), m_componentLabels.end(), label) != m_componentLabels.end()) {
    throw std::runtime_error("Can't use '"+label+"' to label a background -- already used!");
  }

  m_componentLabels.push_back(label);
  if (!factory) {
    factory = ComponentFactoryPtr(new BackgroundComponentFactory());
  } 
  m_fcn.addComponent(factory);
  double upperLimit = std::max(4.0*m_fcn.nObsTight(), 5.0);
  m_upar.Add("sigmaT_"+label, 0.0, 1.0, 0.0, upperLimit);
  nlOptLowerLimits.push_back(0.0); nlOptUpperLimits.push_back(4.0*m_fcn.nObsTight());
  for (unsigned int i = 0; i < m_nOmegaHat; ++i) {
    std::string postfix(label+"_"+toStr(i));
    if (i != (m_nOmegaHat - 1)) {
      std::string phiName("phi_"+postfix);
      m_upar.Add(phiName, 0.0, 0.000001, 0.0, 1.0);
      nlOptLowerLimits.push_back(0.0); nlOptUpperLimits.push_back(1.0);
      if (fixBetas) {
        m_upar.Fix(phiName);
      }
    }
    std::string piName("pi_"+postfix);
    m_upar.Add(piName, 0.0, 0.1, 0.0, 1.0);
    nlOptLowerLimits.push_back(0.0); nlOptUpperLimits.push_back(1.0);
    if (fixPis) {
      m_upar.Fix(piName);
    }
  }
}

BackgroundComponent *Minimiser::component(const std::string &label)
{
  unsigned int i;
  const auto it = std::find(m_componentLabels.begin(), m_componentLabels.end(), label);
  if (it == m_componentLabels.end()) {
    throw std::runtime_error("Unknown component "+label);
  } else {
    i = std::distance(m_componentLabels.begin(), it);
  }
  if (i >= m_fcn.nComponents()) {
    throw std::runtime_error("FCN object modified without our knowing? Not enough components!");
  }

  return m_fcn.componentAtIndex(i);
}

const BackgroundComponent *Minimiser::component(const std::string &label) const
{
  return component(label);
}

void Minimiser::setStartingPoint(const std::shared_ptr<MiniMinimum> startingPoint)
{
  m_upar = startingPoint->UserParameters();
}

void Minimiser::randomiseStartingPoint(bool randomiseEfficiencies)
{
  for (const auto &label : m_componentLabels) {
    std::string sigmaTName("sigmaT_"+label);
    setComponentIfNotFixed(sigmaTName, randUniform(4.0*m_fcn.nObsTight()));
    for (unsigned int i = 0; i < m_nOmegaHat; ++i) {
      std::string postfix(label+"_"+toStr(i));
      if (i != (m_nOmegaHat - 1)) {
        setComponentIfNotFixed("phi_"+postfix, randUniform());
      }
      setComponentIfNotFixed("pi_"+postfix, randUniform());
    }
  }

  if (!randomiseEfficiencies) return;

  for (unsigned int i = 0; i < nRealEff(); ++i) {
    m_upar.SetValue("epsr"+toStr(i), randUniform());
  }
  for (unsigned int i = 0; i < nFakeEff(); ++i) {
    m_upar.SetValue("epsf"+toStr(i), randUniform());
  }
}

void Minimiser::setComponentIfNotFixed(const std::string &varname, double value)
{
  unsigned int index = m_upar.Index(varname);
  if (!m_upar.Parameter(index).IsFixed()) {
    m_upar.SetValue(index, value);
  }
}

void Minimiser::fix(const std::string &varname, double value)
{
  unsigned int index = m_upar.Index(varname);
  m_upar.SetValue(index, value);
  if (!m_upar.Parameter(index).IsFixed() || isManuallyFixed(index)) {
    m_upar.Fix(index);
    m_manuallyFixed.insert(index);
  }
}

std::string Minimiser::labelForVariableName(const std::string &varname) const
{
  std::vector<std::string> elems;
  split(varname, '_', elems);
  return elems[1];
}

void Minimiser::releaseAll()
{
  for (const auto &index : m_manuallyFixed) {
    m_upar.Release(index);
  }
  m_manuallyFixed.clear();
}

bool Minimiser::isManuallyFixed(unsigned int index) const
{
  return m_manuallyFixed.find(index) != m_manuallyFixed.end();
}

const std::shared_ptr<MiniMinimum> &Minimiser::minimise(unsigned int strategy, double tolerance)
{
  ROOT::Minuit2::MnMigrad migrad(m_fcn, m_upar, strategy);
  m_minimum = std::shared_ptr<MiniMinimum>(new MiniMinimum(migrad(0, tolerance)));
  // std::cout << std::endl << "   FINISHED:   " << m_minimum->Fval() << std::endl << std::endl;
  preserveCurrentMinimumIfDesired();
  return m_minimum;
}

void Minimiser::preserveCurrentMinimumIfDesired()
{
  bool minimumIsValid = m_minimum->isValid();
  if (!minimumIsValid) {
    return;
  }

  bool shouldPreserveCurrentMinimum = determineIfMinimumIsUnique();
  bool minimumUniqueByRates = determineIfMinimumIsUniqueByRates();
  if (minimumUniqueByRates) shouldPreserveCurrentMinimum = true;

  if (shouldPreserveCurrentMinimum) {
    m_uniqueMinima.push_back(m_minimum);
  }

  if (minimumUniqueByRates) {
    m_uniqueMinimaByRates.push_back(m_minimum);
  }
}

double Minimiser::getDecentNLL(unsigned int numStarts, unsigned int strategy, double tolerance,
    std::shared_ptr<MiniMinimum> startingPoint)
{
  for (unsigned int i = 0; i < numStarts; ++i) {
    // std::cout << "On start " << i << std::endl;
    if (startingPoint) {
      setStartingPoint(startingPoint);
      minimise(0, tolerance);
    } else {
      randomiseStartingPoint();
      minimise(strategy, tolerance);
    }
  }

  const unsigned int maxIterations = 5000;
  unsigned int numLeft = maxIterations;
  while (nUniqueMinimaByRates() == 0 && numLeft > 0) {
    randomiseStartingPoint();
    minimise(strategy, tolerance);
    --numLeft;
  }

  if (nUniqueMinimaByRates()  == 0) {
    std::cerr << "Could not find a minimum after trying " << maxIterations << " starting points" << std::endl;
    throw std::runtime_error("No valid minimum found");
  }

  return bestUniqueMinimumByRates()->Fval();
}

bool Minimiser::determineIfMinimumIsUnique(double tolerance)
{
  const auto &pars = m_minimum->UserParameters();
  unsigned int nParams = pars.Parameters().size();
  for (const auto &prevMinima : m_uniqueMinima) {
    const auto &testPars = prevMinima->UserParameters();
    bool differsFromThis = false;
    for (unsigned int i = 0; i < nParams; ++i) {
      if (!nearlyEqual(pars.Value(i), testPars.Value(i), tolerance)) {
        differsFromThis = true;
      }
    }
    if (!differsFromThis) return false;
  }
  return true;
}

bool Minimiser::determineIfMinimumIsUniqueByRates(double tolerance)
{
  const auto &pars = m_minimum->UserParameters();
  for (const auto &prevMinima : m_uniqueMinimaByRates) {
    const auto &testPars = prevMinima->UserParameters();
    bool differsFromThis = false;
    for (const auto &label : m_componentLabels) {
      std::string rateName("sigmaT_"+label);
      if (!nearlyEqual(pars.Value(rateName), testPars.Value(rateName), tolerance)) {
        differsFromThis = true;
      }
    }
    if (!differsFromThis) return false;
  }
  return true;
}

void Minimiser::printStartingParameters() const
{
  std::cout << "====================" << std::endl;
  std::cout << "Starting parameters:" << std::endl;
  printParameters(m_upar, std::cout);
  std::cout << "====================" << std::endl;
}

void Minimiser::printCurrentMinimum() const
{
  printMinimum(m_minimum);
}

void Minimiser::printMinimum(const std::shared_ptr<MiniMinimum> &minimum, std::ostream &stream) const
{
  stream << "Fval: " << minimum->Fval() << std::endl;
  stream << "NFcn: " << minimum->NFcn() << std::endl;
  const ROOT::Minuit2::MnUserParameters &minPars = minimum->UserParameters();

  printParameters(minPars, stream);
}

void Minimiser::printParameters(const ROOT::Minuit2::MnUserParameters &pars, std::ostream &stream) const
{
  unsigned int defaultPrecision = stream.precision();
  stream << std::fixed << std::setprecision(3);
  for (unsigned int i = 0; i < nFakeEff(); ++i) {
    std::string epsfName("epsf"+toStr(i));
    printLine(pars, epsfName, stream);
  }
  for (unsigned int i = 0; i < nRealEff(); ++i) {
    std::string epsrName("epsr"+toStr(i));
    printLine(pars, epsrName, stream);
  }

  stream << std::endl;

  for (const auto &label : m_componentLabels) {
    std::string sigmaTName("sigmaT_"+label);
    printLine(pars, sigmaTName, stream);

    for (unsigned int i = 0; i < m_nOmegaHat; ++i) {
      std::string postfix(label+"_"+toStr(i));
      std::string piName("pi_"+postfix);

      if (i != (m_nOmegaHat - 1)) {
        std::string phiName("phi_"+postfix);
        printLine(pars, phiName, stream);
      }
      printLine(pars, piName, stream);
    }
    stream << std::endl;
  }
  stream << std::resetiosflags(std::ios_base::basefield | std::ios_base::floatfield);
  stream.precision(defaultPrecision);
}

void Minimiser::printLine(const ROOT::Minuit2::MnUserParameters &pars, const std::string &varname, std::ostream &stream) const
{
  stream << "(" << pars.Index(varname) << ") "<< varname+":   " << pars.Value(varname) << " +/- " << pars.Error(varname) << std::endl;
}


void Minimiser::printUniqueMinima() const
{
  for (const auto &minimum : m_uniqueMinima) {
    printMinimum(minimum);    
  }
}

void Minimiser::printUniqueMinimaByRates() const
{
  for (const auto &minimum : m_uniqueMinimaByRates) {
    printMinimum(minimum);    
  }
}

std::shared_ptr<MiniMinimum> Minimiser::bestUniqueMinimumByRates() const
{
  if (m_uniqueMinimaByRates.size() == 0) {
    throw std::runtime_error("bestUniqueMinimumByRates: no entries in vector!");
  }

  unsigned int bestIndex = 0;
  double lowestNLL(m_uniqueMinimaByRates[bestIndex]->Fval());
  for (unsigned int i = 0; i < m_uniqueMinimaByRates.size(); ++i) {
    const auto &minimum = m_uniqueMinimaByRates[i];
    double val = minimum->Fval();
    if (val < lowestNLL) {
      bestIndex = i;
      lowestNLL = val;
    }
  }
  return m_uniqueMinimaByRates[bestIndex];
}

