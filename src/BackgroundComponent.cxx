#include "BackgroundComponent.h"
#include "utils.h"

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cassert>
#include <algorithm>

BackgroundComponent::BackgroundComponent(unsigned int nOmegaHat, unsigned int nLeptons, const std::vector<bool> &qIsTight)
  : m_initialisedCacheVectors(false), m_nParameters(0), m_offset(0),
  m_nOmegaHat(nOmegaHat), m_nLeptons(nLeptons), m_qIsTight(qIsTight)
{
  m_nQ = pow(2 * nOmegaHat, nLeptons);
  m_rateTight = 0.0;
  m_rhos.resize(m_nQ, 0.0);
  m_phi.resize(m_nOmegaHat-1, 0.0);
  m_beta.resize(m_nOmegaHat, 0.0);
  m_pi.resize(m_nOmegaHat, 0.0);

  fillConfigCache();
}

void BackgroundComponent::fillConfigCache()
{
  RFConfigVector rfConfig(m_nLeptons);
  TLConfigVector tlConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_nOmegaHat);

  unsigned int q = 0;
  while (!tlConfig.isLast()) {
    ohConfig.reset();
    while (!ohConfig.isLast()) {
      rfConfig.reset();
      while (!rfConfig.isLast()) {
        ConfigCache cache;
        cache.isTight.resize(m_nLeptons);
        cache.isReal.resize(m_nLeptons);
        cache.ohConfig.resize(m_nLeptons);
        for (unsigned int i = 0; i < m_nLeptons; ++i) {
          cache.isTight[i] = (tlConfig[i] == TLConfig::TIGHT);
          cache.isReal[i] = (rfConfig[i] == RFConfig::REAL);
          cache.ohConfig[i] = ohConfig[i];
        }
        cache.q = q;
        cache.qIsTight = m_qIsTight[q];
        m_configCache.push_back(cache);

        rfConfig.increment();
      }
      ++q; ohConfig.increment();
    }
    tlConfig.increment();
  }
}

void BackgroundComponent::initialiseCacheVectors()
{
  assert(!m_initialisedCacheVectors);
  m_rhoGradients.resize(m_nQ, std::vector<double>(m_nParameters, 0.0));
  m_rhoTightGradient.resize(m_nParameters, 0.0);
  m_rhoLooseGradient.resize(m_nParameters, 0.0);
}

unsigned int BackgroundComponent::extractParameters(const std::vector<double> &x, unsigned int offset)
{
  m_offset = offset;
  m_nParameters = x.size();

  if ((offset+(m_nOmegaHat-1) + m_nOmegaHat) > x.size()) {
    throw std::runtime_error("BackgroundComponent: Insufficient parameters!");
  }

  if (!m_initialisedCacheVectors) {
    initialiseCacheVectors();
    m_initialisedCacheVectors = true;
  }

  m_rateTight = x[offset];

  unsigned int phiIndex, piIndex;
  for (unsigned int i = 0; i < m_nOmegaHat; ++i) {
    if (i != m_nOmegaHat-1) {
      phiIndex = offset + 2*i + 1;
      piIndex = offset + 2*i + 2;
      m_phi[i] = x[phiIndex];
    } else {
      piIndex = offset + 2*i + 1;
    }
    m_pi[i] = x[piIndex];
  }

  computeBetasFromPhis();

  return (offset + 2*m_nOmegaHat);
}

void BackgroundComponent::computeBetasFromPhis()
{
  for (unsigned int i = 0; i < m_nOmegaHat; ++i) {
    double thisBeta = 1.0;
    for (unsigned int j = 0; j < i; ++j) {
      thisBeta *= (1.0 - m_phi[j]);
    }

    if (i < (m_nOmegaHat - 1) ) {
      thisBeta *= m_phi[i];
    }
    m_beta[i] = thisBeta;
  }
  
  // Sort phis into increasing order, then use them to split
  // unit line segment
  // std::sort(m_phi.begin(), m_phi.end());
  // for (unsigned int i = 0; i < m_phi.size(); ++i) {
  //   if (i == 0) {
  //     m_beta[i] = m_phi[i];
  //   } else {
  //     m_beta[i] = m_phi[i] - m_phi[i-1];
  //   }
  // }
  // m_beta.back() = 1.0 - m_phi.back();
  // std::cout << m_beta << std::endl;
}


std::vector<double> BackgroundComponent::extraNLLComponentGradient() const
{
  assert(m_nParameters != 0);
  return std::vector<double>(m_nParameters, 0.0);
}

void BackgroundComponent::computeRhos(
    const std::vector<double> &epsf, const std::vector<double> &epsr,
    const std::vector<unsigned int> &categoryToFakeEff,
    const std::vector<unsigned int> &categoryToRealEff,
    bool computeGradients) {
  // std::cout << "m_rateTight: " << m_rateTight << std::endl;
  // std::cout << "m_pi:        " << m_pi << std::endl;
  // std::cout << "m_phi:       " << m_phi << std::endl;
  // std::cout << "m_beta:      " << m_beta << std::endl;
  // std::cout << "categoryToFakeEff: " << categoryToFakeEff << std::endl;
  // std::cout << "categoryToRealEff: " << categoryToRealEff << std::endl;


  std::fill(m_rhos.begin(), m_rhos.end(), 0.0);
  m_rhoTight = 0.0;
  m_rhoLoose = 0.0;
  if (computeGradients) {
    for (auto &rhoGradient : m_rhoGradients) {
      std::fill(rhoGradient.begin(), rhoGradient.end(), 0.0);
    }
    std::fill(m_rhoTightGradient.begin(), m_rhoTightGradient.end(), 0.0);
    std::fill(m_rhoLooseGradient.begin(), m_rhoLooseGradient.end(), 0.0);
  }

  if (computeGradients) {
    // TODO maybe make these globals to save the allocation time?
    std::vector<double> terms(m_nLeptons, 0.0);
    std::vector<std::vector<double> > termGradients(m_nLeptons, std::vector<double>(m_nParameters, 0.0));

    // This is like iterating over q
    for (const auto &cache : m_configCache) {
      double factor = 1.0;
      std::fill(terms.begin(), terms.end(), 0.0);
      for (auto &termGradient : termGradients) {
        std::fill(termGradient.begin(), termGradient.end(), 0.0);
      }

      for (unsigned int i = 0; i < m_nLeptons; ++i) {
        const unsigned int oh = cache.ohConfig[i];
        double beta, pi, eps;
        unsigned int piIndex, epsIndex;
        short int piSign, epsSign;
        beta = m_beta[oh];
        if (oh != m_nOmegaHat - 1) {
          piIndex = m_offset + 2*oh + 2;
        } else {
          piIndex = m_offset + 2*oh + 1;
        }
        
        if (!cache.isReal[i]) {
          pi = m_pi[oh];
          piSign = 1;
          epsIndex = oh;
          if (cache.isTight[i]) {
            eps = epsf[categoryToFakeEff[oh]];
            epsSign = 1;
          } else {
            eps = (1.0 - epsf[categoryToFakeEff[oh]]);
            epsSign = -1;
          }
        } else {
          pi = (1.0 - m_pi[oh]);
          piSign = -1;
          epsIndex = m_nOmegaHat + oh;
          if (cache.isTight[i]) {
            eps = epsr[categoryToRealEff[oh]];
            epsSign = 1;
          } else {
            eps = (1.0 - epsr[categoryToRealEff[oh]]);
            epsSign = -1;
          }

        }

        double term = beta * pi * eps;
        factor *= term;
        terms[i] = term;


        // Gradient wrt. phi
        // Since we are looking at lepton i, and specifically focusing on
        // phiIndex, this is like doing
        // d beta_oh(i) / d phi_phiIndex
        // where oh is the "omega hat" index corresponding to lepton i

        for (unsigned int i_phi = 0; i_phi < (m_nOmegaHat - 1); ++i_phi) {
          // Easy case where derivative is zero
          if (i_phi > oh) {
            continue;
          } 
          
          double thisPhiDerivative = pi * eps;
          unsigned int upperLimit = std::min(oh, (m_nOmegaHat - 2));

          // This is for the purpose of dealing with the product of 
          // phi or  (1 - phi) that form a beta
          for (unsigned int i_phiTemp = 0; i_phiTemp <= upperLimit; ++i_phiTemp) {
            // (1 - x) type factor (not last, or last factor of last beta)
            if (i_phiTemp < oh) {
              // This is the term with respect to which we are taking a
              // derivative
              if (i_phiTemp == i_phi) {
                thisPhiDerivative *= -1.0;
              } else {
                thisPhiDerivative *= (1.0 - m_phi[i_phiTemp]);
              }
            } else { 
              // We're on an a x rather than 1-x type factor
              if (i_phiTemp == i_phi) {
                thisPhiDerivative *= 1.0;
              } else {
                thisPhiDerivative *= m_phi[i_phiTemp];
              }
            }
          }

          const unsigned int phiIndex = m_offset + 2*i_phi + 1;
          termGradients[i][phiIndex] += thisPhiDerivative;
          // std::cout << "[" << cache.isTight << ", " << cache.ohConfig << ", " << cache.isReal << ", " << i << ", " << i_phi << "] " << phiIndex << ", " << pi * eps << ", " << thisPhiDerivative << std::endl;
          // std::cout << "[" << cache.isTight << ", " << cache.ohConfig << ", " << cache.isReal << ", " << i << ", " << i_phi << "] " << phiIndex << ", " << pi * eps << ", " << thisPhiDerivative << ", " << termGradients[i][phiIndex] << std::endl;
        } // Over the parameter index (phi), with respect to which we're taking derivatives

        termGradients[i][piIndex] = piSign * beta * eps;
        termGradients[i][epsIndex] = epsSign * beta * pi;
      } // Over lepton index

      // std::cout << termGradients << std::endl << std::endl << std::endl;


      // TODO probably a small performance gain to be found
      // by semi-nesting this loop with the one above
      for (unsigned int i = 0; i < m_nLeptons; ++i) {
        for (unsigned int j = 0; j < m_nLeptons; ++j) {
          if (j == i) continue;
          vectorMult(termGradients[i], terms[j]);
        }
      }
      
      std::vector<double> cacheGradient(termGradients[0]);
      for (unsigned int i = 1; i < m_nLeptons; ++i) {
        vectorSum(cacheGradient, termGradients[i]);
      }
      vectorSum(m_rhoGradients[cache.q], cacheGradient);

      m_rhos[cache.q] += factor;

      if (cache.qIsTight) {
        m_rhoTight += factor;
        vectorSum(m_rhoTightGradient, cacheGradient);
      } else {
        m_rhoLoose += factor;
        vectorSum(m_rhoLooseGradient, cacheGradient);
      }
    }
  } else {
    for (const auto &cache : m_configCache) {
      double factor = 1.0;
      for (unsigned int i = 0; i < m_nLeptons; ++i) {
        unsigned int oh = cache.ohConfig[i];
        double term = m_beta[oh];
        if (!cache.isReal[i]) {
          term *= m_pi[oh];
          if (cache.isTight[i]) {
            term *= epsf[categoryToFakeEff[oh]];
          } else {
            term *= (1.0 - epsf[categoryToFakeEff[oh]]);
          }
        } else {
          term *= (1.0 - m_pi[oh]);
          if (cache.isTight[i]) {
            term *= epsr[categoryToRealEff[oh]];
          } else {
            term *= (1.0 - epsr[categoryToRealEff[oh]]);
          }
        }
        factor *= term;
      }
      m_rhos[cache.q] += factor;
      if (cache.qIsTight) {
        m_rhoTight += factor;
      } else {
        m_rhoLoose += factor;
      }
    }
  }

  // Ensure all rhos are positive
  for (auto &rho : m_rhos) {
    if (rho < 0.0) {
      rho = 0.0;
    }
  }
}

double BackgroundComponent::rate() const
{
  return rateLoose() + rateTight();
}


double BackgroundComponent::rateForQ(unsigned int q) const
{
  return m_rateTight * m_rhos[q] / m_rhoTight;
}

std::vector<double> BackgroundComponent::rateForQGradient(unsigned int q) const
{
  double rhoQ = m_rhos[q];

  std::vector<double> term1(rateTightGradient());
  vectorMult(term1, rhoQ / m_rhoTight);

  std::vector<double> term2(m_rhoGradients[q]);
  vectorMult(term2, m_rateTight / m_rhoTight);

  std::vector<double> term3(m_rhoTightGradient);
  vectorMult(term3, - m_rateTight * rhoQ / (m_rhoTight * m_rhoTight) );

  vectorSum(term1, term2);
  vectorSum(term1, term3);

  return term1;
}

double BackgroundComponent::rateLoose() const
{
  return m_rateTight * m_rhoLoose / m_rhoTight;
}

double BackgroundComponent::rateTight() const
{
  return m_rateTight;
}

std::vector<double> BackgroundComponent::rateLooseGradient() const
{
  std::vector<double> term1(m_rhoLooseGradient);
  vectorMult(term1, m_rateTight/m_rhoTight);

  std::vector<double> term2(rateTightGradient());
  vectorMult(term2, m_rhoLoose/m_rhoTight);

  std::vector<double> term3(m_rhoTightGradient);
  vectorMult(term3, - m_rateTight * m_rhoLoose  / (m_rhoTight * m_rhoTight) );

  vectorSum(term1, term2);
  vectorSum(term1, term3);

  return term1;
}

std::vector<double> BackgroundComponent::rateTightGradient() const
{
  std::vector<double> result(m_nParameters, 0.0);

  result[m_offset] = 1.0;

  return result;
}

