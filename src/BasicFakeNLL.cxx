#include "BasicFakeNLL.h"
#include "utils.h"

#include <iostream>
#include <cassert>

#include <boost/random/poisson_distribution.hpp>

BasicFakeNLL::BasicFakeNLL(unsigned int nOmegaHat, unsigned int nLeptons)
    : m_nParameters(0),
      m_likelihoodComputationMode(LikelihoodComputationMode::FULL),
      m_nOmegaHat(nOmegaHat),
      m_nLeptons(nLeptons),
      m_enableAsimovMode(false),
      m_usingEpsfPriors(false),
      m_usingEpsrPriors(false),
      m_nObsTight(0.0),
      m_nObsLoose(0.0),
      m_asimovNObsTight(0.0),
      m_asimovNObsLoose(0.0) {
  initialise(nOmegaHat, nLeptons, nOmegaHat, nOmegaHat);
}

BasicFakeNLL::BasicFakeNLL(unsigned int nOmegaHat, unsigned int nLeptons,
                           unsigned int nRealEff, unsigned int nFakeEff)
    : m_nParameters(0),
      m_likelihoodComputationMode(LikelihoodComputationMode::FULL),
      m_nOmegaHat(nOmegaHat),
      m_nLeptons(nLeptons),
      m_enableAsimovMode(false),
      m_usingEpsfPriors(false),
      m_usingEpsrPriors(false),
      m_nObsTight(0.0),
      m_nObsLoose(0.0),
      m_asimovNObsTight(0.0),
      m_asimovNObsLoose(0.0) {
  initialise(nOmegaHat, nLeptons, nRealEff, nFakeEff);
}

void BasicFakeNLL::initialise(unsigned int nOmegaHat, unsigned int nLeptons,
                              unsigned int nRealEff, unsigned int nFakeEff) {
  m_nQ = pow(2 * nOmegaHat, nLeptons);
  m_qIsTight.resize(m_nQ);
  m_nObsByQ.resize(m_nQ, 0);
  m_ratesByQ.resize(m_nQ, 0);
  m_epsf.resize(nFakeEff);
  m_epsr.resize(nRealEff);
  computeQIsTightList();
}

BasicFakeNLL::~BasicFakeNLL()
{
  for (auto component : m_components) {
    delete component;
  }
}

void BasicFakeNLL::computeQIsTightList()
{
  TLConfigVector tlConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_nOmegaHat);

  for (unsigned int i = 0; i < m_nQ; ++i) {
    m_qIsTight[i] = false;
  }

  unsigned int q = 0;
  while (!tlConfig.isLast()) {
    ohConfig.reset();
    while (!ohConfig.isLast()) {
      // TODO this should be replaced by a more general function
      // TODO this should be replaced by a more general function
      bool qIsETight = true;
      for (unsigned int i = 0; i < m_nLeptons; ++i) {
        if (tlConfig[i] == TLConfig::LOOSE) {
          qIsETight = false;
          break;
        }
      }
      m_qIsTight[q] = qIsETight;
      ++q; ohConfig.increment();
    }
    tlConfig.increment();
  }
}

void BasicFakeNLL::addComponent(ComponentFactoryPtr factory)
{
  BackgroundComponent *component = factory->createInstance(m_nOmegaHat, m_nLeptons, m_qIsTight);
  m_components.push_back(component);
}

void BasicFakeNLL::addEvent(const std::vector<unsigned int> &omegaHat,
        const std::vector<TLConfig::e> &tlConfig)
{
  unsigned int q = qForConfig(omegaHat, tlConfig);
  m_nObsByQ[q] += 1; 
  if (m_qIsTight[q]) {
    m_nObsTight += 1;
  } else {
    m_nObsLoose += 1;
  }
}

unsigned int BasicFakeNLL::nObsForEventTL(TLConfig::e tlConfig) const
{ 
  unsigned int result(0);
  unsigned int q = 0;
  for (auto it = m_nObsByQ.begin(); it != m_nObsByQ.end(); ++it) {
    if ((tlConfig == TLConfig::TIGHT && m_qIsTight[q])
        || (tlConfig == TLConfig::LOOSE && !m_qIsTight[q])) {
      result += *it;
    }
    ++q;
  }
  return result;
}

void BasicFakeNLL::bindCategoriesToRealEfficiencies(const std::vector<unsigned int>& categoryToRealEff)
{
  m_categoryToRealEff = categoryToRealEff;
}

void BasicFakeNLL::bindCategoriesToFakeEfficiencies(const std::vector<unsigned int>& categoryToFakeEff)
{
  m_categoryToFakeEff = categoryToFakeEff;
}

void BasicFakeNLL::setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_epsrMeans = means;
  m_epsrUncs = uncs;
  m_usingEpsrPriors = true;
}

void BasicFakeNLL::setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_epsfMeans = means;
  m_epsfUncs = uncs;
  m_usingEpsfPriors = true;
}

unsigned int BasicFakeNLL::qForConfig(const std::vector<unsigned int> &omegaHat,
        const std::vector<TLConfig::e> &tlConfig)
{
  unsigned int nOHLepPerms = pow(m_nOmegaHat, m_nLeptons);

  unsigned int result = 0;
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (tlConfig[i] == TLConfig::LOOSE) {
      result += pow(2, i) * nOHLepPerms;
    }
    result += pow(m_nOmegaHat, i) * omegaHat[i];
  }
  return result;
}

double BasicFakeNLL::nObsForQMaybeAsimov(unsigned int q) const
{
  if (m_enableAsimovMode) {
    return m_asimovNObsByQ[q];
  } else {
    return m_nObsByQ[q];
  }
}

double BasicFakeNLL::nObsTightMaybeAsimov() const
{
  if (m_enableAsimovMode) {
    return m_asimovNObsTight;
  } else {
    return m_nObsTight;
  }
}

double BasicFakeNLL::nObsLooseMaybeAsimov() const
{
  if (m_enableAsimovMode) {
    return m_asimovNObsLoose;
  } else {
    return m_nObsLoose;
  }
}

double BasicFakeNLL::rateForQ(unsigned int q) const
{
  double rate(0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    rate += (*component)->rateForQ(q);
  }
  return rate;
}

double BasicFakeNLL::rateTight() const
{
  double rate(0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    rate += (*component)->rateTight();
  }
  return rate;
}

double BasicFakeNLL::rateLoose() const
{
  double rate(0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    rate += (*component)->rateLoose();
  }
  return rate;
}

std::vector<double> BasicFakeNLL::rateForQGradient(unsigned int q) const
{
  assert(m_nParameters > 0);
  std::vector<double> result(m_nParameters, 0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    vectorSum(result, (*component)->rateForQGradient(q));
  }
  return result;
}

std::vector<double> BasicFakeNLL::rateTightGradient() const
{
  assert(m_nParameters > 0);
  std::vector<double> result(m_nParameters, 0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    vectorSum(result, (*component)->rateTightGradient());
  }
  return result;
}

std::vector<double> BasicFakeNLL::rateLooseGradient() const
{
  assert(m_nParameters > 0);
  std::vector<double> result(m_nParameters, 0.0);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    vectorSum(result, (*component)->rateLooseGradient());
  }
  return result;
}

double BasicFakeNLL::operator() (const std::vector<double> & x) const
{
  static unsigned int nExecutions = 0;
  setParameters(x, false); 
  double result = valueAtCurrentPosition();
  // std::cout << result << ":   " << x << std::endl;
  // if (!(nExecutions % 1000)) {
  //   std::cout << "Computing: " << nExecutions << ", return value is: " << result << std::endl;
  //   // std::cout << x << std::endl;
  // }
  ++nExecutions;
  return result;
}

void BasicFakeNLL::setParameters(const std::vector<double> & x, bool computeGradients) const
{
  // TODO some kind of efficiency check if we're using the same parameters as
  // before? If so can omit rho computation step, and maybe others too
  // TODO will probably need to do a 'compute rho gradient' step
  m_nParameters = x.size();
  extractParameters(x);
  computeRhos(computeGradients);
}

double BasicFakeNLL::valueAtCurrentPosition() const
{
  double result(0.0);

  result += valueForPoissonTerms();

  ////////////////////
  // MC rate Priors //
  ////////////////////
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    result += (*component)->extraNLLComponent();
  }

  // std::cout << "Result 1: " << result << std::endl;

  
  ///////////////////////
  // Efficiency Priors //
  ///////////////////////
  if (m_usingEpsfPriors) {
    for (unsigned int i = 0; i < m_epsf.size(); ++i) {
      double x = m_epsf[i];
      double mu = m_epsfMeans[i];
      double sigma = m_epsfUncs[i];
      result += (x - mu)*(x-mu) / (2.0 * sigma*sigma);
    }
  }

  // std::cout << "Result 2: " << result << std::endl;
    
  if (m_usingEpsrPriors) {
    for (unsigned int i = 0; i < m_epsr.size(); ++i) {
      double x = m_epsr[i];
      double mu = m_epsrMeans[i];
      double sigma = m_epsrUncs[i];
      result += (x - mu)*(x-mu) / (2.0 * sigma*sigma);
    }
  }

  // std::cout << "Result 3: " << result << std::endl;

  return result;
}

double BasicFakeNLL::valueForPoissonTerms() const
{
  double result(0.0);

  if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL) {
    double rateQ;
    for (unsigned int q = 0; q < m_nQ; ++q) {
      rateQ = rateForQ(q);
      // Cache the rates since used externally in CLs
      m_ratesByQ[q] = rateQ;
      result += poissonNLL(nObsForQMaybeAsimov(q), rateQ);
    }
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL_NOTIGHT) {
    double rateQ;
    for (unsigned int q = 0; q < m_nQ; ++q) {
      rateQ = rateForQ(q);
      m_ratesByQ[q] = rateQ;
      if (m_qIsTight[q]) continue;
      result += poissonNLL(nObsForQMaybeAsimov(q), rateQ);
    }
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT_LOOSE) {
    result += poissonNLL(nObsTightMaybeAsimov(), rateTight());
    result += poissonNLL(nObsLooseMaybeAsimov(), rateLoose());
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT) {
    result += poissonNLL(nObsTightMaybeAsimov(), rateTight());
  } else {
    throw std::runtime_error("Unknown likelihood computation mode!");
  }

  return result;
}


std::vector<double> BasicFakeNLL::Gradient(const std::vector<double> &params) const
{
  setParameters(params, true);

  std::vector<double> result(params.size(), 0.0);

  ////////////////////////////
  // Fake & MC Contribution //
  ////////////////////////////
  if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL) {
    for (unsigned int q = 0; q < m_nQ; ++q) {
      vectorSum(result, poissonNLLGradient(nObsForQMaybeAsimov(q), rateForQ(q), rateForQGradient(q)));
    }
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL_NOTIGHT) {
    for (unsigned int q = 0; q < m_nQ; ++q) {
      if (m_qIsTight[q]) continue;
      vectorSum(result, poissonNLLGradient(nObsForQMaybeAsimov(q), rateForQ(q), rateForQGradient(q)));
    }
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT_LOOSE) {
    vectorSum(result, poissonNLLGradient(nObsLooseMaybeAsimov(), rateLoose(), rateLooseGradient()));
    vectorSum(result, poissonNLLGradient(nObsTightMaybeAsimov(), rateTight(), rateTightGradient()));
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT) {
    vectorSum(result, poissonNLLGradient(nObsTightMaybeAsimov(), rateTight(), rateTightGradient()));
  } else {
    throw std::runtime_error("Unknown likelihood computation mode!");
  }


  ////////////////////
  // MC rate Priors //
  ////////////////////
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    const std::vector<double> &temp = (*component)->extraNLLComponentGradient();
    vectorSum(result, temp);
  }

  
  ///////////////////////
  // Efficiency Priors //
  ///////////////////////
  unsigned int currIndex = 0;
  double x, mu, sigma;
  if (m_usingEpsfPriors) {
    for (unsigned int i = 0; i <  m_epsf.size(); ++i) {
      x = m_epsf[i];
      mu = m_epsfMeans[i];
      sigma = m_epsfUncs[i];
      result[currIndex] += (x-mu) / (sigma*sigma);
      ++currIndex;
    }
  }

  if (m_usingEpsrPriors) {
    currIndex = nFakeEff();
    for (unsigned int i = 0; i <  m_epsr.size(); ++i) {
      x = m_epsr[i];
      mu = m_epsrMeans[i];
      sigma = m_epsrUncs[i];
      result[currIndex] += (x-mu) / (sigma*sigma);
      ++currIndex;
    }
  }

  // std::cout << "Grad:  " << result << std::endl << std::endl;
  return result;
}

double BasicFakeNLL::poissonNLL(double nObs, double rate) const
{
  return (rate - nObs * log(rate));
}

std::vector<double> BasicFakeNLL::poissonNLLGradient(double nObs, double rate, const std::vector<double> &rateGradient) const
{
  std::vector<double> result(rateGradient);
  vectorMult(result, (1.0 - nObs/rate));
  return result;
}

void BasicFakeNLL::extractParameters(const std::vector<double> &x) const
{
  unsigned int offset = extractEfficiencies(x);
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    offset = (*component)->extractParameters(x, offset);
  }
}

unsigned int BasicFakeNLL::extractEfficiencies(const std::vector<double> &x) const
{
  for (unsigned int i = 0; i < nFakeEff(); ++i) {
    m_epsf[i] = x[i];
  }

  for (unsigned int i = 0; i < nRealEff(); ++i) {
    m_epsr[i] = x[nFakeEff() + i];
  }
  return nFakeEff() + nRealEff();
}

void BasicFakeNLL::computeRhos(bool computeGradients) const
{
  for (auto component = m_components.begin(); component != m_components.end(); ++component) {
    (*component)->computeRhos(m_epsf, m_epsr, m_categoryToFakeEff, m_categoryToRealEff, computeGradients);
  }
}

void BasicFakeNLL::printObsN() const
{
  unsigned int q = 0;
  for (std::vector<double>::const_iterator it = m_nObsByQ.begin(); it != m_nObsByQ.end(); ++it) {
    std::cout << "nOBS  --  " << q << ": " << *it << std::endl;
    ++q;
  }
}


void BasicFakeNLL::setAsimovObsNByQ(const std::vector<double> &asimovNObsByQ) 
{ 
  m_asimovNObsByQ = asimovNObsByQ; 
  m_asimovNObsTight = 0.0;
  m_asimovNObsLoose = 0.0;
  for (unsigned int q = 0; q < m_asimovNObsByQ.size(); ++q) {
    if (m_qIsTight[q]) {
      m_asimovNObsTight += m_asimovNObsByQ[q];
    } else {
      m_asimovNObsLoose += m_asimovNObsByQ[q];
    }
  }
}

void BasicFakeNLL::setAsimovObsNLoose(double asimovNObsLoose) 
{ 
  m_asimovNObsLoose = asimovNObsLoose;
}

void BasicFakeNLL::setAsimovObsNTight(double asimovNObsTight) 
{ 
  m_asimovNObsTight = asimovNObsTight;
}

void BasicFakeNLL::throwAndStoreToys(const std::vector<double> &parameters)
{
  m_nParameters = parameters.size();
  extractParameters(parameters);
  computeRhos();

  if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL) {
    std::vector<double> toyData(m_nQ, 0.0);
    for (unsigned int q = 0; q < m_nQ; ++q) {
      boost::random::poisson_distribution<> poissonDistribution(rateForQ(q));
      toyData[q] = poissonDistribution(m_rng);
    }
    setAsimovObsNByQ(toyData);
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT_LOOSE) {
    boost::random::poisson_distribution<> looseDist(rateLoose());
    setAsimovObsNLoose(looseDist(m_rng));
    boost::random::poisson_distribution<> tightDist(rateTight());
    setAsimovObsNTight(tightDist(m_rng));
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT) {
    boost::random::poisson_distribution<> tightDist(rateTight());
    setAsimovObsNTight(tightDist(m_rng));
  } else {
    throw std::runtime_error("Unknown likelihood computation mode!");
  }
}

void BasicFakeNLL::storeCurrentParametersAsAsimov()
{
  if (m_likelihoodComputationMode == LikelihoodComputationMode::FULL) {
    setAsimovObsNByQ(ratesByQ());
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT_LOOSE) {
    setAsimovObsNLoose(rateLoose());
    setAsimovObsNTight(rateTight());
  } else if (m_likelihoodComputationMode == LikelihoodComputationMode::TIGHT) {
    setAsimovObsNTight(rateTight());
  } else {
    throw std::runtime_error("Unknown likelihood computation mode!");
  }
}


unsigned int nBasicFakeNLLCalls = 0;
double nloptBasicFakeNLL(const std::vector<double> &x, std::vector<double> &grad, void *data)
{
  ++nBasicFakeNLLCalls;
  NLoptData &dataStruct = *(reinterpret_cast<NLoptData*>(data));

  const BasicFakeNLL &nllObject = *(dataStruct.basicFakeNLL);

  for (unsigned int i = 0; i < x.size(); ++i) {
    dataStruct.allParams[dataStruct.freeParamIndices[i]] = x[i];
  }

  grad = nllObject.Gradient(dataStruct.allParams);
  return nllObject.valueAtCurrentPosition();
}
