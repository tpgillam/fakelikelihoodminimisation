#include <iostream>

#include <glpk.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/gamma.hpp>


void simplexTest()
{
  glp_prob *lp;
  lp = glp_create_prob();
  
  // Rows mean the number of constraints
  // GLP_FX means that it's fixed
  glp_add_rows(lp, 1);
  glp_set_row_bnds(lp, 1, GLP_FX, 6., 6.);

  // Columns means the number of original "structural" variables
  glp_add_cols(lp, 2);
  glp_set_col_bnds(lp, 1, GLP_LO, 0., 0.);
  glp_set_col_bnds(lp, 2, GLP_LO, 0., 0.);

  // Specify the objective function
  glp_set_obj_dir(lp, GLP_MAX);
  glp_set_obj_coef(lp, 1, 1.0);
  glp_set_obj_coef(lp, 2, 1.0);

  // Set the matrix defining the bounds
  // It's a sparse matrix, so we store an array of values, and also the location
  // in the matrix to which this corresponds (via ia and ja)
  // NB!! It uses ***1*** based indices!
  int ia[1+2], ja[1+2];
  double ar[1+2];
  ia[1] = 1; ja[1] = 1; ar[1] = 2.0;
  ia[2] = 1; ja[2] = 2; ar[2] = 1.0;
  glp_load_matrix(lp, 2, ia, ja, ar);

  // Solve the problem with simplex
  // ... and solve it quietly!

  glp_smcp simplex_params;
  glp_init_smcp(&simplex_params);
  simplex_params.msg_lev = GLP_MSG_OFF;
  glp_simplex(lp, &simplex_params);

  // Let's see what this looks like...
  double z = glp_get_obj_val(lp);
  std::cout << z << std::endl;


  // ... so now tweak the problem slightly and try again!
  ar[2] = 1.5;
  glp_load_matrix(lp, 2, ia, ja, ar);
  glp_simplex(lp, &simplex_params); 
  z = glp_get_obj_val(lp);
  std::cout << z << std::endl;
}


void normalDrawsTest()
{
  boost::random::mt19937 rng;
  boost::random::normal_distribution<double> dist(0, 1);
  // that's 500,000,000 in ~ 8.2 seconds
  for (unsigned int i = 0; i < 500000000; ++i) {
    dist(rng);
  }
}


double inv_norm_cdf(double x)
{
  static boost::math::normal dist(0, 1);
  return boost::math::quantile(dist, x);
}


void truncatedNormalDrawsTest()
{
  // Test speed of producing random variates from boost normal

  boost::random::mt19937 rng;
  boost::random::uniform_real_distribution<double> dist(0.1, 1);

  double x;
  double val;
  // that's 500,000,000 in ~ 35 s
  for (unsigned int i = 0; i < 500000000; ++i) {
    x = dist(rng);
    // std::cout << x << std::endl;
    val = inv_norm_cdf(x);
    std::cout << val << std::endl;
  }
}



void gammaDrawsTest()
{
  boost::random::mt19937 rng;
  boost::random::gamma_distribution<double> dist(2, 1);
  // that's 500,000,000 in ~ 86 seconds
  for (unsigned int i = 0; i < 500000000; ++i) {
    dist(rng);
  }
}


double inv_gamma_cdf(double x)
{
  static boost::math::gamma_distribution<> dist(2, 1);
  return boost::math::quantile(dist, x);
}

void truncatedGammaDrawsTest()
{
  boost::random::mt19937 rng;
  boost::random::uniform_real_distribution<double> dist(0.1, 1);

  double x;
  double val;
  // that's 50,000,000 in ~ 34 s
  for (unsigned int i = 0; i < 50000000; ++i) {
    x = dist(rng);
    val = inv_gamma_cdf(x);
    std::cout << val << std::endl;
  }

}



int main()
{
  // simplexTest();
  
  // normalDrawsTest();
  // gammaDrawsTest();
  // truncatedNormalDrawsTest();
  truncatedGammaDrawsTest();

  return 0;
}
