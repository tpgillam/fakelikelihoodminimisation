#ifndef MINIMISER_H
#define MINIMISER_H

#include "Minuit2/MnUserParameters.h"
#include "Minuit2/FunctionMinimum.h"

#include "BasicFakeNLL.h"
#include "MiniMinimum.h"

#include <iostream>
#include <set>

class Minimiser {
  public:
    Minimiser(unsigned int nOmegaHat, unsigned int nLeptons, LikelihoodComputationMode::e computationMode = LikelihoodComputationMode::FULL);
    Minimiser(unsigned int nOmegaHat, unsigned int nLeptons, unsigned int nRealEff, unsigned int nFakeEff, LikelihoodComputationMode::e computationMode = LikelihoodComputationMode::FULL);

  private:
    void initialiseEfficiencies(LikelihoodComputationMode::e computationMode, unsigned int nRealEff, unsigned int nFakeEff);

  public:

    ~Minimiser();

    void clearSavedMinima();

    void grabObservedEvents(std::istream &stream = std::cin);
    inline unsigned int nObsTot() const { return m_nObsTot; }

    void bindCategoriesToRealEfficiencies(const std::vector<unsigned int>& categoryToRealEff);
    void bindCategoriesToFakeEfficiencies(const std::vector<unsigned int>& categoryToFakeEff);

    // TODO expand to accept covariance (or at least correlated uncertainties)
    void setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);
    void setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);

    void addFakeComponent(const std::string &label, ComponentFactoryPtr factory = 0);
    void addRealComponent(const std::string &label, ComponentFactoryPtr factory = 0);

    BackgroundComponent *component(const std::string &label);
    const BackgroundComponent *component(const std::string &label) const;

    void setStartingPoint(const std::shared_ptr<MiniMinimum> startingPoint);
    void randomiseStartingPoint(bool randomiseEfficiencies=false);

    void fix(const std::string &varname, double value);
    void releaseAll();

    const std::shared_ptr<MiniMinimum> &minimise(unsigned int strategy=1, double tolerance=0.1);
    double getDecentNLL(unsigned int numStarts=100, unsigned int strategy=1, double tolerance=0.1,
        std::shared_ptr<MiniMinimum> startingPoint=0);

    void printStartingParameters() const;
    void printCurrentMinimum() const;
    void printMinimum(const std::shared_ptr<MiniMinimum> &minimum, std::ostream &stream = std::cout) const;
    void printUniqueMinima() const;
    void printUniqueMinimaByRates() const;

    inline unsigned int nUniqueMinima() const { return m_uniqueMinima.size(); }
    inline unsigned int nUniqueMinimaByRates() const { return m_uniqueMinimaByRates.size(); }

    inline const std::vector<std::shared_ptr<MiniMinimum> > &uniqueMinima() const { return m_uniqueMinima; }
    inline const std::vector<std::shared_ptr<MiniMinimum> > &uniqueMinimaByRates() const { return m_uniqueMinimaByRates; }
    std::shared_ptr<MiniMinimum> bestUniqueMinimumByRates() const;

    inline BasicFakeNLL *getFcn() { return &m_fcn; }
    inline unsigned int parameterIndex(const std::string &varname) const { return m_upar.Index(varname); }

    std::string labelForVariableName(const std::string &varname) const;

    inline LikelihoodComputationMode::e likelihoodComputationMode() const { return m_likelihoodComputationMode; }

    unsigned int nRealEff() const { return m_nRealEff; }
    unsigned int nFakeEff() const { return m_nFakeEff; }

  private:
    void addComponent(const std::string &label, ComponentFactoryPtr factory, bool fixPis, bool fixBetas);
    bool isManuallyFixed(unsigned int index) const;
    void setComponentIfNotFixed(const std::string &varname, double value);

    bool determineIfMinimumIsUnique(double tolerance=0.01);
    bool determineIfMinimumIsUniqueByRates(double tolerance=0.1);

    void preserveCurrentMinimumIfDesired();

    void printParameters(const ROOT::Minuit2::MnUserParameters &pars, std::ostream &stream) const;
    void printLine(const ROOT::Minuit2::MnUserParameters &pars, const std::string &varname, std::ostream &stream) const;

  private:
    LikelihoodComputationMode::e m_likelihoodComputationMode;
    unsigned int m_nOmegaHat;
    unsigned int m_nLeptons;
    unsigned int m_nObsTot;

    BasicFakeNLL m_fcn;

    ROOT::Minuit2::MnUserParameters m_upar;
    std::vector<std::string> m_componentLabels;
    std::shared_ptr<MiniMinimum> m_minimum;
    std::vector<std::shared_ptr<MiniMinimum> > m_uniqueMinima;
    std::vector<std::shared_ptr<MiniMinimum> > m_uniqueMinimaByRates;
    std::set<unsigned int> m_manuallyFixed;
    std::vector<double> nlOptLowerLimits;
    std::vector<double> nlOptUpperLimits;
    std::vector<unsigned int> m_categoryToRealEff;
    std::vector<unsigned int> m_categoryToFakeEff;
    unsigned int m_nRealEff;
    unsigned int m_nFakeEff;
};


#endif // MINIMISER_H
