#include "utils.h"

#ifndef NO_MINIMISING
#include "MiniMinimum.h"
#include "Minuit2/MnPrint.h"
#endif

#include <boost/algorithm/string.hpp>

#include <cstdlib>

double randUniform(double maxValue)
{
  return maxValue*((double) rand() / (RAND_MAX));
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
  elems.clear();
  boost::split(elems, s, boost::is_any_of(" \t"), boost::token_compress_on);
  return elems;
}


#ifndef NO_MINIMISING
std::ostream& operator << (std::ostream& os, const std::shared_ptr<MiniMinimum>& minimum) 
{
  os << "[" << minimum->Fval() << "]" << " (" << minimum->NFcn() << " evals). Is valid: " << minimum->isValid() << std::endl;
  os << minimum->UserParameters();
  return os;
}
#endif
