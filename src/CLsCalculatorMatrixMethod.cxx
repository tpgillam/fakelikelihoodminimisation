// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
#include "CLsCalculatorMatrixMethod.h"

CLsCalculatorMatrixMethod::CLsCalculatorMatrixMethod(MatrixMethodApplicator &mmApplicator, const std::string &poiName)
  : m_mmApplicator(&mmApplicator), m_poiName(poiName)
{
  m_bestGlobalNLL = m_minimiser->getDecentNLL(10);
  m_muPrime = m_minimiser->bestUniqueMinimumByRates()->UserParameters().Value(m_poiName);
  init();
}

double CLsCalculatorMatrixMethod::q0()
{
  if (m_muPrime < 0) {
    return 0.0;
  }
  m_minimiser->clearSavedMinima();
  double mu = 0.0;
  m_minimiser->fix(m_poiName, mu);
  double bestLocalNLL = m_minimiser->getDecentNLL(10);
  m_minimiser->clearSavedMinima();
  m_minimiser->releaseAll();
  return 2 * (bestLocalNLL - m_bestGlobalNLL);
}

double CLsCalculatorMatrixMethod::qMu(double mu)
{
  if (m_muPrime < 0 || m_muPrime > mu) {
    return 0.0;
  }
  m_minimiser->clearSavedMinima();
  m_minimiser->fix(m_poiName, mu);
  double bestLocalNLL = m_minimiser->getDecentNLL(10);
  m_minimiser->clearSavedMinima();
  m_minimiser->releaseAll();
  return 2 * (bestLocalNLL - m_bestGlobalNLL);
}

unsigned int CLsCalculatorMatrixMethod::nObsTot() const
{
  return m_mmApplicator->nObsTot();
}
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
