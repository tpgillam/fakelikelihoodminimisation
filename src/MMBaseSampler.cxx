#include "MMBaseSampler.h"
#include "utils.h"

#include <algorithm>
#include <exception>
#include <cmath>

MMBaseSampler::MMBaseSampler(unsigned int nLeptons,
                     const ComplexEfficiencyInfo &efficiencyInfo)
    : m_nLeptons(nLeptons),
      m_efficiencyInfo(efficiencyInfo) {

  m_nEventCategories = pow(efficiencyInfo.nCategories(), nLeptons);
  m_nTLConfigs = pow(2, nLeptons);
  m_nRFConfigs = m_nTLConfigs;

  m_nObs.resize(m_nEventCategories);
  for (auto& nObsForCategory : m_nObs) {
    nObsForCategory.resize(m_nTLConfigs, 0);
  }

  computeLookups();
}


void MMBaseSampler::computeLookups()
{
  m_ohConfigs.resize(m_nEventCategories);
  m_tlConfigs.resize(m_nTLConfigs);
  m_rfConfigs.resize(m_nRFConfigs);

  TLConfigVector tlConfig(m_nLeptons);
  RFConfigVector rfConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_efficiencyInfo.nCategories());

  unsigned int i = 0;
  while (!ohConfig.isLast()) {
    m_ohConfigs[i] = ohConfig;
    ++i; ohConfig.increment();
  }

  i = 0;
  while (!tlConfig.isLast()) {
    m_tlConfigs[i] = tlConfig;
    ++i; tlConfig.increment();
  }

  i = 0;
  while (!rfConfig.isLast()) {
    m_rfConfigs[i] = rfConfig;
    ++i; rfConfig.increment();
  }
}


void MMBaseSampler::grabObservedEvents(std::istream &stream) {
  std::string lineInput;
  std::vector<std::string> elems;

  TLConfigVector tlConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_efficiencyInfo.nCategories());

  using namespace TLConfig;
  while (std::getline(stream, lineInput)) {
    split(lineInput, ' ', elems); 
    for (unsigned int i = 0; i < m_nLeptons; ++i) {
      if (elems[i] == "1") {
        tlConfig[i] = TIGHT;
      } else {
        tlConfig[i] = LOOSE;
      }
      ohConfig[i] = std::stoi(elems[m_nLeptons+i]);
    }

    const auto& ohBegin = m_ohConfigs.begin();
    unsigned int ohIndex =
        std::find(ohBegin, m_ohConfigs.end(), ohConfig) - ohBegin;
    const auto& tlBegin = m_tlConfigs.begin();
    unsigned int tlIndex =
        std::find(tlBegin, m_tlConfigs.end(), tlConfig) - tlBegin;

    if (ohIndex >= m_nEventCategories) {
      throw std::runtime_error("ohIndex too big!");
    }

    // Note that we have at least one event in this category
    m_nonZeroOhIndices.insert(ohIndex);

    if (tlIndex >= m_nTLConfigs) {
      std::cerr << "tlConfig is: " << tlConfig << std::endl;
      std::cerr << "tlIndex is: " << tlIndex << std::endl;
      throw std::runtime_error("tlIndex too big!");
    }
    ++m_nObs[ohIndex][tlIndex];
  }
}

