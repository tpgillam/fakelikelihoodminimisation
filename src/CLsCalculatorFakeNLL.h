#ifndef CLS_CALCULATOR_FAKE_NLL_H
#define CLS_CALCULATOR_FAKE_NLL_H

#include "Minimiser.h"
#include "CLsCalculator.h"

#include <memory>

class CLsCalculatorFakeNLL : public CLsCalculator {
  public:
    CLsCalculatorFakeNLL(Minimiser &minimiser, const std::string &poiName,
        std::shared_ptr<MiniMinimum> startingPoint=0);

    virtual double q0();
    virtual double qMu(double mu, bool asimovMode = false,
        std::shared_ptr<MiniMinimum> startingPoint=0);
    virtual unsigned int nObsTot() const;
    virtual unsigned int nObsTight() const;
    virtual double estimateSigmaFromAsimovDataset(double mu, double muPrime);
    virtual inline double muHat() const { return m_muHat; }

    virtual void startImportanceSamplingRun();

    virtual double toysPMu(double mu, double muPrime, unsigned int numToys);
    virtual double toysPMuImportanceSampling(double mu, double muPrime, unsigned int numToys);

    inline std::shared_ptr<MiniMinimum> qMuConditionalMinimum() { return m_qMuConditionalMinimum; }

  private:
    double ensureTestStatisticPositive(double result) const;
    unsigned int numRestarts() const;

  private:
    double m_bestGlobalNLL;
    double m_muHat;
    double m_asimovBestGlobalNLL;
    double m_asimovMuHat;
    Minimiser *m_minimiser;
    std::string m_poiName;
    std::shared_ptr<MiniMinimum> m_globalMinimum;
    std::shared_ptr<MiniMinimum> m_qMuConditionalMinimum;

    bool m_firstSetOfToys;
    struct ToyRun {
      std::vector<double> nObsByQ;
      std::vector<double> ratesByQ;
      double nll;
      std::shared_ptr<MiniMinimum> globalMinimum;
      std::shared_ptr<MiniMinimum> qMuConditionalMinimum;
    };
    std::vector<ToyRun> m_toyRuns;

};

#endif // CLS_CALCULATOR_FAKE_NLL_H
