#ifndef MM_BASE_SAMPLER_H
#define MM_BASE_SAMPLER_H

#include "ComplexEfficiencyInfo.h"
#include "Configs.h"

#include <vector>
#include <iostream>
#include <set>

class MMBaseSampler {
  public:
    MMBaseSampler(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);

    void grabObservedEvents(std::istream &stream = std::cin);

    // Draw the specified number of samples and print output
    // If nSamples == -1, keep going ad infinitum
    virtual void drawSamples(int nSamples = -1) = 0;

  private:
    // Precompute lookups for indices to lepton config vectors
    void computeLookups();


  protected:
    // Number of leptons in the event
    unsigned int m_nLeptons;

    // The efficiency info we've been given
    const ComplexEfficiencyInfo& m_efficiencyInfo;

    // The number of event categories, just by lepton category
    unsigned int m_nEventCategories;

    // The number of tight / loose categories, and real / fake categories
    unsigned int m_nTLConfigs;
    unsigned int m_nRFConfigs;

    // Observed event counts, indexed by event category, then tight/loose category
    std::vector<std::vector<int> > m_nObs;

    // For a given 1st index, specify the corresponding lepton category config
    std::vector<OHConfigVector> m_ohConfigs;

    // For a given 2nd index, specify the corresponding tl config, and rf config
    // respectively
    std::vector<TLConfigVector> m_tlConfigs;
    std::vector<RFConfigVector> m_rfConfigs;

    // These are the event categories for which there are any non-zero observed
    // tight/loose event counts
    std::set<unsigned int> m_nonZeroOhIndices;

};

#endif // MM_BASE_SAMPLER_H
