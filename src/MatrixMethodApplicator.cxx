#include "MatrixMethodApplicator.h"

#include "utils.h"

MatrixMethodApplicator::MatrixMethodApplicator(unsigned int nLeptons)
    : m_nLeptons(nLeptons),
      m_nObsTot(0),
      m_matrixMethod(true, nLeptons),
      m_predictedWeight(0.0),
      m_predictedUncertainty(0.0),
      m_efficiencyInfo(0) {}

MatrixMethodApplicator::MatrixMethodApplicator(
    unsigned int nLeptons, const ComplexEfficiencyInfo &efficiencyInfo)
    : m_nLeptons(nLeptons),
      m_nObsTot(0),
      m_matrixMethod(true, nLeptons),
      m_predictedWeight(0.0),
      m_predictedUncertainty(0.0),
      m_efficiencyInfo(&efficiencyInfo) {}

void MatrixMethodApplicator::setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_matrixMethod.setRealEfficiencyPriors(means, uncs);
}

void MatrixMethodApplicator::setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs)
{
  m_matrixMethod.setFakeEfficiencyPriors(means, uncs);
}

void MatrixMethodApplicator::grabObservedEvents(std::istream &stream)
{
  std::string lineInput;
  std::vector<std::string> elems;
  std::vector<GeneralisedMatrixMethod::MatrixMethod::Lepton> leptons(m_nLeptons);

  if (!m_efficiencyInfo) {
    std::cout << "ERROR: Deprecated usage, must provide full efficiency info!" << std::endl;
    throw;
  }

  double totalWeight = 0.0;
  double totalSquaredWeight = 0.0;

  float uncElCorr = 0.f;
  float uncMuCorr = 0.f;
  std::vector<float> uncBins;
  bool initialisedUncBins = false;

  while (std::getline(stream, lineInput)) {
    ++m_nObsTot;
    split(lineInput, ' ', elems); 
    for (unsigned int i = 0; i < m_nLeptons; ++i) {
      if (elems[i] == "1") {
        leptons[i].isTight = true;
      } else {
        leptons[i].isTight = false;
      }
      leptons[i].effRealIndex = m_efficiencyInfo->categoryToRealEff[std::stoi(elems[m_nLeptons+i])];
      leptons[i].effFakeIndex = m_efficiencyInfo->categoryToFakeEff[std::stoi(elems[m_nLeptons+i])];
    }
    const auto &results = m_matrixMethod.weightsForInput(leptons);
    for (const auto &result : results) {
      totalWeight += result.weight;
      totalSquaredWeight += result.weight * result.weight;
      if (!initialisedUncBins) {
        uncBins.resize(result.varBins.size(), 0.f);
        initialisedUncBins = true;
      }
      std::vector<float> tempUncBins;
      for (const auto& x : result.varBins) { 
        tempUncBins.push_back(sqrt(x));
      }
      uncBins = vectorSum(uncBins, tempUncBins);
      uncElCorr += sqrt(result.varElCorr);
      uncMuCorr += sqrt(result.varMuCorr);
    }
    // std::cout << std::endl;
  }
  // std::cout << uncElCorr << " " << uncMuCorr << " " << totalSquaredWeight << " " << uncBins << std::endl;
  double totalVar = uncElCorr * uncElCorr + uncMuCorr * uncMuCorr + totalSquaredWeight;
  for (const auto& x : uncBins) {
    totalVar += x*x;
  }
  std::cout << "Total MM weight: " << totalWeight << " +/- " << sqrt(totalVar) << std::endl;

  m_predictedWeight = totalWeight;
  m_predictedUncertainty = sqrt(totalVar);
}
