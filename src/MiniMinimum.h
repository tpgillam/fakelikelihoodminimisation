#ifndef MINI_MINIMUM_H
#define MINI_MINIMUM_H

class Minimiser;

#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/MnUserParameters.h>

class MiniMinimum {
  public:
    MiniMinimum(const MiniMinimum &minimumToCopy);
    MiniMinimum(const std::shared_ptr<MiniMinimum> &minimumToCopy);

    MiniMinimum(const ROOT::Minuit2::FunctionMinimum &minuitMinimum);
    MiniMinimum(std::shared_ptr<const ROOT::Minuit2::FunctionMinimum> minuitMinimum);
    MiniMinimum(double minf, const ROOT::Minuit2::MnUserParameters &minParams, bool isValid, unsigned int nFuncCalls);

    std::shared_ptr<const ROOT::Minuit2::FunctionMinimum> minuitMinimum() const { return m_minuitMinimum; }

    // Forwarding methods
    inline bool isValid() const 
    { 
      if (m_isReducedMinimum) return m_isValid;
      else return m_minuitMinimum->IsValid(); 
    }

    inline double Fval() const 
    { 
      if (m_isReducedMinimum) return m_minf;
      else return m_minuitMinimum->Fval(); 
    }

    inline unsigned int NFcn() const 
    { 
      if (m_isReducedMinimum) return m_nFuncCalls;
      return m_minuitMinimum->NFcn(); 
    }

    inline ROOT::Minuit2::MnUserParameters &UserParameters() { return m_customUserParameters; }
    std::vector<double> params() { return m_customUserParameters.Params(); }
    

  private:
    std::shared_ptr<const ROOT::Minuit2::FunctionMinimum> m_minuitMinimum;
    ROOT::Minuit2::MnUserParameters m_customUserParameters;

    bool m_isReducedMinimum;
    double m_minf;
    bool m_isValid;
    unsigned int m_nFuncCalls;

};

#endif // MINI_MINIMUM_H
