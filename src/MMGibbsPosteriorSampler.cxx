#include "MMGibbsPosteriorSampler.h"
#include "utils.h"
#include "TheMatrix.h"

#include <iostream>

MMGibbsPosteriorSampler::MMGibbsPosteriorSampler(
    unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo,
    int nSamplesPerEfficiencyPoint, bool sparseMode)
    : MMBaseSampler(nLeptons, efficiencyInfo),
      m_simplexSolver(0),
      m_nSamplesPerEfficiencyPoint(nSamplesPerEfficiencyPoint),
      m_sparseMode(sparseMode) {
  resizeStorageVectors();
  m_simplexSolver = new TLSpaceSolver(&m_lepRealEffs, &m_lepFakeEffs,
                                      m_tlConfigs, m_rfConfigs);
}

MMGibbsPosteriorSampler::~MMGibbsPosteriorSampler()
{
  clearDistributions();

  if (m_simplexSolver) {
    delete m_simplexSolver;
    m_simplexSolver = 0;
  }
}


void MMGibbsPosteriorSampler::clearDistributions()
{
  for (auto& distSet : m_rateTLDists) {
    for (auto* dist : distSet) {
      if (dist) {
        delete dist;
        dist = 0;
      }
    }
  }
}


void MMGibbsPosteriorSampler::resizeStorageVectors()
{
  m_realEffs.resize(m_efficiencyInfo.nRealEfficiencies());
  m_fakeEffs.resize(m_efficiencyInfo.nFakeEfficiencies());
  m_ratesOHTL.resize(m_nEventCategories);
  for (auto & ratesTL : m_ratesOHTL) {
    ratesTL.resize(m_nTLConfigs);
  }
  m_lepRealEffs.resize(m_nLeptons);
  m_lepFakeEffs.resize(m_nLeptons);
}


void MMGibbsPosteriorSampler::drawSamples(int nSamples)
{
  bool adInfinitum = (nSamples == -1);

  initialiseBeforeDrawing();

  while (adInfinitum || (nSamples > 0)) {
    drawEfficiencies();

    // We need to make sure we start off inside the cheesy shaped hypervolume
    if (m_sparseMode) {
      for (const unsigned int& ohIndex : m_nonZeroOhIndices) {
        populateCurrentLeptonEfficiencies(ohIndex);
        initialiseTLRatesToAllowedPoint(ohIndex);
      }
    } else {
      for (unsigned int ohIndex = 0; ohIndex < m_nEventCategories; ++ohIndex) {
        populateCurrentLeptonEfficiencies(ohIndex);
        initialiseTLRatesToAllowedPoint(ohIndex);
      }
    }

    drawGibbsSamplesForSetEfficiencies(m_nSamplesPerEfficiencyPoint);
    --nSamples;
  }
}


void MMGibbsPosteriorSampler::drawGibbsSamplesForSetEfficiencies(int nSamples)
{
  if (nSamples < 0) {
    throw std::runtime_error("Infinite samples for one efficiency draw? Bad idea...");
  }

  while (nSamples > 0) {
    double fakeTightRate = 0.;
    if (m_sparseMode) {
      for (const unsigned int& ohIndex : m_nonZeroOhIndices) {
        fakeTightRate += drawAndFindFakeTightRate(ohIndex);
      }
    } else {
      for (unsigned int ohIndex = 0; ohIndex < m_nEventCategories; ++ohIndex) {
        fakeTightRate += drawAndFindFakeTightRate(ohIndex);
      }
    }

    // Slightly fudgey -- 
    if (fakeTightRate >= 0.) {
      // FIXME -- only output the last of the batch
      if (nSamples == 1) {
        std::cout << fakeTightRate << std::endl;
      }
      --nSamples;
    } else {
      std::cerr << "WARNING: drew non-positive sample (" << fakeTightRate << "), ignoring" << std::endl;
    }
  }
}


double MMGibbsPosteriorSampler::drawAndFindFakeTightRate(unsigned int ohIndex)
{
  populateCurrentLeptonEfficiencies(ohIndex);
  makeTLRateGibbsStep(ohIndex);
  return fakeTightRateForCurrentCategory(ohIndex);
}


void MMGibbsPosteriorSampler::initialiseBeforeDrawing()
{
  clearDistributions();
  initialiseTLRateDistributions(); 
}


void MMGibbsPosteriorSampler::initialiseTLRateDistributions()
{
  m_rateTLDists.resize(m_nEventCategories);
  for (auto& vec : m_rateTLDists) {
    vec.resize(m_nTLConfigs, 0);
  }

  for (unsigned int i = 0; i < m_nEventCategories; ++i) {
    for (unsigned int j = 0; j < m_nTLConfigs; ++j) {
      // Uniform prior in overall tight space
      // Sum of RVs each gamma distributed is also gamma, and shape parameters
      // add. Hence make the sum over all event categories of the offset be 1.0
      //
      // One can have a "sparse" representation for low overall event counts,
      // where one simply ignores contributions from event categories with 0
      // observed events in all tight/loose channels. At this point one should
      // modify the prior term to be 1. / (num non-zero channels)
      double offset;
      if (m_sparseMode) {
        offset = 1. / m_nonZeroOhIndices.size();
      } else {
        offset = 1. / static_cast<double>(m_nEventCategories);
      }
      m_rateTLDists[i][j] = new boost::math::gamma_distribution<>(
          static_cast<double>(m_nObs[i][j])+offset, 1.0);
    }
  }
}


void MMGibbsPosteriorSampler::drawEfficiencies()
{
  m_efficiencyInfo.drawEfficiencies(m_realEffs, m_fakeEffs);
}


void MMGibbsPosteriorSampler::populateCurrentLeptonEfficiencies(unsigned int ohIndex)
{
  // Look up values of real & fake effs for each lepton
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    const auto& lepCategory = m_ohConfigs[ohIndex][i];
    m_lepRealEffs[i] = m_realEffs[m_efficiencyInfo.categoryToRealEff[lepCategory]];
    m_lepFakeEffs[i] = m_fakeEffs[m_efficiencyInfo.categoryToFakeEff[lepCategory]];
  }
}


void MMGibbsPosteriorSampler::initialiseTLRatesToAllowedPoint(unsigned int ohIndex)
{
  // Initialise using a 'psuedo draw' inspired by the "exact sampler"

  m_simplexSolver->clearConstraints();

  for (unsigned int tlIndex = 0; tlIndex < m_nTLConfigs; ++tlIndex) {
    // If index is zero then the sampling is unlimited
    if (tlIndex == 0) {
      m_ratesOHTL[ohIndex][tlIndex] = drawTruncatedWithQuantiles(m_rateTLDists[ohIndex][tlIndex], 0., 1.);
    } else {
      double lower = m_simplexSolver->lowerLimit(tlIndex);
      double upper = m_simplexSolver->upperLimit(tlIndex);
      rationaliseLowerAndUpperBounds(lower, upper);
      m_ratesOHTL[ohIndex][tlIndex] = drawTruncatedBetween(m_rateTLDists[ohIndex][tlIndex], lower, upper);
    }

    // Add the constraint from the draw we just made to the solver,
    // except if we're at the last step
    if (tlIndex < (m_nTLConfigs - 1)) {
      m_simplexSolver->addConstraint(tlIndex, m_ratesOHTL[ohIndex][tlIndex]);
    }
  }
}


void MMGibbsPosteriorSampler::rationaliseLowerAndUpperBounds(double& lower, double& upper)
{
    if (lower < 0.) {
      // std::cerr << "WARNING: lower bound < 0, setting = 0" << std::endl;
      lower = 0.;
    }
    if (upper < 0.) {
      // std::cerr << "WARNING: upper bound < 0, setting = 0" << std::endl;
      upper = 0.;
    }
    if (upper < lower) {
      // std::cerr << "WARNING: upper (" << upper << ") < lower (" << lower << ") bound, reversing" << std::endl;
      double temp = upper;
      upper = lower;
      lower = temp;
    }
}


void MMGibbsPosteriorSampler::makeTLRateGibbsStep(unsigned int ohIndex)
{
  m_simplexSolver->clearConstraints();

  // Initialise all constraints with current rates
  for (unsigned int tlIndex = 0; tlIndex < m_nTLConfigs; ++tlIndex) {
    m_simplexSolver->addConstraint(tlIndex, m_ratesOHTL[ohIndex][tlIndex]);
  }
  
  // For each tight/loose config, compute the allowed draw conditional on all
  // the other values. After each substep, update the constraints accordingly
  for (unsigned int tlIndex = 0; tlIndex < m_nTLConfigs; ++tlIndex) {
    // First remove the constraint on the thing we're solving for
    m_simplexSolver->removeConstraint(tlIndex);
    
    double lower = m_simplexSolver->lowerLimit(tlIndex);
    double upper = m_simplexSolver->upperLimit(tlIndex);
    rationaliseLowerAndUpperBounds(lower, upper);

    if (nearlyEqual(lower, upper)) {
      m_ratesOHTL[ohIndex][tlIndex] = lower;
    } else {
      m_ratesOHTL[ohIndex][tlIndex] = drawTruncatedBetween(m_rateTLDists[ohIndex][tlIndex], lower, upper);
    }
    if (!(m_ratesOHTL[ohIndex][tlIndex] >= 0.)) {
      std::cerr << "Drew unphysical sample. bounds were: "<< lower << " " << upper << std::endl;
    }

    // Don't bother updating on the last step, won't be used!
    if (tlIndex < (m_nTLConfigs - 1)) {
      m_simplexSolver->addConstraint(tlIndex, m_ratesOHTL[ohIndex][tlIndex]);
    }
  }
}


double MMGibbsPosteriorSampler::fakeTightRateForCurrentCategory(unsigned int ohIndex) const
{
  double fakeTightRate = 0.0;
  for (const auto& tlOutConfig : m_tlConfigs) {
    if (!isThisATightEvent(tlOutConfig)) continue;
    for (const auto& rfConfig : m_rfConfigs) {
      if (!isThisAFakeEvent(rfConfig)) continue;
      unsigned int tlInIndex = 0;
      for (const auto& tlInConfig : m_tlConfigs) {
        // std::cerr << m_ratesOHTL[ohIndex][tlInIndex] << " " <<
        //                  inverseElement(rfConfig, tlInConfig) << " " <<
        //                  matrixElement(rfConfig, tlOutConfig) << std::endl;
        fakeTightRate += m_ratesOHTL[ohIndex][tlInIndex] *
                         inverseElement(rfConfig, tlInConfig) *
                         matrixElement(rfConfig, tlOutConfig);
        ++tlInIndex;
      }
    }
  }

  return fakeTightRate;
}


bool MMGibbsPosteriorSampler::isThisAFakeEvent(const RFConfigVector& rfConfig) const
{
  // Say that it is a fake event if *any* lepton is fake
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (rfConfig[i] == RFConfig::FAKE) return true;
  }
  return false;
}


bool MMGibbsPosteriorSampler::isThisATightEvent(const TLConfigVector& tlConfig) const
{ 
  // Say that it is a tight event only if *all* leptons are tight
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (tlConfig[i] == TLConfig::LOOSE) return false;
  }
  return true;
}


double MMGibbsPosteriorSampler::matrixElement(
    const RFConfigVector& rfConfig, 
    const TLConfigVector& tlConfig
    ) const
{
  return TheMatrix::matrixElement(rfConfig, tlConfig, m_lepRealEffs, m_lepFakeEffs);
}


double MMGibbsPosteriorSampler::inverseElement(
    const RFConfigVector& rfConfig,
    const TLConfigVector& tlConfig
    ) const
{
  return TheMatrix::inverseElement(rfConfig, tlConfig, m_lepRealEffs, m_lepFakeEffs);
}
