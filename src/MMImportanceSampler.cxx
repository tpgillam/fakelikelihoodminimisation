#include "MMImportanceSampler.h"

#include "utils.h"

#include <algorithm>
#include <exception>

MMImportanceSampler::MMImportanceSampler(unsigned int nLeptons,
                     const ComplexEfficiencyInfo &efficiencyInfo)
    : MMBaseSampler(nLeptons, efficiencyInfo) {

  m_ratesTL.resize(m_nTLConfigs);
  m_ratesRF.resize(m_nRFConfigs);

  const unsigned int nReal = efficiencyInfo.nRealEfficiencies();
  const unsigned int nFake = efficiencyInfo.nFakeEfficiencies();
  m_realEffs.resize(nReal, 0.);
  m_fakeEffs.resize(nFake, 0.);
  m_currRealEffs.resize(nLeptons, 0.);
  m_currFakeEffs.resize(nLeptons, 0.);
}


MMImportanceSampler::~MMImportanceSampler()
{
  cleanDistributions();
}


void MMImportanceSampler::cleanDistributions()
{
  for (auto& dist : m_realEffDists) {
    delete dist;
  }
  for (auto& dist : m_fakeEffDists) {
    delete dist;
  }
  for (auto& vec : m_rateDists) {
    for (auto& dist : vec) {
      delete dist;
    }
  }
}



void MMImportanceSampler::drawSamples(int nSamples)
{
  bool adInfinitum = (nSamples == -1);

  initialiseRandomGenerators();

  while (adInfinitum || (nSamples > 0)) {

    double logWeight = 0.;
    double fakeTightRate = 0.;

    logWeight += drawEfficiencies();
    // Choose rates for each category at a time
    for (unsigned int ohIndex = 0; ohIndex < m_ohConfigs.size(); ++ohIndex) {
      logWeight += drawCategoryRatesRF(ohIndex);
      fakeTightRate += computeFakeTightRate();
    }

    std::cout << fakeTightRate << " " << exp(logWeight) << std::endl;

    if (fakeTightRate < 0.0) {
      std::cout << " Oh bother " << std::endl << std::endl;
      std::cout << "real effs: " << m_realEffs << std::endl;
      std::cout << "fake effs: " << m_fakeEffs << std::endl;

      throw std::runtime_error("But... but!!");
    }

    --nSamples;
  }
}


void MMImportanceSampler::initialiseRandomGenerators()
{
  cleanDistributions();


  // Efficiency distributions
  
  m_realEffDists.resize(m_efficiencyInfo.realEffMeans.size(), 0);
  m_fakeEffDists.resize(m_efficiencyInfo.fakeEffMeans.size(), 0);

  for (unsigned int i = 0; i < m_efficiencyInfo.realEffMeans.size(); ++i) {
    double mean = m_efficiencyInfo.realEffMeans[i];
    double sigma = m_efficiencyInfo.realEffUncs[i];
    m_realEffDists[i] = new boost::random::normal_distribution<>(mean, sigma);
  }

  for (unsigned int i = 0; i < m_efficiencyInfo.fakeEffMeans.size(); ++i) {
    double mean = m_efficiencyInfo.fakeEffMeans[i];
    double sigma = m_efficiencyInfo.fakeEffUncs[i];
    m_fakeEffDists[i] = new boost::random::normal_distribution<>(mean, sigma);
  }


  // Rate distributions

  m_rateDists.resize(m_nEventCategories);
  for (auto& vec : m_rateDists) {
    vec.resize(m_nTLConfigs, 0);
  }

  for (unsigned int i = 0; i < m_nEventCategories; ++i) {
    for (unsigned int j = 0; j < m_nTLConfigs; ++j) {
      // // Near zero prior
      // double offset = 0.01;

      // // Jeffrey's prior
      // double offset = 0.5;

      // Uniform prior
      double offset = 1.0;

      m_rateDists[i][j] = new boost::random::gamma_distribution<>(
          static_cast<double>(m_nObs[i][j])+offset, 1.0);
    }
  }

}


double MMImportanceSampler::drawEfficiencies()
{
  // m_realEffs = m_efficiencyInfo.realEffMeans;
  // m_fakeEffs = m_efficiencyInfo.fakeEffMeans;
  // return 1.0;

  double weight = 1.0;

  // TODO think about this... it might not be necessary to have
  // this reweighting factor, *particularly* when the efficiencies are
  // uncorrelated.
  unsigned int nAttempts;

  const unsigned int nReal = m_efficiencyInfo.nRealEfficiencies();
  const unsigned int nFake = m_efficiencyInfo.nFakeEfficiencies();
  for (unsigned int i = 0; i < nReal; ++i) {
    nAttempts = 0;
    do {
      m_realEffs[i] = (*m_realEffDists[i])(m_rng);
      ++nAttempts;
    } while (m_realEffs[i] < 0.0 || m_realEffs[i] > 1.0);
    weight *= 1.0 / static_cast<double>(nAttempts);
  }

  for (unsigned int i = 0; i < nFake; ++i) {
    nAttempts = 0;
    do {
      m_fakeEffs[i] = (*m_fakeEffDists[i])(m_rng);
      ++nAttempts;
    } while (m_fakeEffs[i] < 0.0 || m_fakeEffs[i] > 1.0);
    weight *= 1.0 / static_cast<double>(nAttempts);
  }
  
  // TODO -- do we need to watch out for fakeEff >= realEff for a given
  // category?
  return log(weight);
}


double MMImportanceSampler::drawCategoryRatesRF(unsigned int ohIndex)
{
  // Draw RF rates using the importance distribution
  // TODO -- here I'm using the posterior on the TL rates, perhaps there's
  // something better?
  for (unsigned int i = 0; i < m_nTLConfigs; ++i) {
    m_ratesRF[i] = (*m_rateDists[ohIndex][i])(m_rng);
  }

  // Update the other cache variables in the class
  updateCurrentEfficiencies(ohIndex);
  // std::cout << "category index: " << ohIndex << std::endl;
  updateRatesTL();

  return samplingLogWeightForCurrentRates(ohIndex);
}


void MMImportanceSampler::updateCurrentEfficiencies(unsigned int ohIndex)
{
  // Look up values of real & fake effs for each lepton
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    const auto& lepCategory = m_ohConfigs[ohIndex][i];
    m_currRealEffs[i] = m_realEffs[m_efficiencyInfo.categoryToRealEff[lepCategory]];
    m_currFakeEffs[i] = m_fakeEffs[m_efficiencyInfo.categoryToFakeEff[lepCategory]];
  }
  // std::cout << "realEffs: " << m_currRealEffs << std::endl;
  // std::cout << "fakeEffs: " << m_currFakeEffs << std::endl;
}


void MMImportanceSampler::updateRatesTL()
{
  unsigned int tlIndex = 0;
  for (const auto& tlConfig : m_tlConfigs) {
    // Set the current tl rate according to the sum of the contributions
    // from all the RF rates
    double rate = 0.;
    unsigned int rfIndex = 0;
    for (const auto& rfConfig : m_rfConfigs) {
      rate += m_ratesRF[rfIndex] * matrixElement(rfConfig, tlConfig);
      ++rfIndex;
    }
    m_ratesTL[tlIndex] = rate;
    ++tlIndex;
  }
  // std::cout << "m_ratesTL: " << m_ratesTL << std::endl;
  // std::cout << "m_ratesRF: " << m_ratesRF << std::endl;
  // double totTL = 0.f;
  // double totRF = 0.f;
  // for (const auto& rate : m_ratesTL) totTL += rate;
  // for (const auto& rate : m_ratesRF) totRF += rate;
  // std::cout << "Totals RF vs TL:  " << totRF << " vs. " << totTL << std::endl;
  // std::cout << std::endl;
}


double MMImportanceSampler::samplingLogWeightForCurrentRates(unsigned int ohIndex) const
{
  double logWeight = 0.;
  double vr, vt, a, b;

  for (unsigned int i = 0; i < m_nTLConfigs; ++i) {
    vr = m_ratesRF[i];
    vt = m_ratesTL[i];
    a = m_rateDists[ohIndex][i]->alpha();
    b = m_rateDists[ohIndex][i]->beta();
    logWeight += (a-1.)* ( log(vt) - log(vr)) - (1./b) * (vt - vr);
  }

  // if (logWeight > 7.) {
  //   std::cout << "realEffs: " << m_currRealEffs << std::endl;
  //   std::cout << "fakeEffs: " << m_currFakeEffs << std::endl;
  //   std::cout << "m_nObs: " << m_nObs[ohIndex] << std::endl;
  //   std::cout << "m_ratesTL: " << m_ratesTL << std::endl;
  //   std::cout << "m_ratesRF: " << m_ratesRF << std::endl;
  //   double totTL = 0.f;
  //   double totRF = 0.f;
  //   for (const auto& rate : m_ratesTL) totTL += rate;
  //   for (const auto& rate : m_ratesRF) totRF += rate;
  //   std::cout << "Totals RF vs TL:  " << totRF << " vs. " << totTL << std::endl;
  //   std::cout << logWeight << std::endl << std::endl;;
  // }
  return logWeight;
}


double MMImportanceSampler::matrixElement(
    const RFConfigVector& rfConfig, 
    const TLConfigVector& tlConfig
    ) const
{
  double element = 1.;
  using namespace TLConfig;
  using namespace RFConfig;
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    // Pick out cofactor
    if (tlConfig[i] == TIGHT && rfConfig[i] == REAL) {
      element *= m_currRealEffs[i];
    }
    else if (tlConfig[i] == LOOSE && rfConfig[i] == REAL) {
      element *= (1. - m_currRealEffs[i]);
    }
    else if (tlConfig[i] == TIGHT && rfConfig[i] == FAKE) {
      element *= m_currFakeEffs[i];
    }
    else if (tlConfig[i] == LOOSE && rfConfig[i] == FAKE) {
      element *= (1. - m_currFakeEffs[i]);
    } else {
      throw std::runtime_error("Oh bother");
    }
  }
  return element;
}


double MMImportanceSampler::computeFakeTightRate()
{
  double fakeTightRate = 0.0;

  unsigned int rfIndex = 0;
  for (const auto& rfConfig : m_rfConfigs) {
    if (!isThisAFakeEvent(rfConfig)) continue;
    for (const auto& tlConfig : m_tlConfigs) {
      if (!isThisATightEvent(tlConfig)) continue;
      fakeTightRate += m_ratesRF[rfIndex] * matrixElement(rfConfig, tlConfig);
    }
    ++rfIndex;
  }

  return fakeTightRate;
}


bool MMImportanceSampler::isThisAFakeEvent(const RFConfigVector& rfConfig)
{
  // Say that it is a fake event if *any* lepton is fake
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (rfConfig[i] == RFConfig::FAKE) return true;
  }
  return false;
}


bool MMImportanceSampler::isThisATightEvent(const TLConfigVector& tlConfig)
{ 
  // Say that it is a tight event only if *all* leptons are tight
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (tlConfig[i] == TLConfig::LOOSE) return false;
  }
  return true;
}
