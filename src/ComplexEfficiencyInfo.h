#ifndef COMPLEX_EFFICIENCY_INFO_H
#define COMPLEX_EFFICIENCY_INFO_H 

#include <vector>
#include <string>
#include <ostream>
#include <utility>

#include <boost/math/distributions/normal.hpp>


namespace EfficiencyVersion {
  enum e {
    UNKNOWN, BASIC, WITH_CORRELATIONS
  };
}

namespace FileLocation {
  enum e {
    BEGIN, CATEGORIES, REAL, FAKE
  };
}


class ComplexEfficiencyInfo {
  public:
    ComplexEfficiencyInfo(const std::string& efficiencyConfigFilename, 
        EfficiencyVersion::e efficiencyVersion = EfficiencyVersion::UNKNOWN);
    ~ComplexEfficiencyInfo();

    unsigned int nCategories() const {
      return categoryToRealEff.size();
    }

    unsigned int nRealEfficiencies() const {
      return realEffMeans.size();
    }

    unsigned int nFakeEfficiencies() const {
      return fakeEffMeans.size();
    }

    // Compute draw of the real and fake efficiencies, making use
    // of full correlations where relevant. Please give references to vectors
    // which are already of the correct capacity
    void drawEfficiencies(std::vector<double>& realEffs,
                          std::vector<double>& fakeEffs) const;

    std::vector<unsigned int> categoryToRealEff;
    std::vector<unsigned int> categoryToFakeEff;

    // These are the mean values of each efficiency
    std::vector<double> realEffMeans;
    std::vector<double> fakeEffMeans;

    // These represent the uncorrelated uncertainties for each efficiency
    std::vector<double> realEffUncs;
    std::vector<double> fakeEffUncs;

    // Information to store correlation information. 
    //
    // Vector of correlation groups for real and fake indices, each of which
    // contains indices of efficiencies that are within this correlation group
    std::vector<std::vector<unsigned int> > realEffCorrGroups;
    std::vector<std::vector<unsigned int> > fakeEffCorrGroups;

    // ... and then the corresponding uncertainties for these correlation groups
    // for each efficiency
    std::vector<double> realEffUncsCorr;
    std::vector<double> fakeEffUncsCorr;

    friend std::ostream& operator<<(std::ostream& os, const ComplexEfficiencyInfo& info);


  private:
    EfficiencyVersion::e getEfficiencyVersionFromStream(std::ifstream& stream);
    void loadBasicEfficiencyFile(std::ifstream& stream);
    void loadCorrelatedEfficiencyFile(std::ifstream& stream);
    void initialiseEfficiencyDistributions();
    void initialiseUncorrelatedEfficiencyDistributions();
    void initialiseCorrelatedEfficiencyDistributions();
    void drawUncorrelatedEfficiencies(std::vector<double>& realEffs,
                          std::vector<double>& fakeEffs) const;
    void drawCorrelatedEfficiencies(std::vector<double>& realEffs,
                          std::vector<double>& fakeEffs) const;
    void precomputeCorrelatedTruncationBounds();

  private:
    EfficiencyVersion::e m_efficiencyVersion;
    // Store distributions from which we can sample
    //
    // NB: in the uncorrelated case, these distributions correspond to the entire
    // (bar truncation) distribution from which we want to sample
    //
    // For the correlated case, these are all purely for the *uncorrelated*
    // uncertainty, and by construction have mean of 0
    std::vector<boost::math::normal*> m_realEffDists;
    std::vector<boost::math::normal*> m_fakeEffDists;

    // For the correlated case, the following standard normal distribution is
    // sampled from (in a truncated sense), and then scaled
    boost::math::normal m_standardNormalDist; 

    // Upper and lower bounds for each correlation group
    std::vector<std::pair<double, double> > m_realCorrGroupDrawBounds;
    std::vector<std::pair<double, double> > m_fakeCorrGroupDrawBounds;
};

std::ostream& operator<<(std::ostream& os, const ComplexEfficiencyInfo& info);

#endif // COMPLEX_EFFICIENCY_INFO_H
