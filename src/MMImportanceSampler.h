#ifndef MM_IMPORTANCE_SAMPLER_H
#define MM_IMPORTANCE_SAMPLER_H

#include "MMBaseSampler.h"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>

// Produce draws from the marginal likelihood of the fake-and-tight event rate,
// using an importance sampling methodology
class MMImportanceSampler : public MMBaseSampler {
  public:
    MMImportanceSampler(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);
    ~MMImportanceSampler();

    // Draw the specified number of samples and print output
    // If nSamples == -1, keep going ad infinitum
    virtual void drawSamples(int nSamples = -1);


  private:
    // Delete any previously allocated distribution objects
    void cleanDistributions();

    // Initialise the generators that will produce random numbers
    void initialiseRandomGenerators();
    
    // Store a random draw from the given prior on the efficiencies. Return the
    // log of the weight corresponding to how many tries were made for each
    // efficiency
    double drawEfficiencies();

    // Store an importance-sampled draw on the real & fake index rates. Since we
    // are not drawing from the *actual* distribution, also return the
    // log of the appropriate importance sampling weighting factor
    double drawCategoryRatesRF(unsigned int ohIndex);

    // Update the "current efficiencies" for this event category
    void updateCurrentEfficiencies(unsigned int ohIndex);

    // Update the rates in m_ratesTL given the values currently in m_ratesRF
    void updateRatesTL();

    // Return the (log of the) importance sampling reweighting factor for the
    // rates currently stored in m_ratesRF and m_ratesTL
    double samplingLogWeightForCurrentRates(unsigned int ohIndex) const;

    // Compute the matrix element involved in the matrix method
    double matrixElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;

    // Compute the rate of tight & fake event for this category using the most
    // recently drawn category rates and current efficiencies
    double computeFakeTightRate();

    // Return true if we should regard this configuration as representing
    // a fake event
    bool isThisAFakeEvent(const RFConfigVector& rfConfig);
    
    // Return true if we should regard this configuration as representing
    // a tight event
    bool isThisATightEvent(const TLConfigVector& tlConfig);


  private:
    // Random number generator
    boost::random::mt19937 m_rng;

    // Efficiency distributions
    // TODO no correlation in here at the moment:
    //
    // In general, draw a vector x of standard normal variates, then compute
    // the draw we want using
    //     mu +  A * x
    // where A^T A = \Sigma [covariance matrix]
    // e.g. A is cholesky decomposition of the covariance matrix
    std::vector<boost::random::normal_distribution<> *> m_realEffDists;
    std::vector<boost::random::normal_distribution<> *> m_fakeEffDists;

    // Temporary efficiency storage for a given draw
    std::vector<double> m_realEffs;
    std::vector<double> m_fakeEffs;

    // Efficiency values index over leptons for the current event category being
    // considered
    std::vector<double> m_currRealEffs;
    std::vector<double> m_currFakeEffs;

    // Rate distributions -- same indexing as for m_nObs
    std::vector<std::vector<boost::random::gamma_distribution<> *> > m_rateDists;

    // Temporary rate storage for given event category, indexed over *RF*
    // config. These are 'importance sampled' values
    std::vector<double> m_ratesRF;

    // Temporary rate storage for given event category, indexed over *TL*
    // config. These will always be the values computed from m_ratesRF
    std::vector<double> m_ratesTL;
};


#endif // MM_IMPORTANCE_SAMPLER_H
