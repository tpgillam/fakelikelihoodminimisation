#include "MMExactPosteriorSampler.h"
#include "utils.h"
#include "TheMatrix.h"

#include <iostream>

MMExactPosteriorSampler::MMExactPosteriorSampler(
    unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo)
    : MMBaseSampler(nLeptons, efficiencyInfo),
      m_simplexSolver(0)
{
  resizeStorageVectors();
  m_simplexSolver = new TLSpaceSolver(&m_lepRealEffs, &m_lepFakeEffs,
                                      m_tlConfigs, m_rfConfigs);
}

MMExactPosteriorSampler::~MMExactPosteriorSampler()
{
  clearDistributions();

  if (m_simplexSolver) {
    delete m_simplexSolver;
    m_simplexSolver = 0;
  }
}


void MMExactPosteriorSampler::clearDistributions()
{
  for (auto& distSet : m_rateTLDists) {
    for (auto* dist : distSet) {
      if (dist) {
        delete dist;
        dist = 0;
      }
    }
  }
}


void MMExactPosteriorSampler::resizeStorageVectors()
{
  m_realEffs.resize(m_efficiencyInfo.nRealEfficiencies());
  m_fakeEffs.resize(m_efficiencyInfo.nFakeEfficiencies());
  m_ratesTL.resize(m_nTLConfigs);
  m_lepRealEffs.resize(m_nLeptons);
  m_lepFakeEffs.resize(m_nLeptons);
}


void MMExactPosteriorSampler::drawSamples(int nSamples)
{
  bool adInfinitum = (nSamples == -1);

  initialiseBeforeDrawing();

  while (adInfinitum || (nSamples > 0)) {
    drawEfficiencies();
    double fakeTightRate = 0.;
    for (unsigned int ohIndex = 0; ohIndex < m_nEventCategories; ++ohIndex) {
      populateCurrentLeptonEfficiencies(ohIndex);
      drawTLRates(ohIndex);
      fakeTightRate += fakeTightRateForCurrentCategory();
    }

    // Slightly fudgey -- 
    if (fakeTightRate >= 0.) {
      std::cout << fakeTightRate << std::endl;
      --nSamples;
    } else {
      std::cerr << "WARNING: drew negative sample, ignoring" << std::endl;
    }
  }
}


void MMExactPosteriorSampler::initialiseBeforeDrawing()
{
  clearDistributions();
  initialiseTLRateDistributions(); 
}


void MMExactPosteriorSampler::initialiseTLRateDistributions()
{
  m_rateTLDists.resize(m_nEventCategories);
  for (auto& vec : m_rateTLDists) {
    vec.resize(m_nTLConfigs, 0);
  }

  for (unsigned int i = 0; i < m_nEventCategories; ++i) {
    for (unsigned int j = 0; j < m_nTLConfigs; ++j) {
      // Uniform prior in overall tight space
      // Sum of RVs each gamma distributed is also gamma, and shape parameters
      // add. Hence make the sum over all event categories of the offset be 1.0
      //
      // TODO One could conceive having a "sparse" representation for low
      // overall event counts, where one simply ignores contributions from
      // event categories with 0 observed events in all tight/loose channels. At
      // this point one should modify the prior term to be 1. / (num non-zero
      // channels)
      double offset = 1. / static_cast<double>(m_nEventCategories);
      m_rateTLDists[i][j] = new boost::math::gamma_distribution<>(
          static_cast<double>(m_nObs[i][j])+offset, 1.0);
    }
  }
}


void MMExactPosteriorSampler::drawEfficiencies()
{
  m_efficiencyInfo.drawEfficiencies(m_realEffs, m_fakeEffs);
}


void MMExactPosteriorSampler::populateCurrentLeptonEfficiencies(unsigned int ohIndex)
{
  // Look up values of real & fake effs for each lepton
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    const auto& lepCategory = m_ohConfigs[ohIndex][i];
    m_lepRealEffs[i] = m_realEffs[m_efficiencyInfo.categoryToRealEff[lepCategory]];
    m_lepFakeEffs[i] = m_fakeEffs[m_efficiencyInfo.categoryToFakeEff[lepCategory]];
  }
}


void MMExactPosteriorSampler::drawTLRates(unsigned int ohIndex)
{
  m_simplexSolver->clearConstraints();

  for (unsigned int tlIndex = 0; tlIndex < m_nTLConfigs; ++tlIndex) {
    // If index is zero then the sampling is unlimited
    // TODO this can be made slightly more efficient by using the appropriate
    // sampler
    if (tlIndex == 0) {
      m_ratesTL[tlIndex] = drawTruncatedWithQuantiles(m_rateTLDists[ohIndex][tlIndex], 0., 1.);
    } else {
      double lower = m_simplexSolver->lowerLimit(tlIndex);
      double upper = m_simplexSolver->upperLimit(tlIndex);
      m_ratesTL[tlIndex] = drawTruncatedBetween(m_rateTLDists[ohIndex][tlIndex], lower, upper);
    }

    // Add the constraint from the draw we just made to the solver,
    // except if we're at the last step
    if (tlIndex < (m_nTLConfigs - 1)) {
      m_simplexSolver->addConstraint(tlIndex, m_ratesTL[tlIndex]);
    }
  }
}


double MMExactPosteriorSampler::fakeTightRateForCurrentCategory() const
{
  double fakeTightRate = 0.0;
  for (const auto& tlOutConfig : m_tlConfigs) {
    if (!isThisATightEvent(tlOutConfig)) continue;
    for (const auto& rfConfig : m_rfConfigs) {
      if (!isThisAFakeEvent(rfConfig)) continue;
      unsigned int tlInIndex = 0;
      for (const auto& tlInConfig : m_tlConfigs) {
        fakeTightRate += m_ratesTL[tlInIndex] *
                         inverseElement(rfConfig, tlInConfig) *
                         matrixElement(rfConfig, tlOutConfig);
        ++tlInIndex;
      }
    }
  }

  return fakeTightRate;
}


bool MMExactPosteriorSampler::isThisAFakeEvent(const RFConfigVector& rfConfig) const
{
  // Say that it is a fake event if *any* lepton is fake
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (rfConfig[i] == RFConfig::FAKE) return true;
  }
  return false;
}


bool MMExactPosteriorSampler::isThisATightEvent(const TLConfigVector& tlConfig) const
{ 
  // Say that it is a tight event only if *all* leptons are tight
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    if (tlConfig[i] == TLConfig::LOOSE) return false;
  }
  return true;
}


double MMExactPosteriorSampler::matrixElement(
    const RFConfigVector& rfConfig, 
    const TLConfigVector& tlConfig
    ) const
{
  return TheMatrix::matrixElement(rfConfig, tlConfig, m_lepRealEffs, m_lepFakeEffs);
}


double MMExactPosteriorSampler::inverseElement(
    const RFConfigVector& rfConfig,
    const TLConfigVector& tlConfig
    ) const
{
  return TheMatrix::inverseElement(rfConfig, tlConfig, m_lepRealEffs, m_lepFakeEffs);
}
