// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
#ifndef CLS_CALCULATOR_MATRIX_METHOD_H
#define CLS_CALCULATOR_MATRIX_METHOD_H

#include "CLsCalculator.h"
#include "MatrixMethodApplicator.h"

class CLsCalculatorMatrixMethod : public CLsCalculator {
  public:
    CLsCalculatorMatrixMethod(MatrixMethodApplicator &mmApplicator, const std::string &poiName);

    virtual double q0();
    virtual double qMu(double mu);
    virtual unsigned int nObsTot() const;

  private:
    double m_bestGlobalNLL;
    double m_muPrime;
    MatrixMethodApplicator *m_mmApplicator;
    Minimiser *m_minimiser;
    std::string m_poiName;
};

#endif // CLS_CALCULATOR_MATRIX_METHOD_H
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
// FIXME BROKEN
