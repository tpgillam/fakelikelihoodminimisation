#ifndef MM_EXACT_POSTERIOR_SAMPLER_H
#define MM_EXACT_POSTERIOR_SAMPLER_H

#include "MMBaseSampler.h"
#include "TLSpaceSolver.h"

#include <boost/math/distributions/gamma.hpp>


class MMExactPosteriorSampler : public MMBaseSampler {
  public:
    MMExactPosteriorSampler(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);
    ~MMExactPosteriorSampler();

    virtual void drawSamples(int nSamples = -1);


  private:
    void clearDistributions();
    void resizeStorageVectors();
    void initialiseBeforeDrawing();
    void initialiseTLRateDistributions(); 
    void drawEfficiencies();
    void populateCurrentLeptonEfficiencies(unsigned int ohIndex);
    void drawTLRates(unsigned int ohIndex);
    double fakeTightRateForCurrentCategory() const;
    bool isThisAFakeEvent(const RFConfigVector& rfConfig) const;
    bool isThisATightEvent(const TLConfigVector& tlConfig) const;


    // Compute the matrix element involved in the matrix method
    double matrixElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;
    
    // Compute the element of the inverse matrix involved in the matrix method
    double inverseElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;


  private:
    // Rate distributions -- same indexing as for m_nObs
    // (indexed by event category, then tight/loose category)
    std::vector<std::vector<boost::math::gamma_distribution<> *> > m_rateTLDists;

    // Temporary storage of efficiencies for a given draw
    std::vector<double> m_realEffs;
    std::vector<double> m_fakeEffs;
    
    // Temporary rate storage for current event category, indexed over TL
    // config
    std::vector<double> m_ratesTL;

    // Efficiency values index over leptons for the current event category being
    // considered
    std::vector<double> m_lepRealEffs;
    std::vector<double> m_lepFakeEffs;

    // Simplex solver in TL space
    TLSpaceSolver* m_simplexSolver;
};


#endif // MM_EXACT_POSTERIOR_SAMPLER_H
