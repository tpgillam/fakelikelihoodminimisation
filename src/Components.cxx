#include "Components.h"
#include "utils.h"

#include <cmath>


ComponentWithPrior::ComponentWithPrior(unsigned int nOmegaHat,
    unsigned int nLeptons, const std::vector<bool> &qIsTight,
    const ComponentWithPrior::BasicPrior &priorTight)
  : BackgroundComponent(nOmegaHat, nLeptons, qIsTight),
  m_priorTight(priorTight), 
  m_haveLoosePrior(false)
{
}

ComponentWithPrior::ComponentWithPrior(unsigned int nOmegaHat,
    unsigned int nLeptons, const std::vector<bool> &qIsTight,
    const ComponentWithPrior::BasicPrior &priorTight, 
    const ComponentWithPrior::BasicPrior &priorLoose)
  : BackgroundComponent(nOmegaHat, nLeptons, qIsTight),
  m_priorTight(priorTight),
  m_priorLoose(priorLoose),
  m_haveLoosePrior(true)
{
}

double ComponentWithPrior::extraNLLComponent() const
{
  double result(0.0);

  // FIXME
  // FIXME Should probably be using some kind of (weighted) Poisson here!

  double x = rateTight();
  double mu = m_priorTight.mean;
  double sigma = m_priorTight.unc;
  result += (x-mu)*(x-mu) / (2.0 * sigma*sigma);

  if (m_haveLoosePrior) {
    x = rateLoose();
    mu = m_priorLoose.mean;
    sigma = m_priorLoose.unc;
    result += (x-mu)*(x-mu) / (2.0 * sigma*sigma);
  }

  return result;
}

std::vector<double> ComponentWithPrior::extraNLLComponentGradient() const
{
  std::vector<double> tightGrad(rateTightGradient());
  std::vector<double> result(tightGrad.size(), 0.0);

  double x = rateTight();
  double mu = m_priorTight.mean;
  double sigma = m_priorTight.unc;
  vectorMult(tightGrad, (x-mu) / (sigma*sigma));
  vectorSum(result, tightGrad);

  if (m_haveLoosePrior) {
    std::vector<double> looseGrad(rateLooseGradient());
    x = rateLoose();
    mu = m_priorLoose.mean;
    sigma = m_priorLoose.unc;
    vectorMult(looseGrad, (x-mu) / (sigma*sigma));
    vectorSum(result, looseGrad);
  }

  return result;
}
