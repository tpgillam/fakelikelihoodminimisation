#ifndef COMPONENTS_H
#define COMPONENTS_H

#include "BackgroundComponent.h"

#include <vector>
#include <memory>


class ComponentWithPrior : public BackgroundComponent {
  public:
    struct BasicPrior {
      double mean;
      double unc;
    };

  public:
    ComponentWithPrior(unsigned int nOmegaHat, unsigned int nLeptons, const std::vector<bool> &qIsTight, const BasicPrior &priorTight);
    ComponentWithPrior(unsigned int nOmegaHat, unsigned int nLeptons, const std::vector<bool> &qIsTight, const BasicPrior &priorTight, const BasicPrior &priorLoose);

    virtual double extraNLLComponent() const;
    virtual std::vector<double> extraNLLComponentGradient() const;

  private:
    BasicPrior m_priorTight;
    BasicPrior m_priorLoose;
    bool m_haveLoosePrior;
};


class IComponentFactory {
  public:
    virtual BackgroundComponent *createInstance(unsigned int nOmegaHat, 
        unsigned int nLeptons, 
        const std::vector<bool> &qIsTight) const = 0;
};

template <typename T>
class GenericComponentFactory : public IComponentFactory {
  public:
    virtual BackgroundComponent *createInstance(unsigned int nOmegaHat, 
        unsigned int nLeptons, 
        const std::vector<bool> &qIsTight) const
    {
      return (BackgroundComponent*) new T(nOmegaHat, nLeptons, qIsTight);
    }
};

template <>
class GenericComponentFactory <ComponentWithPrior> : public IComponentFactory {
  public:
    typedef const ComponentWithPrior::BasicPrior &PriorRef;
    typedef const ComponentWithPrior::BasicPrior *PriorPtr;

    GenericComponentFactory <ComponentWithPrior>(PriorRef priorTight)
      : m_priorTight(&priorTight), m_haveLoosePrior(false)
    {
    }

    GenericComponentFactory <ComponentWithPrior>(PriorRef priorTight, PriorRef priorLoose)
      : m_priorTight(&priorTight), m_priorLoose(&priorLoose), m_haveLoosePrior(true)
    {
    }

    virtual BackgroundComponent *createInstance(unsigned int nOmegaHat, 
        unsigned int nLeptons, 
        const std::vector<bool> &qIsTight) const
    {
      if (m_haveLoosePrior) {
        return (BackgroundComponent*) new ComponentWithPrior(nOmegaHat, nLeptons, qIsTight, *m_priorTight, *m_priorLoose);
      } else {
        return (BackgroundComponent*) new ComponentWithPrior(nOmegaHat, nLeptons, qIsTight, *m_priorTight);
      }
    }

  private:
    PriorPtr m_priorTight;
    PriorPtr m_priorLoose;
    bool m_haveLoosePrior;
};

typedef GenericComponentFactory<BackgroundComponent> BackgroundComponentFactory;
typedef GenericComponentFactory<ComponentWithPrior> ComponentWithPriorFactory;

typedef std::shared_ptr<const IComponentFactory> ComponentFactoryPtr;


#endif // COMPONENTS_H
