// Top-level utility to compute MLE fake rate in some signal region, given
// the corresponding set of tight and loose events piped via stdin.
//
// i.e. usage is:
//
// > ./moo complexEfficiencyConfig.txt < myEventFile
//
// > head -n 1 myEventFile
// 1 0 3 7
//
// Each line of the file corresponds to
// <tight/loose 1>  <tight/loose 2>    <category 1>  <category 2>
//
// where "1" and "2" refer to the first and second lepton in the event
// respectively (e.g. ordered by pT).
//
// TODO: this method is fairly easily generalisable to cases with varying
// numbers of leptons, but right now this isn't handled. By default the code is
// set up to expect two leptons in each line, but that can be changed to another
// *fixed* number trivially in the source below. e.g. if this were increased to
// 3 then the input file format would look like
//
// 1 0 1 20 9 2
//
// "efficiencyConfig.txt" is a file that encodes the efficiencies for each category,
// see "sampleEfficiencyConfig.txt" for an example and syntax.
//
// Improvement 1: Figure out the number of categories on the fly
// Imporvement 2: Remove redundant categories [note to self: is this OK though??]
//
//
// In all cases where the hard-coding appears should hopefully be clear

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>


#include "ComplexEfficiencyInfo.h"
#include "MatrixMethodApplicator.h"
#include "MMSampler.h"
#include "MMImportanceSampler.h"
#include "MMExactPosteriorSampler.h"
#include "MMGibbsPosteriorSampler.h"
#include "utils.h"

namespace RunType {
  enum e {
    MATRIX_METHOD, MM_NAIVE_SAMPLER, MM_CONSTRAINED_WEIGHTED_SAMPLER,
    MM_IMPORTANCE_SAMPLER, MM_RELOADED_SAMPLER, MM_GIBBS_SAMPLER,
    MM_SPARSE_GIBBS_SAMPLER
  };
}

void printMMSamples(const RunType::e& runType,
                    const ComplexEfficiencyInfo& efficiencyInfo,
                    std::istream& stream, unsigned int nSamples = -1,
                    unsigned int nSamplesPerEfficiencyPoint = 100
                    );


unsigned int numLeptonsInStream(std::istream& stream)
{
  std::string line;
  int pos = stream.tellg();
  std::getline(stream, line);
  stream.seekg(pos);

  std::vector<std::string> elems;
  split(line, ' ', elems); 
  if (elems.size() % 2 != 0) {
    throw std::runtime_error("ERROR: odd number of columns per line in event file!");
  }
  unsigned int nLeptons = elems.size() / 2;

  return nLeptons;
}


void printMMSamples(const RunType::e& runType,
                    const ComplexEfficiencyInfo& efficiencyInfo,
                    std::istream& stream, unsigned int nSamples, unsigned int nSamplesPerEfficiencyPoint)
{
  const unsigned int nLeptons = numLeptonsInStream(stream);

  if (runType == RunType::MATRIX_METHOD) {
    MatrixMethodApplicator mm(nLeptons, efficiencyInfo);
    mm.setRealEfficiencyPriors(efficiencyInfo.realEffMeans, efficiencyInfo.realEffUncs);
    mm.setFakeEfficiencyPriors(efficiencyInfo.fakeEffMeans, efficiencyInfo.fakeEffUncs);
    mm.grabObservedEvents(stream);
    // TODO print out the result
  } else if (runType == RunType::MM_NAIVE_SAMPLER) {
    // TODO -- make it ignore any constraints, and not bother about
    // event weights
    MMSampler naiveSampler(nLeptons, efficiencyInfo);
    naiveSampler.grabObservedEvents(stream);
    naiveSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_CONSTRAINED_WEIGHTED_SAMPLER) {
    // TODO -- make it **not** ignore any constraints
    MMSampler constraintedWeightedSampler(nLeptons, efficiencyInfo);
    constraintedWeightedSampler.grabObservedEvents(stream);
    constraintedWeightedSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_IMPORTANCE_SAMPLER) {
    MMImportanceSampler importanceSampler(nLeptons, efficiencyInfo);
    importanceSampler.grabObservedEvents(stream);
    importanceSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_RELOADED_SAMPLER) {
    MMExactPosteriorSampler mmReloadedSampler(nLeptons, efficiencyInfo);
    mmReloadedSampler.grabObservedEvents(stream);
    mmReloadedSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_GIBBS_SAMPLER) {
    MMGibbsPosteriorSampler mmGibbsSampler(nLeptons, efficiencyInfo, nSamplesPerEfficiencyPoint);
    mmGibbsSampler.grabObservedEvents(stream);
    mmGibbsSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_SPARSE_GIBBS_SAMPLER) {
    MMGibbsPosteriorSampler mmSparseGibbsSampler(nLeptons, efficiencyInfo, nSamplesPerEfficiencyPoint, true);
    mmSparseGibbsSampler.grabObservedEvents(stream);
    mmSparseGibbsSampler.drawSamples(nSamples);
  } else {
    std::cerr << runType << std::endl;
    throw std::runtime_error("Unhandled run type");
  }
}



int main(int argc, char* argv[])
{
  if (argc < 3) {
    std::cerr << "Usage: ./sampler <RunType> <efficiencyInfo> <nSamples> <nSamplesPerEfficiencyPoint>"  << std::endl << std::endl
    << "  0: MATRIX_METHOD" << std::endl
    << "  1: MM_NAIVE_SAMPLER" << std::endl
    << "  2: MM_CONSTRAINED_WEIGHTED_SAMPLER" << std::endl
    << "  3: MM_IMPORTANCE_SAMPLER" << std::endl
    << "  4: MM_RELOADED_SAMPLER" << std::endl
    << "  5: MM_GIBBS_SAMPLER" << std::endl
    << "  6: MM_SPARSE_GIBBS_SAMPLER" << std::endl << std::endl
    << "filename of efficiency configuration file" << std::endl << std::endl
    << "the number of samples to print (-1 for infinite, default)" << std::endl << std::endl
    << "the number of samples to make per efficiency draw (Gibbs) (default 100)" << std::endl;
    exit(EXIT_FAILURE);
  }
  RunType::e runType = static_cast<RunType::e>(std::stoi(argv[1]));
  std::string efficiencyConfigFilename(argv[2]);
  unsigned int nSamples = -1;
  if (argc > 3) {
    nSamples = std::stoi(argv[3]);
  }

  unsigned int nSamplesPerEfficiencyPoint = 100;
  if (argc > 4) {
    nSamplesPerEfficiencyPoint = std::stoi(argv[4]);
  }
  
  ComplexEfficiencyInfo efficiencyInfo(efficiencyConfigFilename);

  std::ostringstream stream;
  stream << std::cin.rdbuf();
  std::string input = stream.str();
  std::istringstream readStream(input);

  printMMSamples(runType, efficiencyInfo, readStream, nSamples, nSamplesPerEfficiencyPoint);

  return EXIT_SUCCESS;
}
