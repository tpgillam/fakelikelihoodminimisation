#include "MiniMinimum.h"
#include "Minimiser.h"
#include "Components.h"

#include <boost/algorithm/string/predicate.hpp>

MiniMinimum::MiniMinimum(const MiniMinimum &minimumToCopy)
  : m_minuitMinimum(minimumToCopy.m_minuitMinimum),
  m_customUserParameters(minimumToCopy.m_customUserParameters),
  m_isReducedMinimum(minimumToCopy.m_isReducedMinimum),
  m_minf(minimumToCopy.m_minf),
  m_isValid(minimumToCopy.m_isValid),
  m_nFuncCalls(minimumToCopy.m_nFuncCalls)
{
}

MiniMinimum::MiniMinimum(const std::shared_ptr<MiniMinimum> &minimumToCopy)
  : m_minuitMinimum(minimumToCopy->m_minuitMinimum),
  m_customUserParameters(minimumToCopy->m_customUserParameters),
  m_isReducedMinimum(minimumToCopy->m_isReducedMinimum),
  m_minf(minimumToCopy->m_minf),
  m_isValid(minimumToCopy->m_isValid),
  m_nFuncCalls(minimumToCopy->m_nFuncCalls)
{
}

MiniMinimum::MiniMinimum(const ROOT::Minuit2::FunctionMinimum &minuitMinimum)
  : m_minuitMinimum(std::shared_ptr<const ROOT::Minuit2::FunctionMinimum>(new ROOT::Minuit2::FunctionMinimum(minuitMinimum))),
  m_customUserParameters(minuitMinimum.UserParameters()), m_isReducedMinimum(false)
{
}

MiniMinimum::MiniMinimum(std::shared_ptr<const ROOT::Minuit2::FunctionMinimum> minuitMinimum)
  : m_minuitMinimum(minuitMinimum),
  m_customUserParameters(minuitMinimum->UserParameters()), m_isReducedMinimum(false)
{
}

MiniMinimum::MiniMinimum(double minf, const ROOT::Minuit2::MnUserParameters &minParams, bool isValid, unsigned int nFuncCalls)
  : m_customUserParameters(minParams),
  m_isReducedMinimum(true),
  m_minf(minf),
  m_isValid(isValid),
  m_nFuncCalls(nFuncCalls)
{
}
