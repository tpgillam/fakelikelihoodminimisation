#ifndef MM_GIBBS_POSTERIOR_SAMPLER_H
#define MM_GIBBS_POSTERIOR_SAMPLER_H

#include "MMBaseSampler.h"
#include "TLSpaceSolver.h"

#include <boost/math/distributions/gamma.hpp>


class MMGibbsPosteriorSampler : public MMBaseSampler {
  public:
    MMGibbsPosteriorSampler(unsigned int nLeptons,
                            const ComplexEfficiencyInfo& efficiencyInfo,
                            int nSamplesPerEfficiencyPoint,
                            bool sparseMode = false);
    ~MMGibbsPosteriorSampler();

    virtual void drawSamples(int nSamples = -1);


  private:
    void clearDistributions();
    void resizeStorageVectors();
    void initialiseBeforeDrawing();
    void initialiseTLRateDistributions(); 
    void drawEfficiencies();
    void populateCurrentLeptonEfficiencies(unsigned int ohIndex);
    void initialiseTLRatesToAllowedPoint(unsigned int ohIndex);
    void rationaliseLowerAndUpperBounds(double& lower, double& upper);
    void drawGibbsSamplesForSetEfficiencies(int nSamples);
    double drawAndFindFakeTightRate(unsigned int ohIndex);
    void makeTLRateGibbsStep(unsigned int ohIndex);
    double fakeTightRateForCurrentCategory(unsigned int ohIndex) const;
    bool isThisAFakeEvent(const RFConfigVector& rfConfig) const;
    bool isThisATightEvent(const TLConfigVector& tlConfig) const;


    // Compute the matrix element involved in the matrix method
    double matrixElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;
    
    // Compute the element of the inverse matrix involved in the matrix method
    double inverseElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig
        ) const;


  private:
    // Rate distributions -- same indexing as for m_nObs
    // (indexed by event category, then tight/loose category)
    std::vector<std::vector<boost::math::gamma_distribution<> *> > m_rateTLDists;

    // Temporary storage of efficiencies for a given draw
    std::vector<double> m_realEffs;
    std::vector<double> m_fakeEffs;
    
    // Temporary rate storage for current event category, indexed over TL
    // config
    std::vector<std::vector<double> > m_ratesOHTL;

    // Efficiency values index over leptons for the current event category being
    // considered
    std::vector<double> m_lepRealEffs;
    std::vector<double> m_lepFakeEffs;

    // Simplex solver in TL space
    TLSpaceSolver* m_simplexSolver;

    // Number of TL draws to make per efficiency draw
    int m_nSamplesPerEfficiencyPoint;

    // Whether to ignore event categories for which there are no observed events
    bool m_sparseMode;
};


#endif // MM_GIBBS_POSTERIOR_SAMPLER_H
