#include "CLsCalculatorFakeNLL.h"
#include "utils.h"

#include "Minuit2/MnPrint.h"
#include <cmath>
#include <map>

CLsCalculatorFakeNLL::CLsCalculatorFakeNLL(Minimiser &minimiser, const std::string &poiName, 
    std::shared_ptr<MiniMinimum> startingPoint)
  : m_asimovBestGlobalNLL(0.0),
    m_asimovMuHat(0.0),
    m_minimiser(&minimiser), m_poiName(poiName),
    m_firstSetOfToys(true)
{
  m_minimiser->clearSavedMinima();
  m_bestGlobalNLL = m_minimiser->getDecentNLL(numRestarts(), 1, 0.1, startingPoint);
  m_globalMinimum = m_minimiser->bestUniqueMinimumByRates();
  m_muHat = m_minimiser->bestUniqueMinimumByRates()->UserParameters().Value(m_poiName);
  // std::cout << "m_bestGlobalNLL = " << m_bestGlobalNLL << std::endl;
  // std::cout << "m_globalMinimum = " << m_globalMinimum->UserParameters() << std::endl;
  // std::cout << "m_muHat = " << m_muHat << std::endl;
}

unsigned int CLsCalculatorFakeNLL::numRestarts() const
{
  unsigned int result = 3;
  if (m_minimiser->likelihoodComputationMode() != LikelihoodComputationMode::FULL) {
    result = 3;
  }
  return result;
}

double CLsCalculatorFakeNLL::q0()
{
  if (m_muHat < 0) {
    return 0.0;
  }
  m_minimiser->clearSavedMinima();
  double mu = 0.0;
  m_minimiser->fix(m_poiName, mu);
  double bestLocalNLL = m_minimiser->getDecentNLL(1, 1);

  double result = 2 * (bestLocalNLL - m_bestGlobalNLL);
  result = ensureTestStatisticPositive(result);
  m_minimiser->clearSavedMinima();
  m_minimiser->releaseAll();
  return result;
}

double CLsCalculatorFakeNLL::qMu(double mu, bool asimovMode, std::shared_ptr<MiniMinimum> startingPoint)
{
  // We should destroy evidence of any previously stored minimum now
  if (!asimovMode) {
    m_qMuConditionalMinimum = 0;
  }

  // std::cout << "qMu(" << mu << ", " << asimovMode << ", " << (bool) startingPoint << ")" << std::endl;
  double muHat, bestGlobalNLL;
  if (asimovMode) {
    muHat = m_asimovMuHat;
    bestGlobalNLL = m_asimovBestGlobalNLL;
  } else {
    muHat = m_muHat;
    bestGlobalNLL = m_bestGlobalNLL;
  }

  // std::cout << muHat << " " << mu << std::endl;
  if (muHat > mu) {
    return 0.0;
  }
  m_minimiser->clearSavedMinima();
  m_minimiser->fix(m_poiName, mu);
  double bestLocalNLL = m_minimiser->getDecentNLL(numRestarts(), 1, 0.1, startingPoint);
  if (!asimovMode) {
    // std::cout << "We're setting m_qMuConditionalMinimum" << std::endl;
    m_qMuConditionalMinimum = m_minimiser->bestUniqueMinimumByRates();
  }

  double result = 2 * (bestLocalNLL - bestGlobalNLL);
  result = ensureTestStatisticPositive(result);

  m_minimiser->clearSavedMinima();
  m_minimiser->releaseAll();
  // std::cout << "qMu: " << result << std::endl;
  return result;
}

double CLsCalculatorFakeNLL::ensureTestStatisticPositive(double result) const
{
  if (result < 0.0) {
    result = 0.0;
    if (result < -0.01) {
      std::cerr << "Warning: global maximum might not be global" << std::endl;
      std::cerr << m_globalMinimum->UserParameters();
      std::cerr << m_minimiser->bestUniqueMinimumByRates()->UserParameters();
    }
  }
  return result;
}

unsigned int CLsCalculatorFakeNLL::nObsTot() const
{
  return m_minimiser->nObsTot();
}

unsigned int CLsCalculatorFakeNLL::nObsTight() const
{
  return m_minimiser->getFcn()->nObsTight();
}

double CLsCalculatorFakeNLL::estimateSigmaFromAsimovDataset(double mu, double muPrime)
{
  MiniMinimum asimovParams(m_qMuConditionalMinimum);
  asimovParams.UserParameters().SetValue(m_poiName, muPrime);

  // Plug params into fcn
  BasicFakeNLL *fcn = m_minimiser->getFcn();
  (*fcn)(asimovParams.params());
  fcn->storeCurrentParametersAsAsimov();

  fcn->enableAsimovMode();
  m_asimovBestGlobalNLL = (*fcn)(asimovParams.params());
  m_asimovMuHat = muPrime;
  
  // Compute qMu, but ignoring the best global NLL that we've cached previously
  double qMuAsimov = qMu(mu, true);
  fcn->disableAsimovMode();

  return fabs(mu) / sqrt(qMuAsimov);
}

void CLsCalculatorFakeNLL::startImportanceSamplingRun()
{
  m_firstSetOfToys = true;
  m_toyRuns.clear();
}

double CLsCalculatorFakeNLL::toysPMuImportanceSampling(double mu, double muPrime, unsigned int numToys)
{
  // Slight botch to fix 0, 0 at something uninformative
  if (mu == 0.0 && muPrime == 0.0) {
    return 0.5;
  }

  double qMuObs = qMu(mu);
  MiniMinimum toyParams(m_qMuConditionalMinimum);
  toyParams.UserParameters().SetValue(m_poiName, muPrime);

  BasicFakeNLL *fcn = m_minimiser->getFcn();
  unsigned int numAboveObs(0);
  double sumWeights(0.0);
  double sumWeightsSquared(0.0);

  fcn->enableAsimovMode();

  if (m_firstSetOfToys) {
    for (unsigned int i = 0; i < numToys; ++i) {
      fcn->throwAndStoreToys(toyParams.params());

      ToyRun toyRun;
      toyRun.nObsByQ = fcn->asimovNObsByQ();
      toyRun.ratesByQ = fcn->ratesByQ();
      toyRun.nll = fcn->valueForPoissonTerms();

      CLsCalculatorFakeNLL tempCLsCalculator(*m_minimiser, m_poiName);
      double testQMu = tempCLsCalculator.qMu(mu, false);
      // std::cout << "Test " << i << ": " << testQMu << std::endl;
      if (testQMu > qMuObs) {
        ++numAboveObs;
      }
      double currMean = ((double) numAboveObs) / ((double) i);
      double unc = sqrt(  currMean * (1.0 - currMean) / ( (double) i - 1.0 ));
      double relUnc = unc / currMean;
      if ((relUnc > 0.05) && (unc > 0.0025) && (i > (numToys - 2))) {
        ++numToys;
      }

      toyRun.globalMinimum = tempCLsCalculator.m_globalMinimum;
      toyRun.qMuConditionalMinimum = tempCLsCalculator.m_qMuConditionalMinimum;
      m_toyRuns.push_back(toyRun);
    }
  } else {
    for (const auto &toyRun : m_toyRuns) {
      fcn->setParameters(toyParams.params());
      fcn->setAsimovObsNByQ(toyRun.nObsByQ);
      double nll = fcn->valueForPoissonTerms();
      // const auto &newParamThings = fcn->ratesByQ();

      CLsCalculatorFakeNLL tempCLsCalculator(*m_minimiser, m_poiName, toyRun.globalMinimum);
      double testQMu = tempCLsCalculator.qMu(mu, false, toyRun.qMuConditionalMinimum);
      // std::cout << "Test " << i << ": " << testQMu << std::endl;
      if (testQMu > qMuObs) {
        ++numAboveObs;
        double weight = exp(-(toyRun.nll - nll));
        if (weight  > 1000) {
          // std::cout << "Being debug info:" << std::endl;
          // std::cout << "Toys: " << toyRun.nObsByQ << std::endl;
          // std::cout << "Old params: " << toyRun.ratesByQ << std::endl;
          // std::cout << "New params: " << newParamThings << std::endl;
          // std::cout << "New params (after fit): " << fcn->ratesByQ() << std::endl;
          // std::cout << "Old probability: " << exp(-toyRun.nll) << std::endl;
          // std::cout << "New probability: " << exp(-nll) << std::endl;
          // std::cout << "New probability (after fit): " << exp(-fcn->valueForPoissonTerms()) << std::endl;
          // std::cout << nll << " " << toyRun.nll << " " << ", Weight: " << weight << std::endl;
          // std::cout << std::endl;
          weight = 0;
        }
        sumWeights += weight;
        sumWeightsSquared += weight*weight;
      }

      // double currMean = ((double) numAboveObs) / ((double) i);
      // double unc = sqrt(  currMean * (1.0 - currMean) / ( (double) i - 1.0 ));
      // double relUnc = unc / currMean;
      // if ((relUnc > 0.05) && (unc > 0.0025) && (i > (numToys - 2))) {
      //   ++numToys;
      // }
    }

  }
  fcn->disableAsimovMode();

  if (m_firstSetOfToys) {
    m_firstSetOfToys = false;
    // std::cout << "[" << numAboveObs << " / " << numToys << "]" << std::endl;
    return ((double) numAboveObs) / ((double) numToys);
  } else {
    // std::cout << "[" << sumWeights << " / " << m_toyRuns.size() << "]" << std::endl;
    // std::cout << "numToys         : " << m_toyRuns.size() << std::endl;
    // std::cout << "effective number: " << sumWeights * sumWeights / sumWeightsSquared << std::endl;
    return (sumWeights / m_toyRuns.size());
  }
}

double CLsCalculatorFakeNLL::toysPMu(double mu, double muPrime, unsigned int numToys)
{
  bool randomStartingLocations = (m_minimiser->likelihoodComputationMode() != LikelihoodComputationMode::FULL);

  // Slight botch to fix toys for small mu
  if (mu <= m_muHat) {
    return 0.5;
  }
  // if (mu == 0.0 && muPrime == 0.0) {

  double qMuObs = qMu(mu);
  std::shared_ptr<MiniMinimum> minimumToCopy;
  if (!m_qMuConditionalMinimum) {
    minimumToCopy = m_globalMinimum;
  } else {
    minimumToCopy = m_qMuConditionalMinimum;
  }
  MiniMinimum toyParams(minimumToCopy);
  // std::cout << minimumToCopy;
  toyParams.UserParameters().SetValue(m_poiName, muPrime);

  BasicFakeNLL *fcn = m_minimiser->getFcn();
  unsigned int numAboveObs(0);
  fcn->enableAsimovMode();
  for (unsigned int i = 0; i < numToys; ++i) {
    fcn->throwAndStoreToys(toyParams.params());
    // std::cout << toyParams.params() << std::endl;
    // std::cout << fcn->asimovNObsTight() << std::endl;
    double testQMu;
    if (randomStartingLocations) {
      CLsCalculatorFakeNLL tempCLsCalculator(*m_minimiser, m_poiName);
      testQMu = tempCLsCalculator.qMu(mu, false);
    } else {
      CLsCalculatorFakeNLL tempCLsCalculator(*m_minimiser, m_poiName, m_globalMinimum);
      testQMu = tempCLsCalculator.qMu(mu, false, m_qMuConditionalMinimum);
    }
    // std::cout << "Test " << i << ": " << testQMu << "  vs  " << qMuObs << std::endl;
    if (testQMu >= qMuObs) {
      ++numAboveObs;
    }
    double currMean = ((double) numAboveObs) / ((double) i);
    double unc = sqrt(  currMean * (1.0 - currMean) / ( (double) i - 1.0 ));
    double relUnc = unc / currMean;
    if ((relUnc > 0.05) && (unc > 0.0025) && (i > (numToys - 2))) {
      ++numToys;
    }
  }
  fcn->disableAsimovMode();

  // std::cout << "[" << numAboveObs << " / " << numToys << "]" << std::endl;
  return ((double) numAboveObs) / ((double) numToys);
}


