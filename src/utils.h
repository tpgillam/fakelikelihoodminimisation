#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <cmath>
#include <cassert>
#include <memory>
#include <iostream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/math/distributions/normal.hpp>

#define SQRT_2PI 2.50662827463100050241576528

double randUniform(double maxValue = 1.0);
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

template <class T, class U>
bool nearlyEqual(T a, U b, double tolerance=0.0000001)
{
  return (std::abs(a - b) < tolerance);
}

template <class T>
std::vector<T> &vectorSum(std::vector<T> &lhs, const std::vector<T> &rhs)
{
  assert(lhs.size() == rhs.size());
  for (unsigned int i = 0; i < lhs.size(); ++i) {
    lhs[i] += rhs[i];
  }
  return lhs;
}

template <class T, class U>
std::vector<T> &vectorMult(std::vector<T> &lhs, const U &rhs)
{
  for (unsigned int i = 0; i < lhs.size(); ++i) {
    lhs[i] *= rhs;
  }
  return lhs;
}

template <class T>
std::string toStr(T elem)
{
  std::stringstream ss;
  ss << elem;
  return ss.str();
}

template <class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v);
template <class T, class U>
std::ostream& operator<<(std::ostream& os, const std::map<T, U>& m);
template <class T, class U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& v);

template < class T >
std::ostream& operator << (std::ostream& os, const std::vector<T>& v) 
{
  os << "{";
  bool firstTime = true;
  for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii) {
    if (firstTime) {
      os << *ii;
      firstTime = false;
    } else {
      os << ", " << *ii;
    }
  }
  os << "}";
  return os;
}

template < class T, class U >
std::ostream& operator << (std::ostream& os, const std::map<T, U>& m) 
{
  for (auto& kv : m) {
    os << kv.first << " has value " << kv.second << std::endl;
  }
  return os;
}

template < class T , class U >
std::ostream& operator << (std::ostream& os, const std::pair<T, U>& v) 
{
  os << "<" << v.first << ", " << v.second << ">";
  return os;
}

#ifndef NO_MINIMISING
class MiniMinimum;
std::ostream& operator << (std::ostream& os, const std::shared_ptr<MiniMinimum>& minimum);
#endif



// Random number generator and uniform distribution
static boost::random::mt19937 rng;
static boost::random::uniform_real_distribution<double> uniformZeroOne(0., 1.);


template <class Distribution>
double drawTruncatedWithQuantiles(const Distribution* dist,
                                 double qlower = 0., double qupper = 1.) {
 // A bit of a hack..
 if (nearlyEqual(qupper, qlower) && nearlyEqual(qupper, 1.)) {
   // std::cerr << "WARNING: qupper = qlower = 1,    setting qlower = 0.999" << std::endl;
   qlower = 0.999;
 }
 double qrand = qlower + uniformZeroOne(rng) * (qupper - qlower);
 return boost::math::quantile(*dist, qrand);
}

template <class Distribution>
double drawTruncatedAbove(const Distribution* dist, double lower) {
 double qlower = boost::math::cdf(*dist, lower);
 double qupper = 1.;
 return drawTruncatedWithQuantiles(dist, qlower, qupper);
}

template <class Distribution>
double drawTruncatedBelow(const Distribution* dist, double upper) {
 double qlower = 0.;
 double qupper = boost::math::cdf(*dist, upper);
 return drawTruncatedWithQuantiles(*dist, qlower, qupper);
}

template <class Distribution>
double drawTruncatedBetween(const Distribution* dist, double lower,
                           double upper) {
 double qlower = boost::math::cdf(*dist, lower);
 double qupper = boost::math::cdf(*dist, upper);
 return drawTruncatedWithQuantiles(dist, qlower, qupper);
}


#endif // UTILS_H
