#ifndef LLH_MINIMISING_H
#define LLH_MINIMISING_H

#include "BasicFakeNLL.h"
#include "Minimiser.h"

#include <string>
#include <vector>

#include "Minuit2/FunctionMinimum.h"

class TCanvas;

class Param {
  public:
    Param(unsigned int _index, const std::string &_name, const std::string &_label, double _maxValue = 1.0)
      : index(_index), name(_name), label(_label), maxValue(_maxValue)
    {
    }

    unsigned int index;
    std::string name;
    std::string label;
    double maxValue;
};

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

void makeGraphs(const std::shared_ptr<MiniMinimum> &minimum,
    Minimiser &minimiser, 
    const std::string &dirname);

void writeSummaryFile(const std::shared_ptr<MiniMinimum> &minimum,
    Minimiser &minimiser, 
    const std::string &dirname,
    const std::string &filename="_summary.txt"
    );

void draw1DComparison(const BasicFakeNLL *fcn, 
    const std::vector<double> &defaultParams, 
    const Param &var1,
    double maxNLL = -1.0,
    TCanvas *canvas = NULL,
    const std::string &dirname = "",
    const std::string &outputName = ""
    );

void draw2DComparison(const BasicFakeNLL *fcn, 
    const std::vector<double> &defaultParams, 
    const Param &var1, const Param &var2, 
    double maxNLL = -1.0,
    TCanvas *canvas = NULL,
    const std::string &dirname = "",
    const std::string &outputName = ""
    );

void draw2DComparisonMinimisingEverywhere(Minimiser &minimiser, 
    const Param &var1, const Param &var2, 
    double maxNLL = -1.0,
    TCanvas *canvas = NULL,
    const std::string &dirname = "",
    const std::string &outputName = ""
    );

void simpleCLsForMMLikeThings(std::istream &stream, const ComponentWithPrior::BasicPrior &priorFake, bool doToys=true);

void testMinimiser();


#endif // LLH_MINIMISING_H
