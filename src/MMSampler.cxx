#include "MMSampler.h"

#include "utils.h"

#include <algorithm>
#include <exception>

MMSampler::MMSampler(unsigned int nLeptons,
                     const ComplexEfficiencyInfo &efficiencyInfo)
    : m_nLeptons(nLeptons),
      m_matrixMethod(false, nLeptons),
      m_efficiencyInfo(efficiencyInfo) {

  m_nEventCategories = pow(efficiencyInfo.nCategories(), nLeptons);
  m_nTLConfigs = pow(2, nLeptons);

  m_nObs.resize(m_nEventCategories);
  for (auto& nObsForCategory : m_nObs) {
    nObsForCategory.resize(m_nTLConfigs, 0);
  }

  m_rates.resize(m_nTLConfigs);

  const unsigned int nReal = efficiencyInfo.nRealEfficiencies();
  const unsigned int nFake = efficiencyInfo.nFakeEfficiencies();
  m_realEffs.resize(nReal, 0.0);
  m_fakeEffs.resize(nFake, 0.0);

  computeLookups();
}


MMSampler::~MMSampler()
{
  cleanDistributions();
}


void MMSampler::cleanDistributions()
{
  for (auto& dist : m_realEffDists) {
    delete dist;
  }
  for (auto& dist : m_fakeEffDists) {
    delete dist;
  }
  for (auto& vec : m_rateDists) {
    for (auto& dist : vec) {
      delete dist;
    }
  }
}


void MMSampler::computeLookups()
{
  m_ohConfigs.resize(m_nEventCategories);
  m_tlConfigs.resize(m_nTLConfigs);

  TLConfigVector tlConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_efficiencyInfo.nCategories());

  unsigned int i = 0;
  while (!ohConfig.isLast()) {
    m_ohConfigs[i] = ohConfig;
    ++i; ohConfig.increment();
  }

  i = 0;
  while (!tlConfig.isLast()) {
    m_tlConfigs[i] = tlConfig;
    ++i; tlConfig.increment();
  }
}


void MMSampler::grabObservedEvents(std::istream &stream) {
  std::string lineInput;
  std::vector<std::string> elems;


  TLConfigVector tlConfig(m_nLeptons);
  OHConfigVector ohConfig(m_nLeptons, m_efficiencyInfo.nCategories());

  using namespace TLConfig;
  while (std::getline(stream, lineInput)) {
    split(lineInput, ' ', elems); 
    for (unsigned int i = 0; i < m_nLeptons; ++i) {
      if (elems[i] == "1") {
        tlConfig[i] = TIGHT;
      } else {
        tlConfig[i] = LOOSE;
      }
      ohConfig[i] = std::stoi(elems[m_nLeptons+i]);
    }

    const auto& ohBegin = m_ohConfigs.begin();
    unsigned int ohIndex =
        std::find(ohBegin, m_ohConfigs.end(), ohConfig) - ohBegin;
    const auto& tlBegin = m_tlConfigs.begin();
    unsigned int tlIndex =
        std::find(tlBegin, m_tlConfigs.end(), tlConfig) - tlBegin;

    if (ohIndex >= m_nEventCategories) {
      throw std::runtime_error("ohIndex too big!");
    }
    if (tlIndex >= m_nTLConfigs) {
      std::cerr << "tlConfig is: " << tlConfig << std::endl;
      std::cerr << "tlIndex is: " << tlIndex << std::endl;
      throw std::runtime_error("tlIndex too big!");
    }
    ++m_nObs[ohIndex][tlIndex];
  }
  
  // std::cout << "ohConfigs: " << m_ohConfigs << std::endl;
  // std::cout << "tlConfigs: " << m_tlConfigs << std::endl;
  // std::cout << m_nObs << std::endl;
}


void MMSampler::drawSamples(int nSamples)
{
  bool adInfinitum = (nSamples == -1);
  unsigned int nAttempts;

  initialiseRandomGenerators();

  while (adInfinitum || (nSamples > 0)) {

    double logWeight = 0.0;
    double fakeTightRate = 0.0;

    logWeight += log(drawEfficiencies());
    // std::cout << "Efficiency logWeight: " << logWeight << std::endl;
    // std::cout << "Num event configs: " << m_ohConfigs.size() << std::endl;
    // Choose rates for each category at a time
    for (unsigned int ohIndex = 0; ohIndex < m_ohConfigs.size(); ++ohIndex) {
      if (!drawCategoryRates(ohIndex, &nAttempts)) {
        goto tryAgain;
      }
      // std::cout << "  nAttempts: " << nAttempts << std::endl;
      logWeight -= log(static_cast<double>(nAttempts));
      fakeTightRate += computeFakeTightRate(ohIndex);
    }

    std::cout << fakeTightRate << " " << exp(logWeight) << std::endl;
    if (fakeTightRate < 0.0) {
      std::cout << " Oh bother " << std::endl << std::endl;
      std::cout << "real effs: " << m_realEffs << std::endl;
      std::cout << "fake effs: " << m_fakeEffs << std::endl;

      throw std::runtime_error("But... but!!");
    }

    --nSamples;
    tryAgain:;
  }
}


void MMSampler::initialiseRandomGenerators()
{
  cleanDistributions();


  // Efficiency distributions
  
  m_realEffDists.resize(m_efficiencyInfo.realEffMeans.size(), 0);
  m_fakeEffDists.resize(m_efficiencyInfo.fakeEffMeans.size(), 0);

  for (unsigned int i = 0; i < m_efficiencyInfo.realEffMeans.size(); ++i) {
    double mean = m_efficiencyInfo.realEffMeans[i];
    double sigma = m_efficiencyInfo.realEffUncs[i];
    m_realEffDists[i] = new boost::random::normal_distribution<>(mean, sigma);
  }

  for (unsigned int i = 0; i < m_efficiencyInfo.fakeEffMeans.size(); ++i) {
    double mean = m_efficiencyInfo.fakeEffMeans[i];
    double sigma = m_efficiencyInfo.fakeEffUncs[i];
    m_fakeEffDists[i] = new boost::random::normal_distribution<>(mean, sigma);
  }


  // Rate distributions

  m_rateDists.resize(m_nEventCategories);
  for (auto& vec : m_rateDists) {
    vec.resize(m_nTLConfigs, 0);
  }

  for (unsigned int i = 0; i < m_nEventCategories; ++i) {
    for (unsigned int j = 0; j < m_nTLConfigs; ++j) {
      // // Near zero prior
      // double offset = 0.01;

      // // Jeffrey's prior
      // double offset = 0.5;

      // Uniform prior
      double offset = 1.0;

      m_rateDists[i][j] = new boost::random::gamma_distribution<>(
          static_cast<double>(m_nObs[i][j])+offset, 1.0);
    }
  }

}


double MMSampler::drawEfficiencies()
{
  // m_realEffs = m_efficiencyInfo.realEffMeans;
  // m_fakeEffs = m_efficiencyInfo.fakeEffMeans;
  // m_matrixMethod.setRealEfficiencyPriors(m_realEffs);
  // m_matrixMethod.setFakeEfficiencyPriors(m_fakeEffs);
  // return 1.0;

  double weight = 1.0;

  unsigned int nAttempts;

  const unsigned int nReal = m_efficiencyInfo.nRealEfficiencies();
  const unsigned int nFake = m_efficiencyInfo.nFakeEfficiencies();
  for (unsigned int i = 0; i < nReal; ++i) {
    nAttempts = 0;
    do {
      m_realEffs[i] = (*m_realEffDists[i])(m_rng);
      ++nAttempts;
    } while (m_realEffs[i] < 0.0);
    weight *= 1.0 / static_cast<double>(nAttempts);
  }

  for (unsigned int i = 0; i < nFake; ++i) {
    nAttempts = 0;
    do {
      m_fakeEffs[i] = (*m_fakeEffDists[i])(m_rng);
      ++nAttempts;
    } while (m_fakeEffs[i] < 0.0);
    weight *= 1.0 / static_cast<double>(nAttempts);
  }

  m_matrixMethod.setRealEfficiencyPriors(m_realEffs);
  m_matrixMethod.setFakeEfficiencyPriors(m_fakeEffs);
  
  // TODO -- do we need to watch out for fakeEff >= realEff for a given
  // category?
  return weight;
}


bool MMSampler::drawCategoryRates(unsigned int ohIndex, unsigned int* nAttempts)
{
  *nAttempts = 0;
  // TODO tune me!!!
  const unsigned int attemptThreshold = 1000;

  do {
    for (unsigned int i = 0; i < m_nTLConfigs; ++i) {
      m_rates[i] = (*m_rateDists[ohIndex][i])(m_rng);
    }

    // std::cout << m_nObs[ohIndex] << "   vs   " << m_rates << std::endl;

    ++(*nAttempts);

    // See if we're failing miserably here
    if (*nAttempts > attemptThreshold) {
      // std::cout << "  NOTE!! Bailing out of drawCategoryRates" << std::endl;
      return false;
    }
  } while (!ratesSelfConsistent(ohIndex));

  return true;
}


bool MMSampler::ratesSelfConsistent(unsigned int ohIndex)
{
  // std::cout << "ratesSelfConsistent" << std::endl;
  // Naive matrix method mode
  // return true;


  // Look up values of real & fake effs for each lepton
  std::vector<double> effReals(m_nLeptons);
  std::vector<double> effFakes(m_nLeptons);
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    const auto& lepCategory = m_ohConfigs[ohIndex][i];

    effReals[i] = m_realEffs[m_efficiencyInfo.categoryToRealEff[lepCategory]];
    effFakes[i] = m_fakeEffs[m_efficiencyInfo.categoryToFakeEff[lepCategory]];
  }

  RFConfigVector rfConfig(m_nLeptons);

  while (!rfConfig.isLast()) {
    double rate = 0.0;
    unsigned int tlIndex = 0;
    // std::cout << rfConfig << std::endl;
    // std::cout << m_rates << std::endl;
    // std::cout << effReals << "   " << effFakes << std::endl;
    for (const auto& tlConfig : m_tlConfigs) {
      rate += m_rates[tlIndex]*inverseFactor(rfConfig, tlConfig, effReals, effFakes);
      ++tlIndex;
    }
    // std::cout << rate << std::endl;
    if (rate < 0.0) return false;
    rfConfig.increment();
  }

  // std::cout << "DONE!" << std::endl;

  return true;
}


float MMSampler::inverseFactor(
    const RFConfigVector& rfConfig, 
    const TLConfigVector& tlConfig, 
    const std::vector<double>& effReals,
    const std::vector<double>& effFakes
    ) const
{
  float element = 1.f;
  using namespace TLConfig;
  using namespace RFConfig;
  for (unsigned int i = 0; i < m_nLeptons; ++i ) {
    // Pick out cofactor
    if (tlConfig[i] == TIGHT && rfConfig[i] == REAL) {
      element *= 1.f - effFakes[i];
    }
    else if (tlConfig[i] == LOOSE && rfConfig[i] == REAL) {
      element *= - effFakes[i];
    }
    else if (tlConfig[i] == TIGHT && rfConfig[i] == FAKE) {
      element *= - (1.f - effReals[i]);
    }
    else if (tlConfig[i] == LOOSE && rfConfig[i] == FAKE) {
      element *= effReals[i];
    } else {
      throw std::runtime_error("Oh bother");
    }
    
    // Divide by determinant
    element /= effReals[i] - effFakes[i];
  }
  return element;
}



double MMSampler::computeFakeTightRate(unsigned int ohIndex)
{
  double fakeTightRate = 0.0;

  std::vector<GeneralisedMatrixMethod::MatrixMethod::Lepton> leptons(m_nLeptons);

  const auto& ohConfig = m_ohConfigs[ohIndex];
  for (unsigned int i = 0; i < m_nLeptons; ++i) {
    leptons[i].effRealIndex = m_efficiencyInfo.categoryToRealEff[ohConfig[i]];
    leptons[i].effFakeIndex = m_efficiencyInfo.categoryToFakeEff[ohConfig[i]];
  }

  for (unsigned int tlIndex = 0; tlIndex < m_nTLConfigs; ++tlIndex) {
    const auto& tlConfig = m_tlConfigs[tlIndex];
    for (unsigned int i = 0; i < m_nLeptons; ++i) {
      leptons[i].isTight = (tlConfig[i] == TLConfig::TIGHT);
    }

    const auto& results = m_matrixMethod.weightsForInput(leptons);
    for (const auto& result : results) {
      fakeTightRate += m_rates[tlIndex]*result.weight;
    }

  }

  return fakeTightRate;
}

