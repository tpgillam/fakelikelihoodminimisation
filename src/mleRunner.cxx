// Top-level utility to compute MLE fake rate in some signal region, given
// the corresponding set of tight and loose events piped via stdin.
//
// i.e. usage is:
//
// > ./mleRunner sampleEfficiencyConfig.txt < myEventFile
//
// > head -n 1 myEventFile
// 1 0 3 7
//
// Each line of the file corresponds to
// <tight/loose 1>  <tight/loose 2>    <category 1>  <category 2>
//
// where "1" and "2" refer to the first and second lepton in the event
// respectively (e.g. ordered by pT).
//
// TODO: this method is fairly easily generalisable to cases with varying
// numbers of leptons, but right now this isn't handled. By default the code is
// set up to expect two leptons in each line, but that can be changed to another
// *fixed* number trivially in the source below. e.g. if this were increased to
// 3 then the input file format would look like
//
// 1 0 1 20 9 2
//
// "efficiencyConfig.txt" is a file that encodes the efficiencies for each category,
// see "sampleEfficiencyConfig.txt" for an example and syntax.
//
// Improvement 1: Figure out the number of categories on the fly
// Imporvement 2: Remove redundant categories [note to self: is this OK though??]
//
//
// In all cases where the hard-coding appears should hopefully be clear


#include <cstdlib>
#include <iostream>
#include <fstream>

#include <boost/algorithm/string.hpp>

#include "mleRunner.h"
#include "utils.h"

#include "TError.h"

#include "Minimiser.h"


void printFakeMLE(const EfficiencyInfo& efficiencyInfo, std::istream& stream)
{
  const unsigned int nLeptons = 2;

  // Create computation object with all pertinent information
  Minimiser minimiser(efficiencyInfo.nCategories(), nLeptons);
  minimiser.setRealEfficiencyPriors(efficiencyInfo.realEffMeans, efficiencyInfo.realEffUncs);
  minimiser.setFakeEfficiencyPriors(efficiencyInfo.fakeEffMeans, efficiencyInfo.fakeEffUncs);

  // Fill it with the observed events (both tight and loose)
  minimiser.grabObservedEvents(stream);

  // Tell the object about what backgrounds it should expect. This is a very
  // simple scenario where we expect events to either come from a fake-like 
  // process "F", or an unknown signal-like process "R". These names will be
  // used later.
  //
  // A "FakeComponent" differs from a "RealComponent" in the way in which
  // parameters are allowed to vary into regions which generate fake leptons (or
  // not). Specifcally:
  //
  // * "Real" => leptons can only be real, never fake
  // * "Fake" => leptons are allowed to be either real or fake (since, for
  // example in the two lepton case, you want to allow events where just one of
  // the leptons is fake to be classified as fake. With the parametrisation used
  // here it's not feasible to completely prevent real-real events, *however*
  // since we also have the real component these events will tend to be absorbed there.)
  minimiser.addFakeComponent("F");
  minimiser.addRealComponent("R");

  // In the case that you're expecting other irreducible backgrounds that you
  // model by MC (which is very likely in a real scenario), you give the code a
  // better chance of doing something sensible by telling it about these, rather
  // than have it guess based purely on this unknown component that we called
  // "R" above.
  //
  // The following is an example of how you might do that [here it is placing a
  // prior only on the "tight" component of the prediction].
  //
  // const double irredMean = 124.0;
  // const double irredUnc = 5.0;
  // ComponentWithPrior::BasicPrior priorIrredTight({irredMean, irredUnc});
  // ComponentFactoryPtr irredFactoryPtr(new ComponentWithPriorFactory(priorIrredTight));
  // minimiser.addRealComponent("irredBkg", irredFactoryPtr);
  //
  // a constraint can also be placed on the loose events, e.g.
  // ...
  // ComponentWithPrior::BasicPrior priorIrredLoose({irredMean, irredUnc});
  // ComponentFactoryPtr irredFactoryPtr(new ComponentWithPriorFactory(priorIrredTight, priorIrredLoose));
  // ...
  //
  // TODO: in principle there's no reason this could not be extended to more
  // finely grained predictions where appropriate.
  //

  // Now we run the minimisation; here we're only interested in the MLE fake
  // rate so just try our best to find the global minimum of the negative log
  // likelihood.
  //
  // Minuit is run numStarts times each from a new (randomised) starting point,
  // and then the best minimum is selected. Increasing numStarts will likely
  // make this more robust.
  //
  // TODO: I might also be able to do a better job at picking the randomised
  // start points...
  //
  // "strategy" is the method that Minuit uses internally. "2" is highest
  // quality, and for an MLE calculation there shouldn't be any reason to change
  // this.
  const unsigned int numStarts = 30;
  const unsigned int strategy = 2;
  minimiser.getDecentNLL(numStarts, strategy);
  const auto &minimum = minimiser.bestUniqueMinimumByRates();

  // Now print out the central value and uncertainty of the fake component
  std::cout << "Fake tight rate:    " << minimiser.component("F")->rateTight() << " +/- " << minimum->UserParameters().Error("sigmaT_F") << std::endl;

  // In fact one could also print out similar quantities for any other
  // components that have been added like so
  // std::cout << "Real tight rate:    " << minimiser.component("R")->rateTight() << " +/- " << minimum->UserParameters().Error("sigmaT_R") << std::endl;
}


EfficiencyInfo getEfficiencyInfo(const std::string& efficiencyConfigFilename)
{
  std::ifstream stream(efficiencyConfigFilename);
  std::string line;
  std::vector<std::string> tokens;
  EfficiencyInfo efficiencyInfo;
  while (std::getline(stream, line)) {
    boost::algorithm::trim(line);
    if (line.length() == 0) continue;
    if (line[0] == '#') continue;
    split(line, ' ', tokens);
    efficiencyInfo.realEffMeans.push_back(std::stod(tokens[0]));
    efficiencyInfo.realEffUncs.push_back(std::stod(tokens[1]));
    efficiencyInfo.fakeEffMeans.push_back(std::stod(tokens[2]));
    efficiencyInfo.fakeEffUncs.push_back(std::stod(tokens[3]));
  }

  return efficiencyInfo;
}


int main(int argc, char* argv[])
{
  if (argc < 2) {
    std::cerr << "Please enter filename of efficiency configuration file!" << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string efficiencyConfigFilename(argv[1]);
  const EfficiencyInfo& efficiencyInfo = getEfficiencyInfo(efficiencyConfigFilename);

  gErrorIgnoreLevel = 3000;

  std::ostringstream stream;
  stream << std::cin.rdbuf();
  std::string input = stream.str();
  std::istringstream readStream(input);

  printFakeMLE(efficiencyInfo, readStream);

  return EXIT_SUCCESS;
}
