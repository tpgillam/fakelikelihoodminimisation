#include "CLsCalculator.h"
#include "utils.h"

// Due to error in current version of boost
#ifdef __clang__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-register"
#endif
#include <boost/math/special_functions/erf.hpp>
#ifdef __clang__
#pragma GCC diagnostic pop
#endif

#include <boost/math/special_functions/fpclassify.hpp>
#include <boost/math/tools/roots.hpp>
#include <iostream>
#include <algorithm>

CLsCalculator::CLsCalculator() :  
  m_sqrtQMuCache(0.0), m_numToys(0),
  m_justHitLowerBound(false),
  m_numNewtonRaphsonIterations(0),
  m_approxCLsULCache(-1.0),
  m_approxCLsbULCache(-1.0),
  m_toysCLsbULCache(-1.0)
{
}

double CLsCalculator::approxDiscoverySig()
{
  return sqrt(q0());
}

double CLsCalculator::approxP0()
{
  return 0.5 * boost::math::erfc(sqrt(q0()) / M_SQRT2);
}

double CLsCalculator::approxPMu(double mu, double muPrime, bool useCachedSqrtQMu)
{
  double thisSqrtQMu;
  if (useCachedSqrtQMu) {
    thisSqrtQMu = m_sqrtQMuCache;
  } else {
    thisSqrtQMu = sqrt(qMu(mu));
    m_sqrtQMuCache = thisSqrtQMu;
  }

  // std::cout << "thisSqrtQMu: " << thisSqrtQMu << std::endl;
  // std::cout << "mu         : " << mu << std::endl;
  // std::cout << "muPrime    : " << muPrime << std::endl;
  // std::cout << "resIfEq    : " << 0.5 * boost::math::erfc(thisSqrtQMu / M_SQRT2) << std::endl;

  if (nearlyEqual(mu, muPrime)) {
    return 0.5 * boost::math::erfc(thisSqrtQMu / M_SQRT2);
  } else {
    if (nearlyEqual(thisSqrtQMu, 0.0)) {
      return 0.5;
    }
    double sigma = estimateSigmaFromAsimovDataset(mu, muPrime);
    return 0.5 * boost::math::erfc((thisSqrtQMu - ((mu-muPrime)/sigma)) / M_SQRT2);
  }
}

double CLsCalculator::approxCLs(double mu)
{
  double a = approxPMu(mu, mu);
  double b = approxPMu(mu, 0, true);
  // std::cout << a << ", " << b << ", " << a/b << ": " << log(a/b) << std::endl;
  return (a / b);
}

double CLsCalculator::approxCLsb(double mu)
{
  return approxPMu(mu, mu);
}

double CLsCalculator::approxCLsUL(double percentage, double precision)
{
  // Find the *smallest* mu such that CLs(mu) < (1 - percentage/100)
  const double target = 1.0 - percentage/100.0;

  ULEval eval(this, &CLsCalculator::approxCLs, target);
  // return findUpperLimit(eval, precision); 
  m_approxCLsULCache = newtonRaphsonGillam(eval, nObsTight(), true); 
  return m_approxCLsULCache;
}

double CLsCalculator::approxCLsbUL(double percentage, double precision)
{
  // Find the *smallest* mu such that CLsb(mu) < (1 - percentage/100)
  const double target = 1.0 - percentage/100.0;

  ULEval eval(this, &CLsCalculator::approxCLsb, target);
  // return findUpperLimit(eval, precision); 
  double startingPoint = std::max(nObsTight()/2.0, 1.0);
  m_approxCLsbULCache = newtonRaphsonGillam(eval, startingPoint, false); 
  return m_approxCLsbULCache;
}

double CLsCalculator::findUpperLimit(ULEval &eval, double precision)
{
  double target = eval.target();
  ULTol approxULTol(precision);
  boost::uintmax_t maxEvals(100);
  double bracketMax = (double)nObsTot();
  bracketMax = findReasonableBracketMax(eval, bracketMax);

  unsigned int numTriesLeft = 3;
  while (numTriesLeft > 0) {
    --numTriesLeft;
    try {
      const auto &result = boost::math::tools::toms748_solve(eval, 0.0, bracketMax, approxULTol, maxEvals);
      return (result.first + result.second) / 2.0;
    } catch (std::domain_error &e) {
      if (numTriesLeft > 0) {
        bracketMax = findReasonableBracketMax(eval, 2*bracketMax);
      } else {
        std::cerr << "Root not found within limits " << 0.0 << " and " << bracketMax << std::endl;
        std::cerr << "Eval at max: " << approxCLs(bracketMax) << std::endl;
        std::cerr << "Eval at 0.0: " << approxCLs(0.0) << std::endl;
        std::cerr << "Target Eval: " << target << std::endl;
        throw e;
      }
    }
  }
  throw std::runtime_error("findUpperLimit: should not get here!");
}

double CLsCalculator::findReasonableBracketMax(ULEval &eval, const double startingValue)
{
  double bracketMax = startingValue;
  while (true) {
    double testCLs = eval(bracketMax);
    // std::cout << "bracketMax: " << bracketMax << std::endl;
    // std::cout << "testCLs   : " << testCLs << std::endl;
    if (boost::math::isnan(testCLs) || boost::math::isinf(testCLs)) {
      bracketMax *= 1.1;
      continue;
    }
    if (testCLs < 0.0) {
      bracketMax /= 2.0;
    } else {
      break;
    }
  }
  while (eval(bracketMax) > 0.0) {
    bracketMax *= 2.0;
  }
  return bracketMax;
}

double CLsCalculator::maxMuValue() const
{
  return std::max(4.0 * nObsTight(), 5.0);
}


void CLsCalculator::resetNewtonRaphsonGillamCache()
{
  m_xValsTooSmall.clear();
  m_xValsTooBig.clear();
  m_allXVals.clear();
  m_allYVals.clear();
  m_numNewtonRaphsonIterations = 0;
  m_justHitLowerBound = false;
}

double CLsCalculator::newtonRaphsonGillam(ULEval &eval, double startingPoint, bool clsMode, bool parentRun)
{
  if (parentRun) {
    resetNewtonRaphsonGillamCache();
  }

  if (m_numNewtonRaphsonIterations > 6) {
    // Give up, to prevent this thing going on indefinitely
    return nextStartingPosition(startingPoint);
  }
  m_numNewtonRaphsonIterations += 1;

  // std::cout << "newtonRaphsonGillam: " << startingPoint << std::endl;
  if (boost::math::isnan(startingPoint) or boost::math::isinf(startingPoint)) {
    startingPoint = nextStartingPosition(maxMuValue());
    // std::cout << "newtonRaphsonGillam corrected starting value: " << startingPoint << std::endl;
  }

  const double minAbsoluteRange(1.0);
  const double defaultRelRange(0.05);
  double minRange = std::max(std::min(startingPoint * (1.0 - defaultRelRange), startingPoint - minAbsoluteRange/2.0), 0.0);
  double maxRange = std::max(startingPoint * (1.0 + defaultRelRange), startingPoint + minAbsoluteRange/2.0);
  const unsigned int numberOfPoints = 10;

  std::vector<double> xVals;
  std::vector<double> unloggedYVals;
  std::vector<double> yVals;
  // TODO this might need to go elsewhere for CLs
  startImportanceSamplingRun();

  double thisRunMin = minRange;
  double thisRunMax = maxRange;


  bool carryOnWithThings = false;

  double slope, intercept;
  unsigned int numTries = 0;
  while (!carryOnWithThings) {
    ++numTries;
      
    for (unsigned int i = 0; i < numberOfPoints; ++i) {
      double mu = thisRunMin + i * (thisRunMax - thisRunMin) / numberOfPoints;
      double unloggedValue = eval.evaluateWithoutSubtraction(mu);
      double y = log(unloggedValue);
      // std::cout << y << std::endl;
      if (boost::math::isnan(y) || boost::math::isinf(y)) {
        // std::cout << unloggedValue << ", " << y << std::endl;
        continue;
      }
      xVals.push_back(mu);
      unloggedYVals.push_back(unloggedValue);
      yVals.push_back(y);
      m_allXVals.push_back(mu);
      m_allYVals.push_back(unloggedValue);
    }

    // Ensure that we have enough valid points
    if (xVals.size() < numberOfPoints / 2) {
      // std::cout << "Not enough valid points" << std::endl;
      m_xValsTooBig.push_back(startingPoint);
      return newtonRaphsonGillam(eval, nextStartingPosition(startingPoint / 2.0), clsMode, false);
    }


    // Jump to higher position if numbers are way too large
    double sum(0.0);
    for (const auto &x : unloggedYVals) {
      sum += x;
    }
    double average = sum / unloggedYVals.size();
    if (average > 0.3) {
      m_xValsTooSmall.push_back(startingPoint);
      return newtonRaphsonGillam(eval, nextStartingPosition((startingPoint + maxMuValue()) / 2.0), clsMode, false);
    }

    std::vector<double> xValsRaw(xVals);
    std::vector<double> yValsRaw(yVals);

    outlierCorrectedLinearFit(xVals, yVals, slope, intercept, clsMode);
    if (xVals.size() < numberOfPoints / 1.5) {
      thisRunMin -= 0.005;
      thisRunMax -= 0.005;
      minRange -= 0.005;
      // double oldMax = thisRunMax;
      // double oldMin = thisRunMin;
      // thisRunMax = thisRunMin;
      // thisRunMin = std::max(thisRunMax - (oldMax - oldMin), 0.0);
      // minRange = thisRunMin;
      xVals = xValsRaw;
      yVals = yValsRaw;
      // std::cout << std::endl << "GOING BACK TO THE START!" << std::endl << std::endl;
    } else if (numTries > 4) {
      // FIXME!!
      carryOnWithThings = true;
    } else {
      carryOnWithThings = true;
    }
  }

  if (nearlyEqual(slope, 0.0)) {
    double typicalYVal = exp(yVals[0]);
    double candidatePosition(0.0);
    if (nearlyEqual(typicalYVal, 0.5) || (clsMode && nearlyEqual(typicalYVal, 1.0))) {
      m_xValsTooSmall.push_back(startingPoint);
      candidatePosition = (startingPoint + (double) nObsTight()) / 2.0;
    } else {
      // m_xValsTooBig.push_back(startingPoint);
      candidatePosition = startingPoint / 2.0;
    }
    return newtonRaphsonGillam(eval, nextStartingPosition(candidatePosition), clsMode, false);
  } else if (slope > 0.0) {
    double candidatePosition = (startingPoint + (double) nObsTight()) / 2.0;
    return newtonRaphsonGillam(eval, nextStartingPosition(candidatePosition), clsMode, false);
  }
  // Remember that number of points might have effectively changed now

  double logTarget = log(eval.target());
  double candidatePosition = (logTarget - intercept) / slope;

  double xmin = (*std::min_element(xVals.begin(), xVals.end()));
  double xmax = (*std::max_element(xVals.begin(), xVals.end()));
  // std::cout << "xmin: " << xmin << std::endl;
  // std::cout << "xmax: " << xmax << std::endl;
  // std::cout << "candidatePosition: " << candidatePosition << std::endl;

  if (xmin < candidatePosition && candidatePosition < xmax) {
    return candidatePosition;
  } else {
    // Make sensible if necessary
    candidatePosition = std::min(candidatePosition, maxMuValue());
    if (candidatePosition > startingPoint) {
      m_xValsTooSmall.push_back(startingPoint);
    } else {
      m_xValsTooBig.push_back(startingPoint);
    }

    if (m_justHitLowerBound && candidatePosition <= 0.0) {
      return 0.0;
    }
    return newtonRaphsonGillam(eval, nextStartingPosition(candidatePosition), clsMode, false);
  }
}

void CLsCalculator::outlierCorrectedLinearFit(std::vector<double> &xVals, std::vector<double> &yVals,
    double &slope, double &intercept, bool clsMode)
{
  bool finished = false;
  while (!finished) {
    const unsigned int numberOfPoints = xVals.size();
    linearFit(xVals, yVals, slope, intercept);
    // std::cout << slope << ", " << intercept << std::endl;

    double relUnc = 0.02;
    if (clsMode) {
      relUnc = 0.1;
    }

    double chiSquared(0.0);
    std::vector<double> residuals(numberOfPoints);
    std::vector<double> residualsSquared(numberOfPoints);
    for (unsigned int i = 0; i < numberOfPoints; ++i) {
      double pred = intercept + slope*xVals[i];
      double residual = (yVals[i] - pred);
      residuals[i] = residual;
      residualsSquared[i] = residual*residual;
      double unc = relUnc * yVals[i];
      if (boost::math::isnan(unc) or boost::math::isinf(unc) or nearlyEqual(unc, 0.0)) {
        unc = relUnc;
      }
      chiSquared += residual*residual / (unc * unc);
    }
    double chiSquaredRel = chiSquared / ((double)numberOfPoints - 2.0);

    std::vector<double> unloggedYVals(yVals);
    for (auto &yVal : unloggedYVals) {
      yVal = exp(yVal);
    }
    // std::cout << xVals << std::endl;
    // std::cout << unloggedYVals << std::endl;

    // std::cout << yVals << std::endl;
    // std::cout << residuals << std::endl;
    // std::cout << residualsSquared << std::endl;
    // std::cout << chiSquaredRel << std::endl;

    if (chiSquaredRel > 2) {
      // std::cout << "ooh.. discarding outlier!" << std::endl;
      // unsigned int outlierIndex = std::min_element(residuals.begin(), residuals.end()) - residuals.begin();
      unsigned int outlierIndex = std::max_element(residualsSquared.begin(), residualsSquared.end()) - residualsSquared.begin();
      xVals.erase(xVals.begin() + outlierIndex);
      yVals.erase(yVals.begin() + outlierIndex);
    } else {
      finished = true;
    }
  }
}

void CLsCalculator::linearFit(const std::vector<double> &xVals, const std::vector<double> &yVals,
    double &slope, double &intercept)
{
  double sx = 0.0, sy = 0.0, stt = 0.0, sts = 0.0;
  const unsigned int n = xVals.size();
  for (unsigned int i = 0; i < n; ++i)
  {
    sx += xVals[i];
    sy += yVals[i];
  }
  for (unsigned int i = 0; i < n; ++i)
  {
    double t = xVals[i] - sx/n;
    stt += t*t;
    sts += t*yVals[i];
  }

  slope = sts/stt;
  intercept = (sy - sx*slope)/n;
}

double CLsCalculator::nextStartingPosition(double proposedStartingPosition) const
{
  m_justHitLowerBound = false;
  // std::cout << "proposedStartingPosition: " << proposedStartingPosition << std::endl;
  const auto minItr = std::max_element(m_xValsTooSmall.begin(), m_xValsTooSmall.end());
  const auto maxItr = std::min_element(m_xValsTooBig.begin(), m_xValsTooBig.end());
  double minVal = 0.0;
  double maxVal = maxMuValue();
  if (minItr != m_xValsTooSmall.end()) minVal = *minItr;
  if (maxItr != m_xValsTooBig.end()) maxVal = *maxItr;
  if (proposedStartingPosition > (minVal + 0.2) && proposedStartingPosition < (maxVal - 0.2)) {
    return proposedStartingPosition;
  } else {
    if (proposedStartingPosition < minVal) {
      m_justHitLowerBound = true;
    }
    double halfWay = (minVal + maxVal) / 2.0;
    return halfWay;
  }
}


double CLsCalculator::approx95PercentCLsUL(double precision)
{
  return approxCLsUL(95.0, precision);
}

double CLsCalculator::approx95PercentCLsbUL(double precision)
{
  return approxCLsbUL(95.0, precision);
}




double CLsCalculator::toysCLs(double mu)
{
  double a = toysPMu(mu, mu, m_numToys);
  double b = toysPMu(mu, 0, m_numToys);
  return (a / b);
}

double CLsCalculator::toysCLsb(double mu)
{
  double result = toysPMu(mu, mu, m_numToys);
  m_toysPMuMuCache.push_back(std::make_pair(mu, result));
  return result;
  // return toysPMuImportanceSampling(mu, mu, m_numToys);
}

double CLsCalculator::toysCLsUL(double percentage, unsigned int numToys, double precision)
{
  m_numToys = numToys;
  // Find the *smallest* mu such that CLs(mu) < (1 - percentage/100)
  const double target = 1.0 - percentage/100.0;

  ULEval eval(this, &CLsCalculator::toysCLs, target);
  // return findUpperLimit(eval, precision); 
  bool cachesInitialised = ((m_approxCLsULCache >= 0.0) &&
                           (m_approxCLsbULCache >= 0.0) &&
                           (m_toysCLsbULCache >= 0.0));

  double startingPoint(0.0);
  if (cachesInitialised) {
    startingPoint = m_toysCLsbULCache * m_approxCLsULCache / m_approxCLsbULCache;
  } else {
    startingPoint = approxCLsUL(percentage, precision);
  }
  return newtonRaphsonGillam(eval, startingPoint, true); 
}

double CLsCalculator::toysCLsbUL(double percentage, unsigned int numToys, double precision)
{
  m_numToys = numToys;
  // Find the *smallest* mu such that CLsb(mu) < (1 - percentage/100)
  const double target = 1.0 - percentage/100.0;

  ULEval eval(this, &CLsCalculator::toysCLsb, target);
  // return findUpperLimit(eval, precision); 
  bool cachesInitialised = (m_approxCLsbULCache >= 0.0);

  double startingPoint(0.0);
  if (cachesInitialised) {
    startingPoint = m_approxCLsbULCache;
  } else {
    startingPoint = approxCLsbUL(percentage, precision);
  }
  m_toysCLsbULCache = newtonRaphsonGillam(eval, startingPoint, false); 
  return m_toysCLsbULCache;
}

double CLsCalculator::toys95PercentCLsUL(unsigned int numToys, double precision)
{
  return toysCLsUL(95.0, numToys, precision);
}

double CLsCalculator::toys95PercentCLsbUL(unsigned int numToys, double precision)
{
  return toysCLsbUL(95.0, numToys, precision);
}

void CLsCalculator::clearCLsComputationCaches()
{
  m_toysPMuMuCache.clear();
  m_approxCLsULCache = -1.0;
  m_approxCLsbULCache = -1.0;
  m_toysCLsbULCache = -1.0;
}


double CLsCalculator::ULEval::operator()(double mu)
{
  return evaluateWithoutSubtraction(mu) - m_target;
}

double CLsCalculator::ULEval::evaluateWithoutSubtraction(double mu)
{
  if (boost::math::isnan(mu)) return NAN;
  if (boost::math::isinf(mu)) return 0.0;

  double result = ((m_clsCalc->*m_objective)(mu));
  unsigned int numTriesLeft = 3;
  while ((boost::math::isinf(result) or boost::math::isnan(result)) && (numTriesLeft > 0)) {
    result = ((m_clsCalc->*m_objective)(mu));
    // std::cout << std::endl << "in loop: " << result << std::endl;
    --numTriesLeft;
  }

  // std::cout << "ULEval("<<mu<<"): "<<result << std::endl;
  return result;
}
