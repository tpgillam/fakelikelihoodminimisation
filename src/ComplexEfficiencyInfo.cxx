#include "ComplexEfficiencyInfo.h"
#include "utils.h"

#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>


ComplexEfficiencyInfo::ComplexEfficiencyInfo(
    const std::string& efficiencyConfigFilename, 
    EfficiencyVersion::e efficiencyVersion) :
  m_efficiencyVersion(efficiencyVersion),
  m_standardNormalDist(0., 1.)
{
  std::ifstream stream(efficiencyConfigFilename);

  if (m_efficiencyVersion == EfficiencyVersion::UNKNOWN) {
    m_efficiencyVersion = getEfficiencyVersionFromStream(stream);  
  }

  switch (m_efficiencyVersion) {
    case EfficiencyVersion::BASIC:
      loadBasicEfficiencyFile(stream);
      break;

    case EfficiencyVersion::WITH_CORRELATIONS:
      loadCorrelatedEfficiencyFile(stream);
      break;

    case EfficiencyVersion::UNKNOWN:
    default:
      throw std::runtime_error("Need to have figured out version at this point");
  }

  initialiseEfficiencyDistributions();
}


EfficiencyVersion::e ComplexEfficiencyInfo::getEfficiencyVersionFromStream(std::ifstream& stream)
{
  std::string line;
  int pos = stream.tellg();
  bool keepThisLine = false;

  while (std::getline(stream, line)) {
    boost::algorithm::trim(line);
    if (line.length() == 0) continue;
    else if (line[0] == '#') continue;
    else if (line == "VERSION") {
      keepThisLine = true;
    } else if (keepThisLine) {
      if (line == "BASIC") {
        return EfficiencyVersion::BASIC;
      } else if (line == "WITH_CORRELATIONS") {
        return EfficiencyVersion::WITH_CORRELATIONS;
      } else {
        throw std::runtime_error("Uknown efficiency file format!");
      }
    } else {
      // Seek back to where we were before this line and bail out
      stream.seekg(pos);
      break;
    }
  }

  return EfficiencyVersion::UNKNOWN;
}


void ComplexEfficiencyInfo::loadBasicEfficiencyFile(
    std::ifstream& stream)
{
  std::string line;
  std::vector<std::string> tokens;
  FileLocation::e fileLocation(FileLocation::BEGIN);
  while (std::getline(stream, line)) {
    boost::algorithm::trim(line);
    if (line.length() == 0) continue;
    if (line[0] == '#') continue;
    if (line == "CATEGORIES") {
      fileLocation = FileLocation::CATEGORIES;
      continue;
    } else if (line == "REAL") {
      fileLocation = FileLocation::REAL;
      continue;
    } else if (line == "FAKE") {
      fileLocation = FileLocation::FAKE;
      continue;
    }

    if (fileLocation == FileLocation::BEGIN) {
      throw std::runtime_error("Shouldn't have anything before CATEGORIES");
    }

    split(line, ' ', tokens);

    if (fileLocation == FileLocation::CATEGORIES) {
      categoryToRealEff.push_back(std::stoi(tokens[0]));
      categoryToFakeEff.push_back(std::stoi(tokens[1]));
    } else if (fileLocation == FileLocation::REAL) {
      realEffMeans.push_back(std::stod(tokens[0]));
      realEffUncs.push_back(std::stod(tokens[1]));
    } else if (fileLocation == FileLocation::FAKE) {
      fakeEffMeans.push_back(std::stod(tokens[0]));
      fakeEffUncs.push_back(std::stod(tokens[1]));
    }
  }
}


void ComplexEfficiencyInfo::loadCorrelatedEfficiencyFile(std::ifstream& stream)
{
  std::string line;
  std::vector<std::string> tokens;
  FileLocation::e fileLocation(FileLocation::BEGIN);
  bool withinCorrelationGroup = false;
  while (std::getline(stream, line)) {
    boost::algorithm::trim(line);
    if (line.length() == 0) continue;
    if (line[0] == '#') continue;
    if (line == "CATEGORIES") {
      fileLocation = FileLocation::CATEGORIES;
      continue;
    } else if (line == "REAL") {
      withinCorrelationGroup = false;
      fileLocation = FileLocation::REAL;
      continue;
    } else if (line == "FAKE") {
      withinCorrelationGroup = false;
      fileLocation = FileLocation::FAKE;
      continue;
    } else if (line == "CORRELATION_GROUP") {
      if (fileLocation == FileLocation::REAL) {
        realEffCorrGroups.push_back(std::vector<unsigned int>());
      } else if (fileLocation == FileLocation::FAKE) {
        fakeEffCorrGroups.push_back(std::vector<unsigned int>());
      } else {
        throw std::runtime_error("Misplaced CORRELATION_GROUP");
      }
      withinCorrelationGroup = true;
      continue;
    } else if (line == "END_CORRELATION_GROUP") {
      if (!withinCorrelationGroup || (fileLocation == FileLocation::REAL &&
                                      fileLocation == FileLocation::FAKE)) {
        throw std::runtime_error("Misplaced END_CORRELATION_GROUP");
      }
      withinCorrelationGroup = false;
      continue;
    }

    if (fileLocation == FileLocation::BEGIN) {
      throw std::runtime_error("Shouldn't have anything before CATEGORIES");
    }

    split(line, ' ', tokens);

    if (fileLocation == FileLocation::CATEGORIES) {
      categoryToRealEff.push_back(std::stoi(tokens[0]));
      categoryToFakeEff.push_back(std::stoi(tokens[1]));
    } else if (fileLocation == FileLocation::REAL) {
      realEffMeans.push_back(std::stod(tokens[0]));
      realEffUncs.push_back(std::stod(tokens[1]));
      if (withinCorrelationGroup) {
        unsigned int effIndex = realEffMeans.size() - 1;
        realEffCorrGroups.back().push_back(effIndex);
        realEffUncsCorr.push_back(std::stod(tokens[2]));
      }
    } else if (fileLocation == FileLocation::FAKE) {
      fakeEffMeans.push_back(std::stod(tokens[0]));
      fakeEffUncs.push_back(std::stod(tokens[1]));
      if (withinCorrelationGroup) {
        unsigned int effIndex = fakeEffMeans.size() - 1;
        fakeEffCorrGroups.back().push_back(effIndex);
        fakeEffUncsCorr.push_back(std::stod(tokens[2]));
      }
    }
  }
}


void ComplexEfficiencyInfo::initialiseEfficiencyDistributions()
{
  if (m_efficiencyVersion == EfficiencyVersion::BASIC) {
    initialiseUncorrelatedEfficiencyDistributions();
  } else if (m_efficiencyVersion == EfficiencyVersion::WITH_CORRELATIONS) {
    initialiseCorrelatedEfficiencyDistributions();
  } else {
    throw std::runtime_error("Uknown efficiency type to draw!");
  }
}

void ComplexEfficiencyInfo::initialiseUncorrelatedEfficiencyDistributions()
{
  m_realEffDists.resize(nRealEfficiencies(), 0);
  m_fakeEffDists.resize(nFakeEfficiencies(), 0);

  for (unsigned int i = 0; i < realEffMeans.size(); ++i) {
    double mean = realEffMeans[i];
    double sigma = realEffUncs[i];
    m_realEffDists[i] = new boost::math::normal(mean, sigma);
  }

  for (unsigned int i = 0; i < fakeEffMeans.size(); ++i) {
    double mean = fakeEffMeans[i];
    double sigma = fakeEffUncs[i];
    m_fakeEffDists[i] = new boost::math::normal(mean, sigma);
  }
}


void ComplexEfficiencyInfo::initialiseCorrelatedEfficiencyDistributions()
{
  // Initialise the uncorrelated uncertainty distributions, but note with mean
  // of zero
  m_realEffDists.resize(nRealEfficiencies(), 0);
  m_fakeEffDists.resize(nFakeEfficiencies(), 0);

  for (unsigned int i = 0; i < realEffMeans.size(); ++i) {
    double mean = 0.;
    double sigma = realEffUncs[i];
    m_realEffDists[i] = new boost::math::normal(mean, sigma);
  }

  for (unsigned int i = 0; i < fakeEffMeans.size(); ++i) {
    double mean = 0.;
    double sigma = fakeEffUncs[i];
    m_fakeEffDists[i] = new boost::math::normal(mean, sigma);
  }

  precomputeCorrelatedTruncationBounds();
}


void ComplexEfficiencyInfo::precomputeCorrelatedTruncationBounds()
{
  m_realCorrGroupDrawBounds.resize(realEffCorrGroups.size());
  m_fakeCorrGroupDrawBounds.resize(fakeEffCorrGroups.size());

  for (unsigned int i_grp = 0; i_grp < realEffCorrGroups.size(); ++i_grp) {
    // Initialise to very unconstraining values -- these are limits
    // applied to a standard normal distribution
    double lowerBound(-100), upperBound(100);
    const auto& realEffCorrGroup = realEffCorrGroups[i_grp];
    for (const auto& i_eff : realEffCorrGroup) {
      if (i_eff == 0) {
        lowerBound = - realEffMeans[i_eff] / realEffUncsCorr[i_eff];
        upperBound = (1. - realEffMeans[i_eff]) / realEffUncsCorr[i_eff];
      } else {
        lowerBound = std::max(lowerBound, - realEffMeans[i_eff] / realEffUncsCorr[i_eff]);
        upperBound = std::min(upperBound, (1. - realEffMeans[i_eff]) / realEffUncsCorr[i_eff]);
      }
    }
    m_realCorrGroupDrawBounds[i_grp].first = lowerBound;
    m_realCorrGroupDrawBounds[i_grp].second = upperBound;
  }

  for (unsigned int i_grp = 0; i_grp < fakeEffCorrGroups.size(); ++i_grp) {
    // Initialise to very unconstraining values -- these are limits
    // applied to a standard normal distribution
    double lowerBound(-100), upperBound(100);
    const auto& fakeEffCorrGroup = fakeEffCorrGroups[i_grp];
    for (const auto& i_eff : fakeEffCorrGroup) {
      if (i_eff == 0) {
        lowerBound = - fakeEffMeans[i_eff] / fakeEffUncsCorr[i_eff];
        upperBound = (1. - fakeEffMeans[i_eff]) / fakeEffUncsCorr[i_eff];
      } else {
        lowerBound = std::max(lowerBound, - fakeEffMeans[i_eff] / fakeEffUncsCorr[i_eff]);
        upperBound = std::min(upperBound, (1. - fakeEffMeans[i_eff]) / fakeEffUncsCorr[i_eff]);
      }
    }
    m_fakeCorrGroupDrawBounds[i_grp].first = lowerBound;
    m_fakeCorrGroupDrawBounds[i_grp].second = upperBound;
  }

  // std::cout << "correlated truncation bounds: " << m_realCorrGroupDrawBounds << " " << m_fakeCorrGroupDrawBounds << std::endl;
}


ComplexEfficiencyInfo::~ComplexEfficiencyInfo()
{
  for (auto* dist : m_realEffDists) {
    if (dist) {
      delete dist;
      dist = 0;
    }
  }

  for (auto* dist : m_fakeEffDists) {
    if (dist) {
      delete dist;
      dist = 0;
    }
  }
}


void ComplexEfficiencyInfo::drawEfficiencies(
    std::vector<double>& realEffs, std::vector<double>& fakeEffs) const 
{
  // // Snippet to disable variation of efficiencies about means
  
  // for (unsigned int i = 0; i < realEffMeans.size(); ++i) {
  //   realEffs[i] = realEffMeans[i];
  // }
  // for (unsigned int i = 0; i < fakeEffMeans.size(); ++i) {
  //   fakeEffs[i] = fakeEffMeans[i];
  // }
  // return;

  if (m_efficiencyVersion == EfficiencyVersion::BASIC) {
    drawUncorrelatedEfficiencies(realEffs, fakeEffs);
  } else if (m_efficiencyVersion == EfficiencyVersion::WITH_CORRELATIONS) {
    drawCorrelatedEfficiencies(realEffs, fakeEffs);
  } else {
    throw std::runtime_error("Uknown efficiency type to draw!");
  }
}


void ComplexEfficiencyInfo::drawUncorrelatedEfficiencies(
    std::vector<double>& realEffs, std::vector<double>& fakeEffs) const 
{
start:
  for (unsigned int i = 0; i < m_realEffDists.size(); ++i) {
    realEffs[i] = drawTruncatedBetween(m_realEffDists[i], 0., 1.);
  }

  for (unsigned int i = 0; i < m_fakeEffDists.size(); ++i) {
    fakeEffs[i] = drawTruncatedBetween(m_fakeEffDists[i], 0., 1.);
    // We can't have efficiencies being precisely equal, as then MM is
    // degenerate. Probability 0 in theory, but small and finite in practice due
    // to machine precision.
    if (fakeEffs[i] == realEffs[i]) goto start;
  }
}


void ComplexEfficiencyInfo::drawCorrelatedEfficiencies(
    std::vector<double>& realEffs, std::vector<double>& fakeEffs) const 
{
  // Note that the scaling is such the Var[k*X] = k^2 * Var[X]. That is,
  // we simply multiply the drawn variate by the "correlated uncertainty" for
  // each member of the correlation group, and add. One needs to ensure that
  // truncation bounds are met :
  //     these are simply found by iterating over each member of the group,
  //     and storing the most stringent upper and lower bounds given the
  //     central values (in practice this should not cause problems)

start:
  
  // Initialise all efficiencies to their mean value
  for (unsigned int i = 0; i < realEffs.size(); ++i) {
    realEffs[i] = realEffMeans[i];
  }
  for (unsigned int i = 0; i < fakeEffs.size(); ++i) {
    fakeEffs[i] = fakeEffMeans[i];
  }

  // Add on correlated uncertainty component
  for (unsigned int i_grp = 0; i_grp < realEffCorrGroups.size(); ++i_grp) {
    const auto& bounds = m_realCorrGroupDrawBounds[i_grp];
    double variate = drawTruncatedBetween(&m_standardNormalDist, bounds.first, bounds.second);
    const auto& realEffCorrGroup = realEffCorrGroups[i_grp];
    for (const auto& i_eff : realEffCorrGroup) {
      realEffs[i_eff] += variate * realEffUncsCorr[i_eff];
    }
  }

  for (unsigned int i_grp = 0; i_grp < fakeEffCorrGroups.size(); ++i_grp) {
    const auto& bounds = m_fakeCorrGroupDrawBounds[i_grp];
    double variate = drawTruncatedBetween(&m_standardNormalDist, bounds.first, bounds.second);
    const auto& fakeEffCorrGroup = fakeEffCorrGroups[i_grp];
    for (const auto& i_eff : fakeEffCorrGroup) {
      fakeEffs[i_eff] += variate * fakeEffUncsCorr[i_eff];
    }
  }
  

  // Add on uncorrelated uncertainty component
  
  for (unsigned int i = 0; i < m_realEffDists.size(); ++i) {
    realEffs[i] += drawTruncatedBetween(m_realEffDists[i], -realEffs[i], 1.-realEffs[i]);
  }

  for (unsigned int i = 0; i < m_fakeEffDists.size(); ++i) {
    fakeEffs[i] += drawTruncatedBetween(m_fakeEffDists[i], -fakeEffs[i], 1.-fakeEffs[i]);
    // We can't have efficiencies being precisely equal, as then MM is
    // degenerate. Probability 0 in theory, but small and finite in practice due
    // to machine precision.
    if (fakeEffs[i] == realEffs[i]) goto start;
  }

  // std::cout << realEffs << " " << fakeEffs << std::endl;
}



std::ostream& operator<<(std::ostream& os, const ComplexEfficiencyInfo& info)
{
  os << "nCategories:       " << info.nCategories() << std::endl;
  os << "categoryToRealEff: " << info.categoryToRealEff << std::endl;
  os << "categoryToFakeEff: " << info.categoryToFakeEff << std::endl << std::endl;
  os << "realEffMeans:      " << info.realEffMeans << std::endl;
  os << "realEffUncs:       " << info.realEffUncs << std::endl << std::endl;
  os << "fakeEffMeans:      " << info.fakeEffMeans << std::endl;
  os << "fakeEffUncs:       " << info.fakeEffUncs << std::endl;
  return os;
}
