#ifndef MLE_RUNNER_H
#define MLE_RUNNER_H

#include <vector>

struct EfficiencyInfo {
  unsigned int nCategories() const {
    return realEffMeans.size();
  }

  std::vector<double> realEffMeans;
  std::vector<double> realEffUncs;
  std::vector<double> fakeEffMeans;
  std::vector<double> fakeEffUncs;
};

void printFakeMLE(const EfficiencyInfo& efficiencyInfo, std::istream& stream);
EfficiencyInfo getEfficiencyInfo(const std::string& efficiencyConfigFilename);

#endif // MLE_RUNNER_H
