#ifndef BACKGROUND_COMPONENT_H
#define BACKGROUND_COMPONENT_H

#include "Configs.h"

struct ConfigCache {
  std::vector<bool> isTight;
  std::vector<bool> isReal;
  std::vector<unsigned int> ohConfig;
  unsigned int q;
  bool qIsTight;
};


class BackgroundComponent {
  public:
    BackgroundComponent(unsigned int nOmegaHat, unsigned int nLeptons, const std::vector<bool> &qIsTight);
    virtual ~BackgroundComponent() { }

    virtual unsigned int extractParameters(const std::vector<double> &x, unsigned int offset);
    inline virtual double extraNLLComponent() const { return 0.0; }
    virtual std::vector<double> extraNLLComponentGradient() const;

    void computeRhos(const std::vector<double> &epsf,
                     const std::vector<double> &epsr,
                     const std::vector<unsigned int> &categoryToFakeEff,
                     const std::vector<unsigned int> &categoryToRealEff,
                     bool computeGradients = false);

    inline const std::vector<double> &rhos() const { return m_rhos; }

    double rate() const;
    double rateForQ(unsigned int q) const;
    std::vector<double> rateForQGradient(unsigned int q) const;
    double rateLoose() const;
    double rateTight() const;

    std::vector<double> rateLooseGradient() const;
    std::vector<double> rateTightGradient() const;

  private:
    void fillConfigCache();
    void initialiseCacheVectors();
    void computeBetasFromPhis();

  private:
    bool m_initialisedCacheVectors;
    unsigned int m_nParameters;
    unsigned int m_offset;

    unsigned int m_nOmegaHat;
    unsigned int m_nLeptons;
    std::vector<bool> m_qIsTight;
    unsigned int m_nQ;
    std::vector<ConfigCache> m_configCache;

    double m_rateTight;
    std::vector<double> m_rhos;
    std::vector<double> m_phi;
    std::vector<double> m_beta;
    std::vector<double> m_pi;
    double m_rhoTight;
    double m_rhoLoose;

    std::vector<std::vector<double> > m_rhoGradients;
    std::vector<double> m_rhoTightGradient;
    std::vector<double> m_rhoLooseGradient;
};

#endif // BACKGROUND_COMPONENT_H
