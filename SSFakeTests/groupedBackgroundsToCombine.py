backgrounds = {
'topX': [
'topW',
'topWJ2',
'topZ',
'topZJ2',
'ttbarHiggs',
'topPowheg',
'singleTopZ',
'singleTop',
'fourTop',
],

'diBoson': [
'diBosonSS',
'triBoson',
'diBosonWW',
'diBosonPowhegZZ',
'diBosonWZ',
],

'zJets': [
'zJetsMassiveB',
]
}
#'wJetsSherpa',


# meff_bin_boundaries        = [130, 250, 400, 600, 1000, 10000]
# visual_meff_bin_boundaries = [130, 250, 400, 600, 1000, 1400]


meff_bin_boundaries = []
visual_meff_bin_boundaries = []
for i in range(150, 1050, 50):
    meff_bin_boundaries.append(i)
    visual_meff_bin_boundaries.append(i)
meff_bin_boundaries.append(100000)
visual_meff_bin_boundaries.append(1050)
