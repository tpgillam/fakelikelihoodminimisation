#!/usr/bin/env python3.4
import numpy
import ROOT
import sys
import argparse
import random
import math
import re

import twoLepFakeExtracts

def bin_to_category_map():
    if bin_to_category_map.bin_map is None:
        bin_to_category_map.bin_map = {}
        index = 0
        for bin in twoLepFakeExtracts.generate_bins():
            key = (twoLepFakeExtracts.real_efficiency_index(bin), twoLepFakeExtracts.fake_efficiency_index(bin))
            bin_to_category_map.bin_map[key] = index
            index += 1
    return bin_to_category_map.bin_map
bin_to_category_map.bin_map = None

def convert_file(args):
    filenames = args.input_files
    for i,filename in enumerate(filenames):
        f = ROOT.TFile(filename)
        process = filename.split('.')[1]
        tree_name = '{}_nom'.format(process)
        t = f.Get(tree_name)
        is_last_file = (i == len(filenames) - 1)
        process_tree(t, args, is_last_file, process)
        f.Close()


def process_tree(tree, args, is_last_file, process):
    if 'total_weight' not in process_tree.__dict__:
        process_tree.total_weight = 0.0
        process_tree.total_squared_weight = 0.0
        process_tree.total_num_events = 0
    for event in tree:
        if skip_unwanted_event(event, args):
            continue

        realList = bitmask_to_list(event.isRealBitmask, event.nLooseLep)
        combined_weight = event.pileupWgt * event.trigWgt * event.eGammaWgt * event.muonWgt * event.bTagWgt * event.lumiScaling

        process_tree.total_weight += combined_weight
        process_tree.total_squared_weight += combined_weight ** 2
        process_tree.total_num_events += 1

        # if random.random() > combined_weight/15:
        #     continue

        tlList = bitmask_to_list(event.TLBitmask, event.nLooseLep, zero='l', one='t')
        tight1 = 0
        if tlList[0] == 't':
            tight1 = 1
        tight2 = 0
        if tlList[1] == 't':
            tight2 = 1

        category1 = eff_category_for_lepton(event, 1)
        category2 = eff_category_for_lepton(event, 2)

        if args.printEvents:
            print('{0} {1} {2} {3}'.format(tight1, tight2, category1, category2))
        if args.drawEvents:
            N = numpy.random.poisson(combined_weight, 1)[0]
            # if N > 0:
            #     sys.stderr.write('{0} {1}\n'.format(N, combined_weight))
            for i in range(N):
                is_event_real = 1
                for x in realList:
                    if not x:
                        is_event_real = 0
                        break
                fake_info = '{0} {1} {2} {3}'.format(tight1, tight2, category1, category2)
                print('{0} {1} {2} {3} {4}'.format(event.meff/1000, combined_weight, is_event_real, process, fake_info))
        if args.printLeptons:
            print('{0} {1} {2} {3}'.format(realList[0], tight1, category1, combined_weight))
            print('{0} {1} {2} {3}'.format(realList[1], tight2, category2, combined_weight))

        args.nEvents -= 1
        if (args.nEvents == 0):
            break
    if args.printSumWeights and is_last_file:
        print(process_tree.total_weight, math.sqrt(process_tree.total_squared_weight), process_tree.total_num_events)


def skip_unwanted_event(event, args):
    chargeFlipList = bitmask_to_list(event.isChargeFlipBitmask, event.nLooseLep)
    realList = bitmask_to_list(event.isRealBitmask, event.nLooseLep)
    tightList = bitmask_to_list(event.TLBitmask, event.nLooseLep)

    if not args.keepCF:
        for isCF in chargeFlipList:
            if isCF:
                return True

    if args.realOnly:
        for isReal in realList:
            if not isReal:
                return True

    if args.fakeOnly:
        is_fake_event = False
        for isReal in realList:
            isFake = not isReal
            if isFake:
                is_fake_event = True
                break
        if not is_fake_event:
            return True

    if args.tightOnly:
        for isTight in tightList:
            if not isTight:
                return True

    if args.looseOnly:
        is_loose_event = False
        for isTight in tightList:
            isLoose = not isTight
            if isLoose:
                is_loose_event = True
                break
        if not is_loose_event:
            return True

    if not passes_cuts(event, args):
        return True

    return False


def passes_cuts(event, args):
    GeV = 1000
    if not 'cut_string' in passes_cuts.__dict__:
        def repl(matchobj):
            keywords = ['and', 'or', 'GeV']
            match = matchobj.group(0)
            if match in keywords:
                return match
            else:
                return 'event.{}'.format(match)
        passes_cuts.cut_string = args.cuts
        # print('Using cut: "{}"'.format(passes_cuts.cut_string))
        passes_cuts.cut_string = re.sub(r'([a-zA-Z]+[a-zA-Z0-9]*)', repl, passes_cuts.cut_string)
    result = eval(passes_cuts.cut_string)
    return result


def eff_category_for_lepton(event, lep_index):
    pt = getattr(event, 'lep{}Pt'.format(lep_index))/1000.0
    eta = abs(getattr(event, 'lep{}Eta'.format(lep_index)))
    eta = min(eta, 2.49999)
    flavour = bitmask_to_list(event.isElBitmask, event.nLooseLep, zero='m', one='e')[lep_index-1]
    if flavour == 'e':
        bjets = 'noB'
        if event.nBJets20 > 0:
            bjets = 'hasB'
    else:
        bjets = 'anyB'
    bin = (-1, flavour, bjets, (pt, pt, eta, eta))
    key = (twoLepFakeExtracts.real_efficiency_index(bin), twoLepFakeExtracts.fake_efficiency_index(bin))
    return bin_to_category_map()[key]


def bitmask_to_list(bitmask, nLooseLep, zero=0, one=1):
    result = [] 
    for i in range(nLooseLep):
        if (bitmask >> i) % 2 == 0:
            result.append(zero)
        else:
            result.append(one)
    return result


def parse_arguments():
    parser = argparse.ArgumentParser(description='Convert ROOT file to stream of events usable by sampler as input')
    parser.add_argument('input_files', metavar='input_file', nargs='*', help='Names of input files, processed in order')
    parser.add_argument('--nEvents', type=int, help='The number of events to process (-1 => no limit)', default=-1)
    parser.add_argument('--seed', type=int, help='Seed for random number generator (default 0)', default=0)
    parser.add_argument('--realOnly', action='store_true', help='Only look at real events', default=False)
    parser.add_argument('--fakeOnly', action='store_true', help='Only look at fake events', default=False)
    parser.add_argument('--tightOnly', action='store_true', help='Only look at tight events', default=False)
    parser.add_argument('--looseOnly', action='store_true', help='Only look at loose events', default=False)
    parser.add_argument('--keepCF', action='store_true', help='Keep charge-flip events (remove by default)', default=False)
    parser.add_argument('--printSumWeights', action='store_true', help='Print the total weight and standard uncertainty thereof', default=False)
    parser.add_argument('--printEvents', action='store_true', help='Print all events', default=False)
    parser.add_argument('--drawEvents', action='store_true', help='Print a sampling of events with extra info', default=False)
    parser.add_argument('--printLeptons', action='store_true', help='Print one line info about each lepton', default=False)
    parser.add_argument('--cuts', 
            help='The cuts to apply in this region (default: "nBJets20 >= 1 and nJets40 >= 2")', 
            default='nBJets20 >= 1 and nJets40 >= 2')
    return parser.parse_args()


def main():
    args = parse_arguments()
    if args.input_files is None or len(args.input_files) == 0:
        args.input_files = []
        from groupedBackgroundsToCombine import backgrounds
        for background in backgrounds:
            file_stems = backgrounds[background]
            args.input_files += ['MC/histFitterTree_merge.'+file_stem+'.root' for file_stem in file_stems]
    random.seed(args.seed)
    numpy.random.seed(args.seed)
    convert_file(args)


if __name__ == '__main__':
    main()
