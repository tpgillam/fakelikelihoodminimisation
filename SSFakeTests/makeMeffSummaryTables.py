#!/usr/bin/env python3.4
import subprocess
import sys
from groupedBackgroundsToCombine import backgrounds, meff_bin_boundaries 


base_command = '('
for background in backgrounds:
    file_stems = backgrounds[background]
    input_files = ''
    for file_stem in file_stems:
        input_files += 'MC/histFitterTree_merge.'+file_stem+'.root '
    base_command += 'printf "'+background+': "; ./roottoinput.py '+input_files+' --nEvents -1 {0} --cuts "nBJets20 >= 1 and nJets40 >= 2 and meff > {1}*GeV and meff < {2}*GeV" --printSumWeights; '
    
base_command += ') | column -t | sort -k2 -g -r'


for i in range(len(meff_bin_boundaries)-1):
    meff_lower = meff_bin_boundaries[i]
    meff_upper = meff_bin_boundaries[i+1]
    real_tight_command = base_command.format('--realOnly --tightOnly', meff_lower, meff_upper)
    fake_tight_command = base_command.format('--fakeOnly --tightOnly', meff_lower, meff_upper)
    real_loose_command = base_command.format('--realOnly --looseOnly', meff_lower, meff_upper)
    fake_loose_command = base_command.format('--fakeOnly --looseOnly', meff_lower, meff_upper)

    print('REAL & TIGHT [{0}-{1} GeV]:'.format(meff_lower, meff_upper))
    sys.stdout.flush()
    subprocess.call(real_tight_command, shell=True)
    print()
    print('FAKE & TIGHT [{0}-{1} GeV]:'.format(meff_lower, meff_upper))
    sys.stdout.flush()
    subprocess.call(fake_tight_command, shell=True)
    print()
    print('REAL & LOOSE [{0}-{1} GeV]:'.format(meff_lower, meff_upper))
    sys.stdout.flush()
    subprocess.call(real_loose_command, shell=True)
    print()
    print('FAKE & LOOSE [{0}-{1} GeV]:'.format(meff_lower, meff_upper))
    sys.stdout.flush()
    subprocess.call(fake_loose_command, shell=True)
    print()
    print()
    print()
