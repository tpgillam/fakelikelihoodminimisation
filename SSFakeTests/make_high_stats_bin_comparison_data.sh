F_BOB_ALL=datafiles/highstats_bob_merge_all_950_1000.txt
F_MM_ALL=datafiles/highstats_mm_merge_all_950_1000.txt
F_BOB_EM=datafiles/highstats_bob_merge_em_950_1000.txt
F_MM_EM=datafiles/highstats_mm_merge_em_950_1000.txt
F_BOB_EM40=datafiles/highstats_bob_merge_em40_950_1000.txt
F_MM_EM40=datafiles/highstats_mm_merge_em40_950_1000.txt

rm -f  $F_BOB_ALL ; touch $F_BOB_ALL ;
rm -f  $F_MM_ALL ;  touch $F_MM_ALL ;
rm -f  $F_BOB_EM ;  touch $F_BOB_EM ;
rm -f  $F_MM_EM ;   touch $F_MM_EM ;
rm -f  $F_BOB_EM40; touch $F_BOB_EM40 ;
rm -f  $F_MM_EM40 ; touch $F_MM_EM40 ;

for i in `seq 0 99`; do
  ./truth_efficiencies.py --merge all 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode all | ./sampler 0 eff_temp.txt >> $F_MM_ALL;
  ./truth_efficiencies.py --merge all 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode all | ./sampler 6 eff_temp.txt 1000 1 | ./fitter.py --fit-mode rawmedian >> $F_BOB_ALL;

  ./truth_efficiencies.py --merge em 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode em | ./sampler 0 eff_temp.txt >> $F_MM_EM;
  ./truth_efficiencies.py --merge em 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode em | ./sampler 6 eff_temp.txt 1000 1 | ./fitter.py --fit-mode rawmedian >> $F_BOB_EM;

  ./truth_efficiencies.py --merge em40 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode em40 | ./sampler 0 eff_temp.txt >> $F_MM_EM40;
  ./truth_efficiencies.py --merge em40 950 1000; cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 >= 950 && $1 < 1000) { print $5,$6,$7,$8; }}' | ./mergecategories.py --merge-mode em40 | ./sampler 6 eff_temp.txt 1000 1 | ./fitter.py --fit-mode rawmedian >> $F_BOB_EM40;
done
