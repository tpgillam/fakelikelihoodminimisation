echo "REAL & TIGHT:"
echo
for i in `cat backgroundsToCombine`; do printf "$i: "; ./roottoinput.py MC/histFitterTree_merge.$i.root --nEvents -1 --fakeOnly --tightOnly --printSumWeights ; done | column -t | sort -k2 -g -r
echo
echo
echo "FAKE & TIGHT:"
echo
for i in `cat backgroundsToCombine`; do printf "$i: "; ./roottoinput.py MC/histFitterTree_merge.$i.root --nEvents -1 --realOnly --tightOnly --printSumWeights ; done | column -t | sort -k2 -g -r
