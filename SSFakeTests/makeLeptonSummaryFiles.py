from groupedBackgroundsToCombine import visual_meff_bin_boundaries, meff_bin_boundaries
import subprocess

def main():
    for i in range(len(meff_bin_boundaries)-1):
        low = meff_bin_boundaries[i]
        high = meff_bin_boundaries[i+1]
        command = './roottoinput.py --cuts "nBJets20 >= 1 and nJets40 >= 2 and meff > {0}000 and meff <= {1}000" --printLeptons > datafiles/leptons_{0}_{1}.txt'.format(low, high)
        print(command)
        subprocess.call(command, shell=True)


if __name__ == '__main__':
    main()
