import math



Params_RealEffEl_EtaBins = [0,0.8,1.37,1.52,1.8,2.01,2.5]
Params_RealEffEl_PtBins = [15,20.,25.,30.,40.,50.,60.,80,100.,7000]
Params_RealEffEl_Eff= [
        0.6902, 0.6521, 0.6781, 0.6810, 0.6855, 0.7539,
        0.7489, 0.7138, 0.7433, 0.7378, 0.7382, 0.8148,
        0.7850, 0.7452, 0.7614, 0.7673, 0.7632, 0.8329,
        0.8285, 0.7888, 0.7899, 0.8071, 0.8014, 0.8494,
        0.8469, 0.8428, 0.8307, 0.8563, 0.8481, 0.8802,
        0.8744, 0.8726, 0.8515, 0.8758, 0.8709, 0.8906,
        0.8829, 0.8754, 0.8525, 0.8792, 0.8668, 0.8806,
        0.8873, 0.8774, 0.8616, 0.8859, 0.8820, 0.8966,
        0.8874, 0.8643, 0.8500, 0.8730, 0.8674, 0.8919 
        ]
Params_RealEffEl_Stat = [
        0.0012, 0.0014, 0.0042, 0.0021, 0.0024, 0.0016,
        0.0008, 0.0010, 0.0025, 0.0015, 0.0018, 0.0011,
        0.0006, 0.0008, 0.0018, 0.0012, 0.0015, 0.0009,
        0.0003, 0.0004, 0.0009, 0.0006, 0.0008, 0.0005,
        0.0003, 0.0004, 0.0009, 0.0006, 0.0007, 0.0005,
        0.0006, 0.0008, 0.0018, 0.0013, 0.0017, 0.0012,
        0.0009, 0.0011, 0.0028, 0.0019, 0.0026, 0.0019,
        0.0016, 0.0022, 0.0053, 0.0035, 0.0046, 0.0035,
        0.0020, 0.0028, 0.0068, 0.0046, 0.0066, 0.0048
        ]
Params_RealEffEl_SystUncorr = [
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03, 0.03, 0.03
        ]



Params_RealEffMu_EtaBins = [0,0.6,1.2,1.8,2.5]
Params_RealEffMu_PtBins = [15.,20.,25.,30.,40.,50.,60.,100.,7000]
Params_RealEffMu_Eff= [
        0.8889, 0.9199, 0.9372, 0.9392,
        0.9304, 0.9515, 0.9637, 0.9625,
        0.9530, 0.9671, 0.9746, 0.9721,
        0.9717, 0.9791, 0.9827, 0.9811,
        0.9843, 0.9882, 0.9905, 0.9898,
        0.9880, 0.9907, 0.9921, 0.9915,
        0.9898, 0.9920, 0.9928, 0.9919,
        0.9902, 0.9922, 0.9926, 0.9909
        ]
Params_RealEffMu_Stat=[
        0.0010, 0.0008, 0.0007, 0.0007,
        0.0005, 0.0004, 0.0004, 0.0004,
        0.0003, 0.0003, 0.0003, 0.0003,
        0.0001, 0.0001, 0.0001, 0.0001,
        0.0001, 0.0001, 0.0001, 0.0001,
        0.0002, 0.0002, 0.0002, 0.0002,
        0.0002, 0.0002, 0.0002, 0.0003,
        0.0008, 0.0007, 0.0007, 0.0010
        ]
Params_RealEffMu_SystUncorr = [
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03,
        0.03, 0.03, 0.03, 0.03
        ]



# Powheg for diboson (ZZ)
Params_FakeRateEl_Bins = [(15,20,0,1.5),(15,20,1.5,2.5),(20,25,0,1.5),(20,25,1.5,2.5),(25,35,0,1.5),
        (25,35,1.5,2.5),(35,45,0.,2.5),(45,65,0,2.5),(65,7000,0,2.5)]

# pt muon at 40 ; b-jet at 25  
Params_FakeRateEl_noB_Eff = [0.0350, 0.0554, 0.0515,0.0750,0.0317,0.0704, 0.1001, 0.1071,0.1306]
Params_FakeRateEl_noB_Stat = [0.0058,0.0078,0.0106,0.0135,0.0097,0.0131,0.0141,0.0191,0.0286]
Params_FakeRateEl_noB_SystUncorr = [0.0350*0.080, 0.0554*0.297, 0.0515*0.401,0.0750*0.789, 0.0317*0.478,			
        0.0704*0.542, 0.1001*0.589, 0.1071*0.582,0.1306*0.519]
Params_FakeRateEl_noB_SystCorr = [0.0049, 0.0033, 0.0076, 0.0057, 0.0135,0.0093, 0.0156, 0.0323, 0.0510]

# pt muon at 40 ; b-jet at 25  
Params_FakeRateEl_hasB_Eff = [0.0547, 0.0591, 0.1008,0.0828, 0.0353, 0.0447, 0.0795, 0.0426, 0.0552]
Params_FakeRateEl_hasB_Stat = [0.0153, 0.0241, 0.0281, 0.0451, 0.0179, 0.0406, 0.0336, 0.0418, 0.0723]
Params_FakeRateEl_hasB_SystUncorr = [0.0547*0.393,0.0591*0.533, 0.1008*0.479,0.0828*0.426, 0.0353*0.513, 0.0447*0.793, 
        0.0795*0.391, 0.0426*0.521, 0.0552*1.268]
Params_FakeRateEl_hasB_SystCorr = [0.0028,0.0027, 0.0045, 0.0081, 0.0055, 0.0162,0.0165, 0.0353, 0.0856]


Params_FakeRateMu_Bins = [(15,20,0,2.5),(20,25,0,2.5),(25,40,0,2.5),(40,7000,0,2.5)]
Params_FakeRateMu_Eff = [0.1075, 0.0870, 0.1278, 0.1278*1.16]
Params_FakeRateMu_Stat = [0.0190, 0.0312, 0.0510, 0.14]
Params_FakeRateMu_SystUncorr = [0.1075*0.380, 0.0870*0.650, 0.1278*0.916, 0.1278*1.16*0.337]
Params_FakeRateMu_SystCorr = [0.0122, 0.0306, 0.0906, 0] 



def get_efficiency_index(isRealEff, isEl, ptInGeV, eta, hasB=None):
    eta = abs(eta)

    n_realEff_El = (len(Params_RealEffEl_EtaBins) - 1) * (len(Params_RealEffEl_PtBins) - 1)
    n_realEff_Mu = (len(Params_RealEffMu_EtaBins) - 1) * (len(Params_RealEffMu_PtBins) - 1)
    n_fakeEff_El = len(Params_FakeRateEl_Bins) * 2
    n_fakeEff_Mu = len(Params_FakeRateMu_Bins)

    category_index = 0
    if isRealEff and isEl:
        category_index += 0
        pt_bin = get_1D_bin_index(Params_RealEffEl_PtBins, ptInGeV)
        eta_bin = get_1D_bin_index(Params_RealEffEl_EtaBins, eta)
        category_index += pt_bin*(len(Params_RealEffEl_EtaBins) - 1) + eta_bin
    elif isRealEff and not isEl:
        category_index += n_realEff_El
        pt_bin = get_1D_bin_index(Params_RealEffMu_PtBins, ptInGeV)
        eta_bin = get_1D_bin_index(Params_RealEffMu_EtaBins, eta)
        category_index += pt_bin*(len(Params_RealEffMu_EtaBins) - 1) + eta_bin
    elif not isRealEff and isEl:
        category_index += 0
        if hasB is None:
            raise RuntimeError("Need to say whether we have a b-jet!")
        if hasB:
            category_index += n_fakeEff_El / 2
        category_index += get_2D_bin_index(Params_FakeRateEl_Bins, (ptInGeV, eta))
    elif not isRealEff and not isEl:
        category_index += n_fakeEff_El
        category_index += get_2D_bin_index(Params_FakeRateMu_Bins, (ptInGeV, eta))
    return category_index
    

def get_1D_bin_index(edges, value):
    bin_index = 0
    for upper_edge in edges[1:]:
        if value < upper_edge:
            return bin_index
        bin_index += 1
        continue
    raise RuntimeError("Couldn't find bin! {1} not in {0}".format(edges, value))


def get_2D_bin_index(bins, values):
    bin_index = 0
    for bin in bins:
        if (values[0] >= bin[0] 
                and values[0] < bin[1] 
                and values[1] >= bin[2] 
                and values[1] < bin[3]):
            return bin_index
        bin_index += 1
    raise RuntimeError("Couldn't find bin! {1} not in {0}".format(bins, values))


def real_efficiency_index(bin):
    isRealEff = True
    isEl = (bin[1] == 'e')
    hasB = (bin[2] == 'hasB')
    mid_pt = (float(bin[3][0]) + float(bin[3][1])) / 2.0
    mid_eta = (float(bin[3][2]) + float(bin[3][3])) / 2.0
    return get_efficiency_index(isRealEff, isEl, mid_pt, mid_eta, hasB)

def fake_efficiency_index(bin):
    isRealEff = False
    isEl = (bin[1] == 'e')
    hasB = (bin[2] == 'hasB')
    mid_pt = (float(bin[3][0]) + float(bin[3][1])) / 2.0
    mid_eta = (float(bin[3][2]) + float(bin[3][3])) / 2.0
    return get_efficiency_index(isRealEff, isEl, mid_pt, mid_eta, hasB)


def crossing_edges(edges, bin_low, bin_high):
    result = []
    for edge in edges:
        if edge > bin_low and edge < bin_high:
            result.append(edge)
    return result


def delimiting_edges(edges, bin_low, bin_high):
    result = [bin_low]
    result += crossing_edges(edges, bin_low, bin_high)
    result.append(bin_high)
    return result


def pt_eta_limits(flavour):
    if flavour == 'm':
        real_eta_bins = Params_RealEffMu_EtaBins 
        real_pt_bins = Params_RealEffMu_PtBins 
        fake_bins = Params_FakeRateMu_Bins 
    else:
        real_eta_bins = Params_RealEffEl_EtaBins 
        real_pt_bins = Params_RealEffEl_PtBins 
        fake_bins = Params_FakeRateEl_Bins

    for bin in fake_bins:
        pt_low = bin[0]
        pt_high = bin[1]
        eta_low = bin[2]
        eta_high = bin[3]
        delimiting_pt_edges = delimiting_edges(real_pt_bins, pt_low, pt_high)
        delimiting_eta_edges = delimiting_edges(real_eta_bins, eta_low, eta_high)
        for i_pt in range(len(delimiting_pt_edges) - 1):
            for i_eta in range(len(delimiting_eta_edges) - 1):
                yield (delimiting_pt_edges[i_pt], delimiting_pt_edges[i_pt+1],
                        delimiting_eta_edges[i_eta], delimiting_eta_edges[i_eta+1])


def generate_bins():
    bin_index = 0
    for flavour in ['e', 'm']:
        if flavour == 'e':
            bjets_list = ['noB', 'hasB']
        else:
            bjets_list = ['anyB']
        for bjets in bjets_list:
            for pt_eta_bin in pt_eta_limits(flavour):
                yield (bin_index, flavour, bjets, pt_eta_bin)
                bin_index += 1


def print_complex_configuration():
    """Print out the efficiency configuration file needed for my software.
    Note that it doesn't (yet) take into account correlation properly in
    fake efficiencies."""
    # Print version information
    print('VERSION')
    print('WITH_CORRELATIONS')
    print
    print('CATEGORIES')
    for bin in generate_bins():
        print('{0} {1}'.format(real_efficiency_index(bin), fake_efficiency_index(bin)))
    print
    print('REAL')
    for i in range(len(Params_RealEffEl_Eff)):
        eff = Params_RealEffEl_Eff[i]
        stat = Params_RealEffEl_Stat[i]
        systuncorr = Params_RealEffEl_SystUncorr[i]
        print(eff, math.sqrt(stat**2 + systuncorr**2))
    for i in range(len(Params_RealEffMu_Eff)):
        eff = Params_RealEffMu_Eff[i]
        stat = Params_RealEffMu_Stat[i]
        systuncorr = Params_RealEffMu_SystUncorr[i]
        print(eff, math.sqrt(stat**2 + systuncorr**2))
    print
    print('FAKE')
    print('\nCORRELATION_GROUP')
    for i in range(len(Params_FakeRateEl_noB_Eff)):
        eff = Params_FakeRateEl_noB_Eff[i]
        stat = Params_FakeRateEl_noB_Stat[i]
        systuncorr = Params_FakeRateEl_noB_SystUncorr[i]
        systcorr = Params_FakeRateEl_noB_SystCorr[i]
        print(eff, math.sqrt(stat**2 + systuncorr**2), systcorr)
    print('\nCORRELATION_GROUP')
    for i in range(len(Params_FakeRateEl_hasB_Eff)):
        eff = Params_FakeRateEl_hasB_Eff[i]
        stat = Params_FakeRateEl_hasB_Stat[i]
        systuncorr = Params_FakeRateEl_hasB_SystUncorr[i]
        systcorr = Params_FakeRateEl_hasB_SystCorr[i]
        print(eff, math.sqrt(stat**2 + systuncorr**2), + systcorr)
    print('\nCORRELATION_GROUP')
    for i in range(len(Params_FakeRateMu_Eff)):
        eff = Params_FakeRateMu_Eff[i]
        stat = Params_FakeRateMu_Stat[i]
        systuncorr = Params_FakeRateMu_SystUncorr[i]
        systcorr = Params_FakeRateMu_SystCorr[i]
        print(eff, math.sqrt(stat**2 + systuncorr**2), systcorr)


if __name__ == '__main__':
    # print(get_efficiency_index(True, True, 16, 0.1))
    # print(get_efficiency_index(True, True, 16, 0.9))
    # print(get_efficiency_index(True, True, 21, 0.1))
    # print(get_efficiency_index(True, False, 23, 0.1))
    # for bin in generate_bins():
    #     print('{0}: {1} {2} {3}    [{4} {5}]'.format(bin[0], bin[1], bin[2], bin[3], real_efficiency_index(bin), fake_efficiency_index(bin)))
    print_complex_configuration()

