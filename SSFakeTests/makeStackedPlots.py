#!/usr/bin/env python3.4

from groupedBackgroundsToCombine import visual_meff_bin_boundaries, meff_bin_boundaries
from array import array
import math
import ROOT
import subprocess
import os
ROOT.gROOT.SetBatch()


class DataForTag:
    def __init__(self):
        self.weights = [[], [], []]
        self.errs = [[], [], []]
        self.tot_weights = None
        self.tot_errs = None

    def append(self, x_value, line):
        bkg = line.split(':')[0].strip()
        i = self._index_for_background(bkg)
        weight = float(line.split()[1].strip())
        self.weights[i].append(weight)
        err = float(line.split()[2].strip())
        self.errs[i].append(err)

    def _index_for_background(self, bkg):
        bkgs = ['zJets', 'topX', 'diBoson'] 
        return bkgs.index(bkg)

    def _compute_tots(self):
        self.tot_weights = [0.0] * len(self.errs[0])
        self.tot_errs = [0.0] * len(self.errs[0])
        for i in range(len(self.errs[0])):
            for j in range(3):
                self.tot_weights[i] += self.weights[j][i]
                self.tot_errs[i] += self.errs[j][i] ** 2
            self.tot_errs[i] = math.sqrt(self.tot_errs[i])

    def hist(self, filename):
        self._compute_tots()

        c = ROOT.TCanvas('moo', 'moo', 500, 400)
        hist0 = ROOT.TH1D('moo0', 'moo0', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
        hist1 = ROOT.TH1D('moo1', 'moo1', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
        hist2 = ROOT.TH1D('moo2', 'moo2', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
        hist0.SetFillColor(ROOT.kRed-3)
        hist1.SetFillColor(ROOT.kSpring-5)
        hist2.SetFillColor(ROOT.kAzure-1)
        stack = ROOT.THStack()
        for i in range(3):
            hist = locals()['hist{}'.format(i)]
            hist.Sumw2()
            hist.SetLineWidth(0)
            hist.SetLineColor(hist.GetFillColor())
            for x,weight in enumerate(self.weights[i]):
                hist.SetBinContent(x+1, weight)
            stack.Add(hist)
        stack.Draw('hist')
        stack.GetXaxis().SetTitle('m_{eff} [GeV]')
        stack.GetYaxis().SetTitle('Expected events / bin')
        stack.GetXaxis().SetTitleSize(0.05)
        stack.GetYaxis().SetTitleSize(0.05)
        stack.GetXaxis().SetTitleOffset(0.85)
        stack.GetYaxis().SetTitleOffset(0.85)

        graph = ROOT.TGraphAsymmErrors(hist0.GetNbinsX())
        graph.SetFillColorAlpha(ROOT.kGray+3, 0.25)
        for i in range(0, hist0.GetNbinsX()):
            graph.SetPoint(i, hist0.GetBinCenter(i+1), self.tot_weights[i])
            graph.SetPointEXlow(i, hist0.GetBinCenter(i+1) - hist0.GetBinLowEdge(i+1))
            graph.SetPointEXhigh(i, hist0.GetBinLowEdge(i+2) - hist0.GetBinCenter(i+1))
            graph.SetPointEYhigh(i, self.tot_errs[i])
            graph.SetPointEYlow(i, self.tot_errs[i])
        graph.Draw('e2')

        legend = ROOT.TLegend(0.65,0.7,0.95,0.88)
        legend.SetTextFont(42)
        legend.SetTextSize(0.05)
        legend.SetFillStyle(0)
        legend.SetBorderSize(0)
        legend.AddEntry(hist2, 'VV + VVV', 'f')
        legend.AddEntry(hist1, 'top + X', 'f')
        legend.AddEntry(hist0, 'Z + jets', 'f')
        legend.Draw()

        c.Update()
        c.SaveAs(filename)
        hist0.Delete()
        hist1.Delete()
        hist2.Delete()


def real_and_fake_hist(real_data, fake_data, filename, datafile=None, data_tight=None, log=False, fake_label=None, fake_color=None):
    real_data._compute_tots()
    fake_data._compute_tots()

    tot_weights = [0.0] * len(real_data.tot_errs)
    tot_errs_up = [0.0] * len(real_data.tot_errs)
    tot_errs_down = [0.0] * len(real_data.tot_errs)
    plot_maximum = 0
    for i in range(len(real_data.tot_errs)):
        tot_weights[i] += real_data.tot_weights[i]
        tot_weights[i] += fake_data.tot_weights[i]
        plot_maximum = max(plot_maximum, tot_weights[i])
        if ('tot_errs_up' in fake_data.__dict__):
            tot_errs_up[i] = math.sqrt(
                    real_data.tot_errs[i] ** 2 + fake_data.tot_errs_up[i] ** 2)
            tot_errs_down[i] = math.sqrt(
                    real_data.tot_errs[i] ** 2 + fake_data.tot_errs_down[i] ** 2)
        else:
            tot_errs_up[i] = math.sqrt(
                    real_data.tot_errs[i] ** 2 + fake_data.tot_errs[i] ** 2)
            tot_errs_down[i] = tot_errs_up[i]


    c = ROOT.TCanvas('moo', 'moo', 500, 400)
    if log:
        c.SetLogy()
    hist0 = ROOT.TH1D('moo0', 'moo0', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
    hist1 = ROOT.TH1D('moo1', 'moo1', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
    hist0.SetFillColor(ROOT.kAzure-8)
    if fake_label is None:
        hist1.SetFillColor(ROOT.kOrange-9)
    elif fake_color is None:
        hist1.SetFillColor(ROOT.kMagenta-6)
    else:
        hist1.SetFillColor(fake_color)
    stack = ROOT.THStack()
    data = [real_data, fake_data]
    for i in range(2):
        hist = locals()['hist{}'.format(i)]
        hist.SetLineWidth(0)
        hist.SetLineColor(hist.GetFillColor())
        for x,weight in enumerate(data[i].tot_weights):
            hist.SetBinContent(x+1, weight)
        stack.Add(hist)
    stack.Draw('hist')
    stack.GetXaxis().SetTitle('m_{eff} [GeV]')
    stack.GetYaxis().SetTitle('Expected events / bin')
    stack.GetXaxis().SetTitleSize(0.05)
    stack.GetYaxis().SetTitleSize(0.05)
    stack.GetXaxis().SetTitleOffset(0.85)
    stack.GetYaxis().SetTitleOffset(0.85)
    stack.SetMinimum(1)
    if plot_maximum < 300:
        plot_maximum = 200
    else:
        plot_maximum = 900
    stack.SetMaximum(plot_maximum)

    graph = ROOT.TGraphAsymmErrors(hist0.GetNbinsX())
    graph.SetFillColorAlpha(ROOT.kGray+3, 0.25)
    for i in range(0, hist0.GetNbinsX()):
        graph.SetPoint(i, hist0.GetBinCenter(i+1), tot_weights[i])
        graph.SetPointEXlow(i, hist0.GetBinCenter(i+1) - hist0.GetBinLowEdge(i+1))
        graph.SetPointEXhigh(i, hist0.GetBinLowEdge(i+2) - hist0.GetBinCenter(i+1))
        graph.SetPointEYhigh(i, tot_errs_up[i])
        graph.SetPointEYlow(i, tot_errs_down[i])
    graph.Draw('e2')
    c.Update()

    data_hist = None
    if datafile is not None:
        data_hist = make_data_point_hist(datafile, data_tight)
        data_hist.Draw('pe1 same')

    legend = ROOT.TLegend(0.60,0.70,0.95,0.88)
    legend.SetTextFont(42)
    legend.SetTextSize(0.05)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    if data_hist is not None:
        legend.AddEntry(data_hist, 'Pseudo-data', 'pe')
    if fake_label is None:
        legend.AddEntry(hist1, 'Fake', 'f')
    else:
        legend.AddEntry(hist1, fake_label, 'f')
    legend.AddEntry(hist0, 'Real', 'f')
    legend.Draw()

    c.SaveAs(filename)
    hist0.Delete()
    hist1.Delete()
    if data_hist is not None:
        data_hist.Delete()


def make_data_point_hist(datafile, data_tight):
    hist = ROOT.TH1D('moodata', 'moodata', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))
    hist.Sumw2()
    hist.SetMarkerStyle(20)
    with open(datafile) as f:
        for line in f:
            toks = line.split()
            t1 = int(toks[4])
            t2 = int(toks[5])
            is_event_tight = (t1 == 1 and t2 == 1)
            if not (is_event_tight == data_tight):
                continue
            meff = float(toks[0])
            # Make last bin overflow
            if meff >= visual_meff_bin_boundaries[-1]:
                meff = visual_meff_bin_boundaries[-1]-1
            hist.Fill(meff)
    return hist


def ratio_plot(fake_truth_data, fake_estimate_datasets, titles, colours, filename, max_y=4):
    fake_truth_data._compute_tots()
    for fake_estimate_data in fake_estimate_datasets:
        fake_estimate_data._compute_tots()

    num_bins = len(fake_truth_data.tot_errs)
    num_estimates = len(fake_estimate_datasets)

    truth_central = [1.0] * num_bins
    truth_errs_up = [0.0] * num_bins
    truth_errs_down = [0.0] * num_bins
    estimate_central = [[0.0] * num_bins for i in range(num_estimates)]
    estimate_errs_up = [[0.0] * num_bins for i in range(num_estimates)]
    estimate_errs_down = [[0.0] * num_bins for i in range(num_estimates)]

    for i in range(num_bins):
        if 'tot_errs_up' in fake_truth_data.__dict__:
            truth_errs_up[i] = fake_truth_data.tot_errs_up[i] / fake_truth_data.tot_weights[i]
            truth_errs_down[i] = fake_truth_data.tot_errs_down[i] / fake_truth_data.tot_weights[i]
        else:
            truth_errs_up[i] = fake_truth_data.tot_errs[i] / fake_truth_data.tot_weights[i]
            truth_errs_down[i] = truth_errs_up[i]
        for j in range(num_estimates):
            estimate_central[j][i] = fake_estimate_datasets[j].tot_weights[i] / fake_truth_data.tot_weights[i]
            if 'tot_errs_up' in fake_estimate_datasets[j].__dict__:
                estimate_errs_up[j][i] = fake_estimate_datasets[j].tot_errs_up[i] / fake_estimate_datasets[j].tot_weights[i]
                estimate_errs_down[j][i] = fake_estimate_datasets[j].tot_errs_down[i] / fake_estimate_datasets[j].tot_weights[i]
            else:
                estimate_errs_up[j][i] = fake_estimate_datasets[j].tot_errs[i] / fake_estimate_datasets[j].tot_weights[i]
                estimate_errs_down[j][i] = estimate_errs_up[j][i]

    c = ROOT.TCanvas()

    silly_hist = ROOT.TH1D('moo0', 'moo0', len(visual_meff_bin_boundaries)-1, array('d', visual_meff_bin_boundaries))

    truth_graph = ROOT.TGraphAsymmErrors(silly_hist.GetNbinsX())
    truth_graph.SetFillColorAlpha(ROOT.kGray+3, 0.25)
    estimate_graphs = [ROOT.TGraphAsymmErrors(silly_hist.GetNbinsX()) for i in range(num_estimates)]
    for i in range(0, silly_hist.GetNbinsX()):
        for j,estimate_graph in enumerate(estimate_graphs):
            bin_width = silly_hist.GetBinLowEdge(i+1) - silly_hist.GetBinLowEdge(i)
            if len(fake_estimate_datasets) == 1:
                shift = 0
            else:
                tot_shift_width = 0.4 * bin_width
                shift_width = tot_shift_width / (num_estimates - 1)
                shift = j * shift_width - tot_shift_width / 2
            estimate_graph.SetPoint(i, silly_hist.GetBinCenter(i+1)+shift, estimate_central[j][i])
            estimate_graph.SetPointEYhigh(i, estimate_errs_up[j][i])
            estimate_graph.SetPointEYlow(i, estimate_errs_down[j][i])
        truth_graph.SetPoint(i, silly_hist.GetBinCenter(i+1), truth_central[i])
        truth_graph.SetPointEXlow(i, silly_hist.GetBinCenter(i+1) - silly_hist.GetBinLowEdge(i+1))
        truth_graph.SetPointEXhigh(i, silly_hist.GetBinLowEdge(i+2) - silly_hist.GetBinCenter(i+1))
        truth_graph.SetPointEYhigh(i, truth_errs_up[i])
        truth_graph.SetPointEYlow(i, truth_errs_down[i])

    silly_hist.Delete()

    first = True
    for i,estimate_graph in enumerate(estimate_graphs):
        estimate_graph.SetMarkerStyle(20)
        estimate_graph.GetXaxis().SetTitle('m_{eff} [GeV]')
        estimate_graph.GetYaxis().SetTitle('Fake rate ratio: estimated / truth')
        estimate_graph.GetXaxis().SetTitleSize(0.05)
        estimate_graph.GetYaxis().SetTitleSize(0.05)
        estimate_graph.GetXaxis().SetTitleOffset(0.85)
        estimate_graph.GetYaxis().SetTitleOffset(0.85)
        estimate_graph.SetTitle('')
        estimate_graph.SetMinimum(0)
        estimate_graph.SetMaximum(max_y)
        estimate_graph.SetLineColor(colours[i])
        estimate_graph.SetMarkerColor(colours[i])
        if first:
            estimate_graph.Draw('ape0')
            first = False
        else:
            estimate_graph.Draw('pe0')

    truth_graph.Draw('e2')

    legend = ROOT.TLegend(0.15,0.6,0.5,0.88)
    legend.SetTextFont(42)
    legend.SetTextSize(0.05)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.AddEntry(truth_graph, 'Truth', 'f')
    for i,estimate_graph in enumerate(estimate_graphs):
        legend.AddEntry(estimate_graph, titles[i], 'pe')
    legend.Draw()

    c.SaveAs(filename)
    for estimate_graph in estimate_graphs:
        estimate_graph.Delete()
    truth_graph.Delete()


def data_from_summary_table(filename):
    result = {}
    current_tag = None
    x_value = None
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if line == '': continue
            if line.startswith('#'): continue
            # Is this a tagline?
            if line.startswith('R') or line.startswith('F'):
                current_tag = line.split('[')[0].strip()
                x_value = float(line.split('[')[1].split('-')[0].strip()) + 1
                continue

            if not current_tag in result:
                result[current_tag] = DataForTag()
            data = result[current_tag]
            data.append(x_value, line)
    return result


def matrix_method_prediction(filename, meff_lower, meff_upper, with_merge=False, merge_mode='all', with_truth=False):
    """
    Return the mean prediction and uncertainty for the fake & tight
    matrix method prediction.
    meff_upper=None means there is no upper bound.
    """
    if meff_upper is not None:
        data_command = "cat {0} | gawk '{{if ($1 >= {1} && $1 < {2}) {{ print $5,$6,$7,$8; }}}}'".format(filename, meff_lower, meff_upper)
    else:
        data_command = "cat {0} | gawk '{{if ($1 >= {1}) {{ print $5,$6,$7,$8; }}}}'".format(filename, meff_lower)

    if with_merge or with_truth:
        sampler_command = './mergecategories.py --merge-mode {} --eff-out /dev/null | ./sampler 0 eff_temp.txt'.format(merge_mode)
    else:
        sampler_command = './sampler 0 effConfigTwoLepOJ.txt'

    command = '{0} | {1}'.format(data_command, sampler_command)
    # Do the extra step if necessary
    if with_merge:
        command = '{0} | ./mergecategories.py --merge-mode {1} > /dev/null; {2}'.format(data_command, merge_mode, command)
    elif with_truth:
        command = './truth_efficiencies.py --merge-mode {0} {1} {2}; {3}'.format(merge_mode, meff_lower, meff_upper, command)
    raw = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()[0]
    return (float(raw.split()[3]), float(raw.split()[5]))


def uncle_bob_prediction(filename, meff_lower, meff_upper, gibbs_steps, with_merge=False, merge_mode='all', fit_mode='gamma', fit_plot_filename=None, with_truth=False):
    """
    Return the mean prediction and 'up' and 'down' uncertainties separately for
    the fake & tight "BoB sampler" prediction.
    meff_upper=None means there is no upper bound.
    fit_plot_filename == None => make up sensible filename
    """

    # if fit_plot_filename is None:
    #     fit_plot_filename = 'plots/fits/{0}_{1}steps_{2}_{3}_merge_{4}_fitmode_{5}.pdf'.format(os.path.basename(filename).strip('.txt'), 
    #             gibbs_steps, meff_lower, meff_upper, merge_mode, fit_mode)

    if meff_upper is not None:
        data_command = "cat {0} | gawk '{{if ($1 >= {1} && $1 < {2}) {{ print $5,$6,$7,$8; }}}}'".format(filename, meff_lower, meff_upper)
    else:
        data_command = "cat {0} | gawk '{{if ($1 >= {1}) {{ print $5,$6,$7,$8; }}}}'".format(filename, meff_lower)

    if with_merge or with_truth:
        sampler_command = './mergecategories.py --merge-mode {0} --eff-out /dev/null | ./sampler 6 eff_temp.txt 1000 {1}'.format(merge_mode, gibbs_steps)
    else:
        sampler_command = './sampler 6 effConfigTwoLepOJ.txt 1000 {}'.format(gibbs_steps)

    command = '{0} | {1} | ./fitter.py --fit-mode {2}'.format(data_command, sampler_command, fit_mode)
    if fit_plot_filename is not None:
        command += ' {}'.format(fit_plot_filename)
    # Do the extra step if necessary
    if with_merge:
        command = '{0} | ./mergecategories.py --merge-mode {1} > /dev/null; {2}'.format(data_command, merge_mode, command)
    elif with_truth:
        command = './truth_efficiencies.py --merge-mode {0} {1} {2}; {3}'.format(merge_mode, meff_lower, meff_upper, command)
    print(command)
    raw = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()[0]
    toks = [float(x) for x in raw.split()]
    return tuple(toks)


class MatrixMethodData:
    def __init__(self, filename, with_merge=False, merge_mode='all', with_truth=False):
        self.tot_weights = []
        self.tot_errs = []

        for i in range(len(meff_bin_boundaries)-1):
            meff_lower = meff_bin_boundaries[i]
            meff_upper = meff_bin_boundaries[i+1]
            w, uw = matrix_method_prediction(filename, meff_lower, meff_upper, with_merge, merge_mode, with_truth)
            self.tot_weights.append(w)
            self.tot_errs.append(uw)

    def _compute_tots(self):
        '''Exists to make interface compatible'''
        pass


class UncleBobData:
    def __init__(self, filename=None, gibbs_steps=1, with_merge=False, merge_mode='all', fit_mode='gamma', with_truth=False):
        self.tot_weights = []
        self.tot_errs_up = []
        self.tot_errs_down = []
        if filename is None:
            return

        for i in range(len(meff_bin_boundaries)-1):
            meff_lower = meff_bin_boundaries[i]
            meff_upper = meff_bin_boundaries[i+1]
            tup = uncle_bob_prediction(filename, meff_lower, meff_upper, 
                    gibbs_steps, with_merge, merge_mode, fit_mode, None, with_truth)
            w, uw_down, uw_up = tup
            self.tot_weights.append(w)
            self.tot_errs_up.append(uw_up)
            self.tot_errs_down.append(uw_down)

    @classmethod
    def gen_multiple(cls, filename, gibbs_steps, with_merge=False, merge_mode='all', fit_mode='gamma', with_truth=False):
        results = None
        num_results = -1
        for i in range(len(meff_bin_boundaries)-1):
            meff_lower = meff_bin_boundaries[i]
            meff_upper = meff_bin_boundaries[i+1]
            tup = uncle_bob_prediction(filename, meff_lower, meff_upper, 
                    gibbs_steps, with_merge, merge_mode, fit_mode, None, with_truth)
            if num_results == -1:
                num_results = math.floor(len(tup) / 3)
                results = tuple(cls() for i in range(num_results))
            for j in range(num_results):
                w, uw_down, uw_up = tup[3*j : 3*(j+1)]
                results[j].tot_weights.append(w)
                results[j].tot_errs_up.append(uw_up)
                results[j].tot_errs_down.append(uw_down)
        return results

    def _compute_tots(self):
        '''Exists to make interface compatible'''
        pass



def main():
    data = data_from_summary_table('datafiles/summaryTables.txt')

    # data['FAKE & TIGHT']._compute_tots()
    # print(data['FAKE & TIGHT'].tot_weights)
    # sys.exit()

    data['REAL & TIGHT'].hist('plots/realAndTightRates.pdf')
    data['FAKE & TIGHT'].hist('plots/fakeAndTightRates.pdf')
    data['REAL & LOOSE'].hist('plots/realAndLooseRates.pdf')
    data['FAKE & LOOSE'].hist('plots/fakeAndLooseRates.pdf')

    real_and_fake_hist(data['REAL & TIGHT'], data['FAKE & TIGHT'], 'plots/overallTightRates.pdf')
    real_and_fake_hist(data['REAL & LOOSE'], data['FAKE & LOOSE'], 'plots/overallLooseRates.pdf')
    real_and_fake_hist(data['REAL & TIGHT'], data['FAKE & TIGHT'], 'plots/overallLogTightRates.pdf', log=True)
    real_and_fake_hist(data['REAL & LOOSE'], data['FAKE & LOOSE'], 'plots/overallLogLooseRates.pdf', log=True)

    # for i in range(10):
    for i in range(1):
        pseudo_data = 'datafiles/pseudoData_{}.txt'.format(i)

        real_and_fake_hist(data['REAL & TIGHT'], data['FAKE & TIGHT'], 
                'plots/overallLogTightRates_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True)
        real_and_fake_hist(data['REAL & LOOSE'], data['FAKE & LOOSE'], 
                'plots/overallLogLooseRates_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=False, log=True)


        mm_data = MatrixMethodData(pseudo_data)
        real_and_fake_hist(data['REAL & TIGHT'], mm_data, 
                'plots/overallLogTightRatesMM_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        mm_data_with_merge_all = MatrixMethodData(pseudo_data, True, 'all')
        real_and_fake_hist(data['REAL & TIGHT'], mm_data_with_merge_all, 
                'plots/overallLogTightRatesMMMergedEff_all_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        mm_data_with_merge_em = MatrixMethodData(pseudo_data, True, 'em')
        real_and_fake_hist(data['REAL & TIGHT'], mm_data_with_merge_em, 
                'plots/overallLogTightRatesMMMergedEff_em_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        mm_data_with_merge_em40 = MatrixMethodData(pseudo_data, True, 'em40')
        real_and_fake_hist(data['REAL & TIGHT'], mm_data_with_merge_em40, 
                'plots/overallLogTightRatesMMMergedEff_em40_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        ratio_plot(data['FAKE & TIGHT'], 
                [mm_data, mm_data_with_merge_all, mm_data_with_merge_em, mm_data_with_merge_em40], 
                ['No merge', 'Merge all', 'Separate e/#mu', 'Separate e/#mu and p_{T} cut'],
                [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kGreen],
                'plots/ratios_MM_{}.pdf'.format(i))

        ratio_plot(data['FAKE & TIGHT'], 
                [mm_data ], 
                ['Matrix method'],
                [ROOT.kBlack],
                'plots/ratios_MMsimple_{}.pdf'.format(i))



        bob_data_with_merge_em_1gibbs, bob_data_with_merge_em_1gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 1, True, 'em', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_1gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_em_1steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_1gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_em_1steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        bob_data_with_merge_em_10gibbs, bob_data_with_merge_em_10gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 10, True, 'em', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_10gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_em_10steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_10gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_em_10steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        bob_data_with_merge_em_25gibbs, bob_data_with_merge_em_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, True, 'em', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_25gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_em_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_em_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        bob_data_with_merge_em_50gibbs, bob_data_with_merge_em_50gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 50, True, 'em', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_50gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_em_50steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em_50gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_em_50steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        ratio_plot(data['FAKE & TIGHT'], 
                [
                    bob_data_with_merge_em_1gibbs_rawmedian,
                    bob_data_with_merge_em_10gibbs_rawmedian,
                    bob_data_with_merge_em_25gibbs_rawmedian,
                    bob_data_with_merge_em_50gibbs_rawmedian,
                    ],
                ['1 step', '10 steps', '25 steps', '50 steps'],
                [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kGreen],
                'plots/ratios_BoB_merge_em_compareGibbs_{}.pdf'.format(i))



        bob_data_with_merge_all_25gibbs, bob_data_with_merge_all_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, True, 'all', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_all_25gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_all_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_all_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_all_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        bob_data_with_merge_em40_25gibbs, bob_data_with_merge_em40_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, True, 'em40', 'all')
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em40_25gibbs, 
                'plots/overallLogTightRatesBoBMergedEff_em40_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_with_merge_em40_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBMergedEff_em40_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)


        ratio_plot(data['FAKE & TIGHT'], 
                [
                    bob_data_with_merge_all_25gibbs_rawmedian,
                    bob_data_with_merge_em_25gibbs_rawmedian,
                    bob_data_with_merge_em40_25gibbs_rawmedian,
                    ],
                ['Merge all', 'Separate e/#mu', 'Separate e/#mu and p_{T} cut'],
                [ROOT.kRed, ROOT.kBlue, ROOT.kGreen],
                'plots/ratios_BoB_25steps_compareMerge_{}.pdf'.format(i))





        ################################
        # TRUTH EFFICIENCY COMPARISONS #
        ################################


        mm_trutheff_all_data = MatrixMethodData(pseudo_data, False, 'all', True)
        real_and_fake_hist(data['REAL & TIGHT'], mm_trutheff_all_data, 
                'plots/overallLogTightRatesMMTruthEff_all_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        bob_data_trutheff_all_25gibbs, bob_data_trutheff_all_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, False, 'all', 'all', True)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_all_25gibbs, 
                'plots/overallLogTightRatesBoBTruthEff_all_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_all_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBTruthEff_all_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)


        ratio_plot(data['FAKE & TIGHT'], 
                [mm_trutheff_all_data, bob_data_trutheff_all_25gibbs_rawmedian ], 
                ['MM', 'Bayesian'],
                [ROOT.kBlack, ROOT.kRed],
                'plots/ratios_trutheff_all_{}.pdf'.format(i))


        mm_trutheff_em_data = MatrixMethodData(pseudo_data, False, 'em', True)
        real_and_fake_hist(data['REAL & TIGHT'], mm_trutheff_em_data, 
                'plots/overallLogTightRatesMMTruthEff_em_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        bob_data_trutheff_em_25gibbs, bob_data_trutheff_em_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, False, 'em', 'all', True)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em_25gibbs, 
                'plots/overallLogTightRatesBoBTruthEff_em_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBTruthEff_em_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        ratio_plot(data['FAKE & TIGHT'], 
                [mm_trutheff_em_data, bob_data_trutheff_em_25gibbs_rawmedian ], 
                ['MM', 'Bayesian'],
                [ROOT.kBlack, ROOT.kRed],
                'plots/ratios_trutheff_em_{}.pdf'.format(i))


        mm_trutheff_em40_data = MatrixMethodData(pseudo_data, False, 'em40', True)
        real_and_fake_hist(data['REAL & TIGHT'], mm_trutheff_em40_data, 
                'plots/overallLogTightRatesMMTruthEff_em40_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        bob_data_trutheff_em40_25gibbs, bob_data_trutheff_em40_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, False, 'em40', 'all', True)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em40_25gibbs, 
                'plots/overallLogTightRatesBoBTruthEff_em40_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em40_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBTruthEff_em40_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True,
                fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        ratio_plot(data['FAKE & TIGHT'], 
                [mm_trutheff_em40_data, bob_data_trutheff_em40_25gibbs_rawmedian ], 
                ['MM', 'Bayesian'],
                [ROOT.kBlack, ROOT.kRed],
                'plots/ratios_trutheff_em40_{}.pdf'.format(i))


        mm_trutheff_em3050_data = MatrixMethodData(pseudo_data, False, 'em3050', True)
        real_and_fake_hist(data['REAL & TIGHT'], mm_trutheff_em3050_data, 
                'plots/overallLogTightRatesMMTruthEff_em3050_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='MM')

        bob_data_trutheff_em3050_25gibbs, bob_data_trutheff_em3050_25gibbs_rawmedian = UncleBobData.gen_multiple(pseudo_data, 25, False, 'em3050', 'all', True)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em3050_25gibbs, 
                'plots/overallLogTightRatesBoBTruthEff_em3050_25steps_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='Bayesian',
                fake_color=ROOT.kRed-6)
        real_and_fake_hist(data['REAL & TIGHT'], bob_data_trutheff_em3050_25gibbs_rawmedian, 
                'plots/overallLogTightRatesBoBTruthEff_em3050_25steps_rawmedian_pseudoData_{}.pdf'.format(i), 
                datafile=pseudo_data, data_tight=True, log=True, fake_label='Bayesian',
                fake_color=ROOT.kRed-6)

        ratio_plot(data['FAKE & TIGHT'], 
                [mm_trutheff_em3050_data, bob_data_trutheff_em3050_25gibbs_rawmedian ], 
                ['MM', 'Bayesian'],
                [ROOT.kBlack, ROOT.kRed],
                'plots/ratios_trutheff_em3050_{}.pdf'.format(i))


if __name__ == '__main__':
    main()


