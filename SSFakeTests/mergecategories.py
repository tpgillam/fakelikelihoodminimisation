#!/usr/bin/env python3.4

"""
Take a stream of events from stdin (in the 'sampler' input format, not raw
pseudodata), and outputs a stream with the categories merged according to some
predefined scheme in this script.

The merging is done in a weighted fashion, using numbers of tight leptons to 
weight the real efficiencies, and loose leptons to weight the fake efficiencies.
This is approximate, of course.

The script takes one argument -- the name of the temporary efficiency file to
write, defaulting to "eff_temp.txt". This is guaranteed to be written and closed
before anything is written to stdout.
"""

import argparse
import fileinput
import math
from twoLepFakeExtracts import generate_bins


class BasicEfficiencyFile:
    def __init__(self):
        self.categoryToRealEffIndex = [] 
        self.categoryToFakeEffIndex = [] 
        # Lists of tuples of (mean, unc)
        self.realEffs = []
        self.fakeEffs = []

    def num_categories(self):
        return len(self.categoryToRealEffIndex)

    @classmethod
    def init_from_file(cls, filename):
        obj = cls()
        with open(filename) as f:
            next_version = False
            next_categories = False
            next_real = False
            next_fake = False
            for line in f:
                line = line.strip()
                if line == '': continue
                if line.startswith('#'): continue
                elif line == 'VERSION':
                    next_version = False
                    next_categories = False
                    next_real = False
                    next_fake = False
                    next_version = True
                    continue
                elif line == 'CATEGORIES':
                    next_version = False
                    next_categories = False
                    next_real = False
                    next_fake = False
                    next_categories = True
                    continue
                elif line == 'REAL':
                    next_version = False
                    next_categories = False
                    next_real = False
                    next_fake = False
                    next_real = True
                    continue
                elif line == 'FAKE':
                    next_version = False
                    next_categories = False
                    next_real = False
                    next_fake = False
                    next_fake = True
                    continue

                if next_version:
                    if line != 'BASIC':
                        raise RuntimeError('Only support basic efficiency files')
                if next_categories:
                    obj.categoryToRealEffIndex.append(int(line.split()[0]))
                    obj.categoryToFakeEffIndex.append(int(line.split()[1]))
                if next_real:
                    obj.realEffs.append((float(line.split()[0]), float(line.split()[1])))
                if next_fake:
                    obj.fakeEffs.append((float(line.split()[0]), float(line.split()[1])))
        return obj

    def write_to_file(self, filename):
        '''Write the configuration object described here to a file'''
        with open(filename, 'w') as f:
            f.write('VERSION\n')
            f.write('BASIC\n')
            f.write('\n')
            f.write('CATEGORIES\n')
            for i in range(self.num_categories()):
                f.write('{0} {1}\n'.format(self.categoryToRealEffIndex[i],
                    self.categoryToFakeEffIndex[i]))
            f.write('\n')
            self.write_effs_to_handle(f)
            f.flush()

    def write_effs_to_handle(self, f):
        f.write('REAL\n')
        for eff in self.realEffs:
            f.write('{0} {1}\n'.format(eff[0], eff[1]))
        f.write('\n')
        f.write('FAKE\n')
        for eff in self.fakeEffs:
            f.write('{0} {1}\n'.format(eff[0], eff[1]))


def read_stream():
    lines = []
    for line in fileinput.input('-'):
        toks = line.strip().split()
        # Strip off extra stuff if it looks like it's a pseudo dataset
        if len(toks) > 4:
            toks = toks[-4:]
        toks = [int(x) for x in toks]
        lines.append(toks)
    return lines


def get_merging_scheme():
    """
    Return a dictionary mapping the new category numbers to a list of old
    categories. Every old category should only appear exactly once in 
    total, over all of the lists.
    """
    global options
    mode = options.merge_mode
    result = {}
    if mode == 'all':
        # Everything into one category
        result[0] = list(range(0, 183+1))
    elif mode == 'em':
        # Electrons and muons separately
        result[0] = list(range(0, 151+1))
        result[1] = list(range(152, 183+1))
    elif mode == 'em40':
        # Split electrons and muons, and additionally separate at 40 GeV
        # That is, produce a total of 4 categories
        for j in range(4):
            result[j] = []
        for i,bin in enumerate(generate_bins()):
            is_e = (bin[1] == 'e')
            pt_under_40 = (bin[3][1] <= 40.0)
            if is_e and pt_under_40:
                j = 0
            elif not is_e and pt_under_40:
                j = 1
            elif is_e and not pt_under_40:
                j = 2
            elif not is_e and not pt_under_40:
                j = 3
            result[j].append(i)
    elif mode == 'em3050':
        # Split electrons and muons, and additionally separate at 30 GeV
        # and 50 GeV. That is, produce a total of 6 categories
        for j in range(6):
            result[j] = []
        for i,bin in enumerate(generate_bins()):
            is_e = (bin[1] == 'e')
            pt_under_30 = (bin[3][1] <= 30.0)
            pt_under_50 = (bin[3][1] <= 50.0)
            if is_e and pt_under_30:
                j = 0
            elif not is_e and pt_under_30:
                j = 1
            elif is_e and pt_under_50:
                j = 2
            elif not is_e and pt_under_50:
                j = 3
            elif is_e and not pt_under_50:
                j = 4
            elif not is_e and not pt_under_50:
                j = 5
            result[j].append(i)
    elif mode == 'none':
        for i in range(0, 183+1):
            result[i] = [i]
    else:
        raise RuntimeError('Unsupported merge mode: {}'.format(mode))
    return result


def get_inverse_merging_scheme():
    """Return the inverse dictionary to get_merging_scheme"""
    merging_scheme = get_merging_scheme()
    result = {}
    for new in merging_scheme:
        for old in merging_scheme[new]:
            result[old] = new
    return result
    

def create_new_efficiency_file(data, old_name, new_name):
    """
    Create a new efficiency file based on the old, given the current merging
    scheme.
    """
    eff_old = BasicEfficiencyFile.init_from_file(old_name)
    num_tight = count_tight_per_category(data)
    num_loose = count_loose_per_category(data)
    num = count_per_category(data)

    # The above need to be split into segments for each *destination*
    # category
    merging_scheme = get_merging_scheme()
    eff_new = BasicEfficiencyFile()
    i = 0
    for destination in merging_scheme:
        sources = merging_scheme[destination]
        eff_new.categoryToRealEffIndex.append(i)
        eff_new.categoryToFakeEffIndex.append(i)
        this_num_tight = {k:v for k,v in num_tight.items() if k in sources}
        this_num_loose = {k:v for k,v in num_loose.items() if k in sources}
        this_num = {k:v for k,v in num.items() if k in sources}
        # num_tight_per_real_eff = sum_to_independent_real(this_num_tight, eff_old)
        # num_loose_per_fake_eff = sum_to_independent_fake(this_num_loose, eff_old)
        num_tight_per_real_eff = sum_to_independent_real(this_num, eff_old)
        num_loose_per_fake_eff = sum_to_independent_fake(this_num, eff_old)
        real_eff = 0
        u_real_eff = 0
        total_tight = 0
        for real_eff_i in num_tight_per_real_eff:
            total_tight += num_tight_per_real_eff[real_eff_i]
            real_eff += num_tight_per_real_eff[real_eff_i] * eff_old.realEffs[real_eff_i][0]
            u_real_eff += (num_tight_per_real_eff[real_eff_i] * eff_old.realEffs[real_eff_i][1]) ** 2
        if total_tight > 0:
            real_eff /= total_tight
            u_real_eff = math.sqrt(u_real_eff) / total_tight
        else:
            # They won't be used anyway - but my other program complains
            # if we set them to 0
            real_eff = 0.5
            u_real_eff = 0.5

        fake_eff = 0
        u_fake_eff = 0
        total_loose =0 
        for fake_eff_i in num_loose_per_fake_eff:
            total_loose += num_loose_per_fake_eff[fake_eff_i]
            fake_eff += num_loose_per_fake_eff[fake_eff_i] * eff_old.fakeEffs[fake_eff_i][0]
            u_fake_eff += (num_loose_per_fake_eff[fake_eff_i] * eff_old.fakeEffs[fake_eff_i][1]) ** 2
        if total_loose > 0:
            fake_eff /= total_loose
            u_fake_eff = math.sqrt(u_fake_eff) / total_loose
        else:
            # They won't be used anyway - but my other program complains
            # if we set them to 0
            fake_eff = 0.5
            u_fake_eff = 0.5

        eff_new.realEffs.append((real_eff, u_real_eff))
        eff_new.fakeEffs.append((fake_eff, u_fake_eff))
        i += 1
    eff_new.write_to_file(new_name)


def count_tight_per_category(data):
    return count_per_category(data, True)


def count_loose_per_category(data):
    return count_per_category(data, False)


def count_per_category(data, is_tight=None):
    num = {}
    for event in data:
        if (is_tight is None) or (is_tight and event[0]) or (not is_tight and not event[0]):
            if not event[2] in num:
                num[event[2]] = 0
            num[event[2]] += 1
        if (is_tight is None) or (is_tight and event[1]) or (not is_tight and not event[1]):
            if not event[3] in num:
                num[event[3]] = 0
            num[event[3]] += 1
    return num


def sum_to_independent_real(num, eff):
    return sum_to_independent(num, eff, True)


def sum_to_independent_fake(num, eff):
    return sum_to_independent(num, eff, False)


def sum_to_independent(num, eff, is_real):
    if is_real:
        category_map = eff.categoryToRealEffIndex
        effs = eff.realEffs
    else:
        category_map = eff.categoryToFakeEffIndex
        effs = eff.fakeEffs
    result = {}
    for category in num:
        eff_index = category_map[category]
        if eff_index not in result:
            result[eff_index] = 0
        result[eff_index] += num[category]
    return result


def print_merged_data(data):
    inverse_merging_scheme = get_inverse_merging_scheme()
    for event in data:
        cat0 = inverse_merging_scheme[event[2]]
        cat1 = inverse_merging_scheme[event[3]]
        print('{0} {1} {2} {3}'.format(
            event[0], event[1], cat0, cat1))


def parse_arguments():
    parser = argparse.ArgumentParser(description='Merge categories')
    parser.add_argument('--eff-in', default='effConfigTwoLepOJ.txt',
            help='Input efficiency configuration file. Correlations not \
                    supported')
    parser.add_argument('--eff-out', default='eff_temp.txt',
            help='Temporary output efficiency configuration file')
    parser.add_argument('--merge-mode', default='all',
            help='Specify the merging scheme for the categories. Supported \
                    values are "all" (one category), "em" (two categories, one for \
                    electrons, one for muons.')
    return parser.parse_args()


def main():
    global options
    options = parse_arguments()
    data = read_stream()
    create_new_efficiency_file(data, options.eff_in, options.eff_out)
    print_merged_data(data)


if __name__ == '__main__':
    main()
