#!/usr/bin/env python3.4

"""
This script should create a set of files with the following format:

    meff/GeV [float]  weight [float]  isEventReal [0/1]  process [string]  <T/L and category data for fake estimation>

[This format should be defined in roottoinput.py]

The events should be drawn randomly from all the datasets in groupedBackgroundsToCombine.backgrounds.
Each MC event should be included N times, where N ~ Poisson(weight).

There should then be 100 files, each one produced with a different seed.
"""

import subprocess
from groupedBackgroundsToCombine import backgrounds, meff_bin_boundaries 



input_files = ''
for background in backgrounds:
    file_stems = backgrounds[background]
    for file_stem in file_stems:
        input_files += 'MC/histFitterTree_merge.'+file_stem+'.root '


# 0: seed
# 1: output file
base_command = './roottoinput.py '+input_files+' --nEvents -1 --cuts "nBJets20 >= 1 and nJets40 >= 2" --drawEvents --seed {0} | column -t > {1}'

for i in range(100):
    seed = i
    filename = 'datafiles/pseudoData_{}.txt'.format(i)
    command = base_command.format(seed, filename)
    print ('Starting run {}'.format(i))
    subprocess.call(command, shell=True)

