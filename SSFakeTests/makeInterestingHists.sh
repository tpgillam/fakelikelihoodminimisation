# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $3,$4}' | hist2D rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for ttbar MC" intx inty pdf plots/ttbarEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $3}' | hist xname "Category of lepton 1" title "Lepton 1 category for ttbar MC" intx pdf plots/ttbarLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $4}' | hist xname "Category of lepton 2" title "Lepton 2 category for ttbar MC" intx pdf plots/ttbarLepton2Category.pdf quit

# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $3,$4}' | hist2D rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for ttbar+Z MC" intx inty pdf plots/ttbarZEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $3}' | hist xname "Category of lepton 1" title "Lepton 1 category for ttbar+Z MC" intx pdf plots/ttbarZLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $4}' | hist xname "Category of lepton 2" title "Lepton 2 category for ttbar+Z MC" intx pdf plots/ttbarZLepton2Category.pdf quit

# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $3,$4}' | hist2D rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for Z+jets MC" intx inty pdf plots/zJetsMassiveBEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $3}' | hist xname "Category of lepton 1" title "Lepton 1 category for Z+jets MC" intx pdf plots/zJetsMassiveBLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $4}' | hist xname "Category of lepton 2" title "Lepton 2 category for Z+jets MC" intx pdf plots/zJetsMassiveBLepton2Category.pdf quit



# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $3,$4,$5}' | hist2DWeighted rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for ttbar MC (weighted)" intx inty pdf plots/ttbarEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $3,$5}' | histWeighted xname "Category of lepton 1" title "Lepton 1 category for ttbar MC (weighted)" intx pdf plots/ttbarLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topPowheg.root | gawk '{print $4,$5}' | histWeighted xname "Category of lepton 2" title "Lepton 2 category for ttbar MC (weighted)" intx pdf plots/ttbarLepton2Category.pdf quit

# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $3,$4,$5}' | hist2DWeighted rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for ttbar+Z MC (weighted)" intx inty pdf plots/ttbarZEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $3,$5}' | histWeighted xname "Category of lepton 1" title "Lepton 1 category for ttbar+Z MC (weighted)" intx pdf plots/ttbarZLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.topZ.root | gawk '{print $4,$5}' | histWeighted xname "Category of lepton 2" title "Lepton 2 category for ttbar+Z MC (weighted)" intx pdf plots/ttbarZLepton2Category.pdf quit

# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $3,$4,$5}' | hist2DWeighted rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for Z+jets MC (weighted)" intx inty pdf plots/zJetsMassiveBEventCategory.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $3,$5}' | histWeighted xname "Category of lepton 1" title "Lepton 1 category for Z+jets MC (weighted)" intx pdf plots/zJetsMassiveBLepton1Category.pdf quit
# ./roottoinput.py --nEvents -1 MC/histFitterTree_merge.zJetsMassiveB.root | gawk '{print $4,$5}' | histWeighted xname "Category of lepton 2" title "Lepton 2 category for Z+jets MC (weighted)" intx pdf plots/zJetsMassiveBLepton2Category.pdf quit


for i in `seq 0 1`; do
  cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 > 200 && $1 < 250) {print $7,$8}}' | hist2D rainbow nofillvoid xname "Category of lepton 1" yname "Category of lepton 2" title "Event categories for pseudo data $i, 200<meff<250 GeV" intx inty pdf plots/binning_200_250_pseudoData_${i}_EventCategory.pdf quit colourscale ux 190 uy 190 uz 10;
  cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 > 200 && $1 < 250) {print $7}}' | hist xname "Category of lepton 1" title "Lepton 1 category for pseudo data $i, 200<meff<250 GeV" intx pdf plots/binning_200_250_pseudoData_${i}_Lepton1Category.pdf quit l 70 u 190;
  cat datafiles/pseudoData_${i}.txt | gawk '{if ($1 > 200 && $1 < 250) {print $8}}' | hist xname "Category of lepton 2" title "Lepton 2 category for pseudo data $i, 200<meff<250 GeV" intx pdf plots/binning_200_250_pseudoData_${i}_Lepton2Category.pdf quit l 70 u 190;
done



