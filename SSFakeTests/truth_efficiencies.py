#!/usr/bin/env python3.4
from groupedBackgroundsToCombine import visual_meff_bin_boundaries, meff_bin_boundaries
from mergecategories import BasicEfficiencyFile
import math
import argparse
from twoLepFakeExtracts import generate_bins
import sys


def truth_efficiencies(meff_low, meff_high, allowed_categories=None):
    filename = 'datafiles/leptons_{0}_{1}.txt'.format(meff_low, meff_high)
    rt = 0.0
    rl = 0.0
    ft = 0.0
    fl = 0.0
    var_rt = 0.0
    var_rl = 0.0
    var_ft = 0.0
    var_fl = 0.0
    with open(filename) as f:
        for line in f:
            toks = line.split()
            real = int(toks[0])
            fake = not real
            tight = int(toks[1])
            loose = not tight
            category = int(toks[2])
            if allowed_categories is not None:
                if category not in allowed_categories:
                    continue
            weight = float(toks[3])
            if real and tight:
                rt += weight
                var_rt += weight ** 2
            elif real and loose:
                rl += weight
                var_rl += weight ** 2
            elif fake and tight:
                ft += weight
                var_ft += weight ** 2
            elif fake and loose:
                fl += weight
                var_fl += weight ** 2

    var_rt = math.sqrt(var_rt)
    var_rl = math.sqrt(var_rl)
    var_ft = math.sqrt(var_ft)
    var_fl = math.sqrt(var_fl)

    if rt > 0:
        real_eff = rt / (rt + rl)
        u_real_eff = math.sqrt(  ( rl / (rt + rl)**2 )**2 * var_rt   +   ( rt / (rt + rl)**2 )**2 * var_rl  )
    else:
        real_eff = u_real_eff = 0.1
    if ft > 0:
        fake_eff = ft / (ft + fl)
        u_fake_eff = math.sqrt(  ( fl / (ft + fl)**2 )**2 * var_ft   +   ( ft / (ft + fl)**2 )**2 * var_fl  )
    else:
        fake_eff = u_fake_eff = 0.1
    return ((real_eff, u_real_eff), (fake_eff, u_fake_eff))


def generate_truth_efficiency_file(meff_low, meff_high, filename, merge_mode, display_only=False):
    if merge_mode == 'all':
        cats_collection = [ list(range(0, 183+1)) ]
    elif merge_mode == 'em':
        electron_cats = list(range(0, 151+1))
        muon_cats = list(range(152, 183+1))
        cats_collection = [electron_cats, muon_cats]
    elif merge_mode == 'em40':
        # Split electrons and muons, and additionally separate at 40 GeV
        # That is, produce a total of 4 categories
        cats_collection = []
        for j in range(4):
            cats_collection.append([])
        for i,bin in enumerate(generate_bins()):
            is_e = (bin[1] == 'e')
            pt_under_40 = (bin[3][1] <= 40.0)
            if is_e and pt_under_40:
                j = 0
            elif not is_e and pt_under_40:
                j = 1
            elif is_e and not pt_under_40:
                j = 2
            elif not is_e and not pt_under_40:
                j = 3
            cats_collection[j].append(i)
    elif merge_mode == 'em3050':
        # Split electrons and muons, and additionally separate at 30 GeV
        # and 50 GeV. That is, produce a total of 6 categories
        cats_collection = []
        for j in range(6):
            cats_collection.append([])
        for i,bin in enumerate(generate_bins()):
            is_e = (bin[1] == 'e')
            pt_under_30 = (bin[3][1] <= 30.0)
            pt_under_50 = (bin[3][1] <= 50.0)
            if is_e and pt_under_30:
                j = 0
            elif not is_e and pt_under_30:
                j = 1
            elif is_e and pt_under_50:
                j = 2
            elif not is_e and pt_under_50:
                j = 3
            elif is_e and not pt_under_50:
                j = 4
            elif not is_e and not pt_under_50:
                j = 5
            cats_collection[j].append(i)
    else:
        raise RuntimeError('Unknown mode: {}'.format(merge_mode))

    eff_file = BasicEfficiencyFile()
    for i,cats in enumerate(cats_collection):
        real_eff, fake_eff = truth_efficiencies(meff_low, meff_high, cats)
        eff_file.categoryToRealEffIndex.append(i)
        eff_file.categoryToFakeEffIndex.append(i)
        eff_file.realEffs.append(real_eff)
        eff_file.fakeEffs.append(fake_eff)
    if display_only:
        eff_file.write_effs_to_handle(sys.stdout)
    else:
        eff_file.write_to_file(filename)


def test():
    print(truth_efficiencies(150, 200))
    generate_truth_efficiency_file(150, 200, 'eff_temp.txt', 'em40')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('low', help='Lower meff bound')
    parser.add_argument('high', help='Upper meff bound')
    parser.add_argument('--display', action='store_true', default=False,
            help='Only display efficiencies, rather than writing to file')
    parser.add_argument('--eff-out', default='eff_temp.txt',
            help='Temporary output efficiency configuration file')
    parser.add_argument('--merge-mode', default='all',
            help='Specify the merging scheme for the categories. Supported \
                    values are "all" (one category), "em" (two categories, one for \
                    electrons, one for muons.')
    options = parser.parse_args()
    generate_truth_efficiency_file(options.low, options.high, options.eff_out, options.merge_mode, options.display)


if __name__ == '__main__':
    main()
    # test()
