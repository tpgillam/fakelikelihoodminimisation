#!/usr/bin/env python
from math import *

def signal_handler(signal, frame):
  import os
  os._exit(1)

import signal
signal.signal(signal.SIGINT, signal_handler)

def truthSummary(nEvents, seed, generatorConfig):
  command = './madtom.py {0} {1} yes {2} truthSummary'.format(generatorConfig, nEvents, seed)
  return runCommand(command)

def likelihoodRun(nEvents, seed, generatorConfig):
  command = './madtom.py {0} {1} yes {2} compact | ./llhMinimising'.format(generatorConfig, nEvents, seed)
  return runCommand(command)

def runCommand(cmd):
  import subprocess
  return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).communicate()[0]


class GeneratorRun(object):
  def __init__(self, nEvents, seed, generatorConfig):
    self._nEvents = nEvents
    self._seed = seed
    self._generatorConfig = generatorConfig
    self._determineTruthComponents()
    self._determineCLsComponents()

  def _determineTruthComponents(self):
    summary = truthSummary(self._nEvents, self._seed, self._generatorConfig)
    self.nFT = 0
    self.nRT = 0
    doingUnderlyingRatesTight = False
    for line in summary.splitlines():
      if 'Underlying all-tight rate' in line:
        doingUnderlyingRatesTight = True
        continue 
      if 'Underlying all-tight all-real rate' in line:
        break
      if 'apparently' in line:
        break
      if not doingUnderlyingRatesTight:
        if '  redBkg' in line:
          self.nFT = int(line.split()[-1])
        elif '  irredBkg' in line:
          self.nRT = int(line.split()[-1])
        elif '  signal' in line:
          self.nSignalRT = int(line.split()[-1])
      else:
        if '  redBkg' in line:
          self.rateFTTruth = float(line.split()[-1])
        elif '  irredBkg' in line:
          self.rateRTTruth = float(line.split()[-1])
        elif '  signal' in line:
          self.rateSignalRTTruth = float(line.split()[-1])

  def _determineCLsComponents(self):
    summary = likelihoodRun(self._nEvents, self._seed, self._generatorConfig)
    doingLikelihood = False

    limitType = None

    for line in summary.splitlines():
      if 'Go likelihood method!' in line:
        doingLikelihood = True
        continue
      if line.startswith('95% CLs UL (toys)'):     limitType = 'CLsToys'
      elif line.startswith('95% CLsb UL (toys)'):  limitType = 'CLsbToys'
      elif line.startswith('95% CLs '):         limitType = 'CLs'
      elif line.startswith('95% CLsb '):        limitType = 'CLsb'
      elif line.startswith('sigmaDisc'):
        if doingLikelihood:
          self.sigmaDiscLikelihood = float(line.split()[-1])
        else:
          self.sigmaDiscMM = float(line.split()[-1])
      elif not doingLikelihood and line.startswith('Total MM weight'):
        self.rateFTMM = float(line.split()[-3])
        self.rateFTMMUnc = float(line.split()[-1])
      elif doingLikelihood and line.startswith('Fake tight rate'):
        self.rateFTLikelihood = float(line.split()[-3])
        self.rateFTLikelihoodUnc = float(line.split()[-1])

      if limitType is not None:
        likelihoodOrMMPostfix = 'MM'
        if doingLikelihood:
          likelihoodOrMMPostfix = 'Likelihood'
        fullName = 'ul95{}{}'.format(limitType, likelihoodOrMMPostfix)
        line = line.strip()
        if line.startswith('xVals'):
          setattr(self, fullName+'_xVals', self._listFromLine(line))
        elif line.startswith('yVals'):
          setattr(self, fullName+'_yVals', self._listFromLine(line))
          limitType = None
        else:
          setattr(self, fullName, float(line.split()[-1]))

  def _listFromLine(self, line):
    return [float(x.strip()) for x in line.split(':')[-1].strip().strip('{}').split(',')]

  def __repr__(self):
    result = ''
    for item in self.__dict__:
      result += '{0}: {1}; '.format(item, self.__dict__[item])
    return result


def _createRun(arg):
  numEvents = arg[0]
  seed = arg[1]
  generatorConfig = arg[2]
  newObj = GeneratorRun(numEvents, seed, generatorConfig)
  return newObj
  # return {1: [2,3], 2: 'something'}

def isCambridge():
  import subprocess
  return 'cam.ac.uk' in subprocess.Popen('hostname -d', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()[0]

def createPickleJar(numEvents, name, generatorConfig, numProcesses=8, rangeStart=100, rangeStop=1000, seeds=None):
  if seeds is None:
    seeds = range(rangeStart, rangeStop)
  args = zip([numEvents]*len(seeds), seeds, [generatorConfig]*len(seeds))

  if isCambridge():
    import os.path
    current_path = os.path.dirname(os.path.realpath(__file__))
    setup = 'cd /usera/gillam/fakeLikelihoodStuff/; . setup.sh; cd {0};'.format(current_path)
    machineconfig = [ 
        # ('cl001', 8, setup),
        # ('cl002', 8, setup),
        # ('cl003', 8, setup),
        # ('cl004', 8, setup),
        # ('cl005', 8, setup),
        ('cl011', 24, setup),
        ('cl012', 24, setup),
        ('cl013', 24, setup),
        ('cl014', 24, setup),
        # ('pckg', 4, setup), 
        # ('pcil', 4, setup), 
        # ('pckh', 4, setup), 
        # ('pchy', 4, setup), 
        ]   

    import sys 
    sys.path.append('/usera/gillam/PythonModules/')
    from gillam.triffid import runserver
    runs = runserver(_createRun, args, machineconfig)
  else:
    from multiprocessing import Pool
    p = Pool(processes=numProcesses)
    asyncResult = p.imap_unordered(_createRun, args)
    runs = []
    totalNumTasks = len(seeds)
    numCompleted = 0
    import sys
    sys.stdout.write('\rCompleted 0/{0}'.format(totalNumTasks))
    sys.stdout.flush()
    for thing in asyncResult:
      runs.append(thing)
      numCompleted += 1
      sys.stdout.write('\rCompleted {0}/{1}'.format(numCompleted, totalNumTasks))
      sys.stdout.flush()
    print
    writePickleJar(name, runs)


def writePickleJar(name, runs):
  outputFile = open(name+'.pickle', 'w')
  import pickle
  pickle.dump(runs, outputFile, protocol=2)
  outputFile.close()
  print 'Written pickle file!'


def loadPickleJar(filename='runsTest.pickle'):
  import pickle
  inputFile = open(filename)
  runs = pickle.load(inputFile)
  inputFile.close()
  return runs
 
def makePrettyPlots(outputDir, runs):
  import os
  if not os.path.isdir(outputDir):
    os.makedirs(outputDir)
  makePrettyPlot(runs, outputDir, 'discoveryComp', 'sigmaDiscMM', 'sigmaDiscLikelihood')

  makePrettyPlot(runs, outputDir, 'upperLimitComp', 'ul95CLsMM', 'ul95CLsLikelihood')
  makePrettyPlot(runs, outputDir, 'upperLimitToysComp', 'ul95CLsToysMM', 'ul95CLsToysLikelihood')

  makePrettyPlot(runs, outputDir, 'FTRateComp', 'rateFTMM', 'rateFTLikelihood')
  makePrettyPlot(runs, outputDir, 'FTRateMMvsTruth', 'nFT', 'rateFTMM')
  makePrettyPlot(runs, outputDir, 'FTRateLikelihoodvsTruth', 'nFT', 'rateFTLikelihood')
  makePrettyPlot(runs, outputDir, 'nRTvsnFT', 'nFT', 'nRT')

  makePrettyPlot(runs, outputDir, 'upperLimitMMvsnFT', 'nFT', 'ul95CLsMM')
  makePrettyPlot(runs, outputDir, 'upperLimitLikelihoodvsnFT', 'nFT', 'ul95CLsLikelihood')
  makePrettyPlot(runs, outputDir, 'upperLimitMMvsnRT', 'nRT', 'ul95CLsMM')
  makePrettyPlot(runs, outputDir, 'upperLimitLikelihoodvsnRT', 'nRT', 'ul95CLsLikelihood')
  makePrettyPlot(runs, outputDir, 'upperLimitToysMMvsnFT', 'nFT', 'ul95CLsToysMM')
  makePrettyPlot(runs, outputDir, 'upperLimitToysLikelihoodvsnFT', 'nFT', 'ul95CLsToysLikelihood')
  makePrettyPlot(runs, outputDir, 'upperLimitToysMMvsnRT', 'nRT', 'ul95CLsToysMM')
  makePrettyPlot(runs, outputDir, 'upperLimitToysLikelihoodvsnRT', 'nRT', 'ul95CLsToysLikelihood')

  makePrettyPlot(runs, outputDir, 'discoveriesvsnFT', 'nFT', ['sigmaDiscMM', 'sigmaDiscLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'discoveriesvsnRT', 'nRT', ['sigmaDiscMM', 'sigmaDiscLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'discoveriesvsnT', 'nRT+nFT', ['sigmaDiscMM', 'sigmaDiscLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'upperLimitsvsnFT', 'nFT', ['ul95CLsMM', 'ul95CLsLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'upperLimitsvsnRT', 'nRT', ['ul95CLsMM', 'ul95CLsLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'upperLimitsToysvsnFT', 'nFT', ['ul95CLsToysMM', 'ul95CLsToysLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'upperLimitsToysvsnRT', 'nRT', ['ul95CLsToysMM', 'ul95CLsToysLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'FTRates', '1', ['rateFTMM', 'rateFTLikelihood'], displacementFactor=0.5, padding=0.3)
  makePrettyPlot(runs, outputDir, 'FTRatesvsnFT', 'nFT', ['rateFTMM', 'rateFTLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'FTRatesvsnRT', 'nRT', ['rateFTMM', 'rateFTLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'upperLimitsvsnT', 'nRT+nFT', ['ul95CLsMM', 'ul95CLsLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'upperLimitsToysvsnT', 'nRT+nFT', ['ul95CLsToysMM', 'ul95CLsToysLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'upperLimitsCLsbvsnT', 'nRT+nFT', ['ul95CLsbMM', 'ul95CLsbLikelihood'], displacementFactor=0.5)
  makePrettyPlot(runs, outputDir, 'upperLimitsToysCLsbvsnT', 'nRT+nFT', ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'FTRatesvsnT', 'nRT+nFT', ['rateFTMM', 'rateFTLikelihood'], displacementFactor=0.5)

  makePrettyPlot(runs, outputDir, 'FTRatevsUpperLimitToyCLsbMM', 'rateFTMM', 'ul95CLsbToysMM', addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRatevsUpperLimitToyCLsbLikelihood', 'rateFTLikelihood', 'ul95CLsbToysLikelihood', addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernTvsUpperLimitToyCLsbMM', 'rateFTMM/(nRT+nFT)','ul95CLsbToysMM',  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernTvsUpperLimitToyCLsbLikelihood', 'rateFTLikelihood/(nRT+nFT)', 'ul95CLsbToysLikelihood', addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernFTvsUpperLimitToyCLsbMM', 'rateFTMM/nFT','ul95CLsbToysMM',  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernFTvsUpperLimitToyCLsbLikelihood', 'rateFTLikelihood/nFT', 'ul95CLsbToysLikelihood', addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateFTTruthvsUpperLimitToyCLsbMM', 'rateFTMM/rateFTTruth','ul95CLsbToysMM',  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateFTTruthvsUpperLimitToyCLsbLikelihood', 'rateFTLikelihood/rateFTTruth', 'ul95CLsbToysLikelihood', addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateTTruthvsUpperLimitToyCLsbMM', 'rateFTMM/(rateFTTruth+rateSignalRTTruth)','ul95CLsbToysMM',  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateTTruthvsUpperLimitToyCLsbLikelihood', 'rateFTLikelihood/(rateFTTruth+rateSignalRTTruth)', 'ul95CLsbToysLikelihood', addCoverageInfo=True)

  makePrettyPlot(runs, outputDir, 'FTRatevsUpperLimitToyCLsb', ['rateFTMM', 'rateFTLikelihood'], ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'], addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernTvsUpperLimitToyCLsb', ['rateFTMM/(nRT+nFT)', 'rateFTLikelihood/(nRT+nFT)'], ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'],  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOvernFTvsUpperLimitToyCLsb', ['rateFTMM/nFT', 'rateFTLikelihood/nFT'], ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'],  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateFTTruthvsUpperLimitToyCLsb', ['rateFTMM/rateFTTruth', 'rateFTLikelihood/rateFTTruth'], ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'],  addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'FTRateOverRateTTruthvsUpperLimitToyCLsb', ['rateFTMM/(rateFTTruth+rateSignalRTTruth)', 'rateFTLikelihood/(rateFTTruth+rateSignalRTTruth)'], ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'],  addCoverageInfo=True)

  makePrettyPlot(runs, outputDir, 'discoveries', '1', ['sigmaDiscMM', 'sigmaDiscLikelihood'], displacementFactor=0.1, padding=0.3)

  makePrettyPlot(runs, outputDir, 'upperLimitsCLs', '1', ['ul95CLsMM', 'ul95CLsLikelihood'], displacementFactor=0.1, padding=0.3, addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'upperLimitsCLsb', '1', ['ul95CLsbMM', 'ul95CLsbLikelihood'], displacementFactor=0.1, padding=0.3, addCoverageInfo=True)

  makePrettyPlot(runs, outputDir, 'upperLimitsToysCLs', '1', ['ul95CLsToysMM', 'ul95CLsToysLikelihood'], displacementFactor=0.1, padding=0.3, addCoverageInfo=True)
  makePrettyPlot(runs, outputDir, 'upperLimitsToysCLsb', '1', ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood'], displacementFactor=0.1, padding=0.3, addCoverageInfo=True)


  # makePrettyPlot(runs, outputDir, 'upperLimitsLog', '1', ['log(ul95CLsMM)', 'log(ul95CLsLikelihood)'], displacementFactor=0.1, padding=0.3)

  makePrettyPlot(runs, outputDir, 'upperLimitRatiovsnT', 'nRT+nFT', 'ul95CLsMM/ul95CLsLikelihood')
  makePrettyPlot(runs, outputDir, 'upperLimitToysRatiovsnT', 'nRT+nFT', 'ul95CLsToysMM/ul95CLsToysLikelihood')

  makePrettyPlot(runs, outputDir, 'FTRateRatiovsnT', 'nRT+nFT', 'rateFTMM/rateFTLikelihood')
  # makePrettyPlot(runs, outputDir, 'discoveryRatiovsnT', 'nRT+nFT', 'log(sigmaDiscMM/sigmaDiscLikelihood)')
  

def fail_silently(f):
  def f_fail_silently(*args, **kwargs):
    try:
      return f(*args, **kwargs)
    except:
      print 'Calling {} failed'.format(f)
  return f_fail_silently


@fail_silently
def makePrettyPlot(runs, outputDir, title, attrX, attrY, displacementFactor=0.0, padding=0.0, addCoverageInfo=False):
  import ROOT
  ROOT.gROOT.SetBatch(1)
  multipleX = hasattr(attrX, '__iter__')
  multipleY = hasattr(attrY, '__iter__')
  if multipleX:
    xs = []
    for thisAttrX in attrX:
      x = []
      for run in runs:
        if run is None: continue
        if not hasattr(run, 'ul95CLsLikelihood'): continue
        if not hasattr(run, 'ul95CLsbLikelihood'): continue
        import math
        x.append(eval(thisAttrX, globals(), run.__dict__))
      xs.append(x)
  else:
    x = []
    for run in runs:
      if run is None: continue
      if not hasattr(run, 'ul95CLsLikelihood'): continue
      import math
      x.append(eval(attrX, globals(), run.__dict__))

  if multipleY:
    ys = []
    for thisAttrY in attrY:
      y = []
      for run in runs:
        if run is None: continue
        if not hasattr(run, 'ul95CLsLikelihood'): continue
        if not hasattr(run, 'ul95CLsbLikelihood'): continue
        import math
        y.append(eval(thisAttrY, globals(), run.__dict__))
      ys.append(y)
  else:
    y = []
    for run in runs:
      if run is None: continue
      if not hasattr(run, 'ul95CLsLikelihood'): continue
      if not hasattr(run, 'ul95CLsbLikelihood'): continue
      import math
      y.append(eval(attrY, globals(), run.__dict__))

  # if not multipleY:
  #   for moo in zip(x, y):
  #     print moo

  from array import array
  if multipleY: 
    if multipleX:
      nX = len(attrX)
      nY = len(attrY)
      assert(nX == nY)
      import random
      xarrs = [array('d', x) for x in xs]
      yarrs = [array('d', y) for y in ys]
    else:
      n = len(attrY)
      import random
      xarrs = [array('d', [moo + displacementFactor*(i/(n-1) - 0.5) + displacementFactor * 0.8 * random.random() for moo in x]) for i in range(n)]
      yarrs = [array('d', y) for y in ys]
  else:
    xarr = array('d', x)
    yarr = array('d', y)

  c = ROOT.TCanvas()
  graphs = []
  if multipleY:
    for xarr,yarr in zip(xarrs,yarrs):
      graph = ROOT.TGraph(len(xarr), xarr, yarr)
      graphs.append(graph)
  else:
    graph = ROOT.TGraph(len(xarr), xarr, yarr)
    graphs.append(graph)

  minx = graphs[0].GetXaxis().GetXmin()
  maxx = graphs[0].GetXaxis().GetXmax()
  for graph in graphs:
    minx = min(minx, graph.GetXaxis().GetXmin())
    maxx = max(maxx, graph.GetXaxis().GetXmax())
  minx = minx - padding
  maxx = maxx + padding

  for graph in graphs:
    graph.SetMarkerStyle(2)
    graph.SetMarkerSize(1)
    graph.SetTitle(title)
    if multipleX:
      graph.GetXaxis().SetTitle(', '.join(attrX))
    else:
      graph.GetXaxis().SetTitle(attrX)
    graph.GetXaxis().SetLimits(minx, maxx)
    if multipleY:
      graph.GetYaxis().SetTitle(', '.join(attrY))
    else:
      graph.GetYaxis().SetTitle(attrY)
  
  firstTime = True
  for graph in graphs:
    if firstTime:
      graph.Draw('ap')
      firstTime = False
    else:
      graph.SetMarkerColor(ROOT.kRed)
      graph.Draw('psame')

  if addCoverageInfo:
    coverageMM, coverageLikelihood = getCoverageInfo(runs, attrY)
    if coverageMM is not None:
      yForLine = findTruthRate(runs) 
      line = ROOT.TLine(minx, yForLine, maxx, yForLine)
      line.SetLineColor(ROOT.kBlue)
      line.Draw()
      text = ROOT.TText()
      # ARGGHGHH centre align
      text.SetTextAlign(21)
      text.SetTextColor(ROOT.kBlack)
      k = 0.2
      text.DrawText(k*(maxx-minx)+minx, 1.2*yForLine, '{:.2f}%'.format(100*coverageMM))
      text.SetTextColor(ROOT.kRed)
      k = 0.8
      text.DrawText(k*(maxx-minx)+minx, 1.2*yForLine, '{:.2f}%'.format(100*coverageLikelihood))
  c.SaveAs('{0}/{1}.pdf'.format(outputDir, title))

def getCoverageInfo(runs, attrY):
  doCLsb = False
  doToys = False
  if hasattr(attrY, '__iter__'):
    attributeText = attrY[0]
  else:
    attributeText = attrY
  if 'CLsb' in attributeText:
    doCLsb = True
  if 'Toys' in attributeText:
    doToys = True

  totalMM = 0 
  totalCoveringMM = 0
  totalLikelihood = 0 
  totalCoveringLikelihood = 0
  # numNansMM = 0
  # numNansLikelihood = 0
  truthRate = findTruthRate(runs) 
  for run in runs:
    if run is None: continue
    if not hasattr(run, 'ul95CLsLikelihood'): continue
    if not hasattr(run, 'ul95CLsbLikelihood'): continue
    if not hasattr(run, 'rateRTTruth'): return None, None
    from math import isnan
    if isnan(run.ul95CLsMM):
      print 'MOOOO', run.ul95CLsLikelihood
      continue
    if isnan(run.ul95CLsLikelihood):
      print 'BAAAA'
      continue
    totalMM += 1
    if not doToys:
      if not doCLsb:
        if run.ul95CLsMM > truthRate:
          totalCoveringMM +=1
        totalLikelihood += 1
        if run.ul95CLsLikelihood > truthRate:
          totalCoveringLikelihood +=1
      else:
        if run.ul95CLsbMM > truthRate:
          totalCoveringMM +=1
        totalLikelihood += 1
        if run.ul95CLsbLikelihood > truthRate:
          totalCoveringLikelihood +=1
    else:
      if not doCLsb:
        if run.ul95CLsToysMM > truthRate:
          totalCoveringMM +=1
        totalLikelihood += 1
        if run.ul95CLsToysLikelihood > truthRate:
          totalCoveringLikelihood +=1
      else:
        if run.ul95CLsbToysMM > truthRate:
          totalCoveringMM +=1
        totalLikelihood += 1
        if run.ul95CLsbToysLikelihood > truthRate:
          totalCoveringLikelihood +=1
  # print truthRate, '  ', doCLsb, '  ', doToys, '  :   ', totalCoveringMM, totalMM, '   ', totalCoveringLikelihood, totalLikelihood
  coverageMM = float(totalCoveringMM) / float(totalMM)
  coverageLikelihood = float(totalCoveringLikelihood) / float(totalLikelihood)
  return (coverageMM, coverageLikelihood) 
  # return (numNansMM, numNansLikelihood)

def findTruthRate(runs):
  if hasattr(runs[0], 'rateSignalRTTruth') and runs[0].rateSignalRTTruth > 0:
    rate = runs[0].rateSignalRTTruth
  else:
    rate = runs[0].rateRTTruth
  return rate

def makeAllPlots():
  names = []
  import os
  for filename in os.listdir('.'):
    if not filename.endswith('.pickle'): continue
    # if not 'manGrad' in filename: continue
    # if not 'RealBkg' in filename: continue
    # if not 'semiLikelihoodWrongEfficiencies' in filename: continue
    names.append(filename.replace('.pickle', ''))
  for name in names:
    runs = loadPickleJar(name+'.pickle')
    makePrettyPlots('plots/'+name, runs)


if __name__ == '__main__':
  # createPickleJar(100,  'runs_20140525_mostlyFake_ev100_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(200,  'runs_20140525_mostlyFake_ev200_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(400,  'runs_20140525_mostlyFake_ev400_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(800,  'runs_20140525_mostlyFake_ev800_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(1600, 'runs_20140525_mostlyFake_ev1600_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(3200, 'runs_20140525_mostlyFake_ev3200_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')
  # createPickleJar(6400, 'runs_20140525_mostlyFake_ev6400_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt')

  # createPickleJar(100,  'runs_20140604_eightCategories_ev100_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(200,  'runs_20140604_eightCategories_ev200_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(400,  'runs_20140604_eightCategories_ev400_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(800,  'runs_20140604_eightCategories_ev800_seeds100-999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(1600, 'runs_20140604_eightCategories_ev1600_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(3200, 'runs_20140604_eightCategories_ev3200_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(6400, 'runs_20140604_eightCategories_ev6400_seeds100-999_fixQMuDef_manGrad', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')

  # createPickleJar(100,  'runs_20140605_eightCategories_ev100_seeds100-999_uninformativeMM',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(200,  'runs_20140605_eightCategories_ev200_seeds100-999_uninformativeMM',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(400,  'runs_20140605_eightCategories_ev400_seeds100-999_uninformativeMM',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(800,  'runs_20140605_eightCategories_ev800_seeds100-999_uninformativeMM',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(1600, 'runs_20140605_eightCategories_ev1600_seeds100-999_uninformativeMM', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(3200, 'runs_20140605_eightCategories_ev3200_seeds100-999_uninformativeMM', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(6400, 'runs_20140605_eightCategories_ev6400_seeds100-999_uninformativeMM', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')

  # createPickleJar(100,  'runs_20140605_2_eightCategories_ev100_seeds100-999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(200,  'runs_20140605_2_eightCategories_ev200_seeds100-999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(400,  'runs_20140605_2_eightCategories_ev400_seeds100-999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(800,  'runs_20140605_2_eightCategories_ev800_seeds100-999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(1600, 'runs_20140605_2_eightCategories_ev1600_seeds100-999_semiLikelihood', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(3200, 'runs_20140605_2_eightCategories_ev3200_seeds100-999_semiLikelihood', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(6400, 'runs_20140605_2_eightCategories_ev6400_seeds100-999_semiLikelihood', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')

  # createPickleJar(100,  'runs_20140605_3_eightCategories_ev100_seeds100-999_semiLikelihoodNoTight',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(200,  'runs_20140605_3_eightCategories_ev200_seeds100-999_semiLikelihoodNoTight',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(400,  'runs_20140605_3_eightCategories_ev400_seeds100-999_semiLikelihoodNoTight',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(800,  'runs_20140605_3_eightCategories_ev800_seeds100-999_semiLikelihoodNoTight',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(1600, 'runs_20140605_3_eightCategories_ev1600_seeds100-999_semiLikelihoodNoTight', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(3200, 'runs_20140605_3_eightCategories_ev3200_seeds100-999_semiLikelihoodNoTight', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(6400, 'runs_20140605_3_eightCategories_ev6400_seeds100-999_semiLikelihoodNoTight', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')

  # createPickleJar(100,  'runs_20140609_eightCategories_ev100_seeds100-999_semiLikelihoodWrongEfficiencies',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(200,  'runs_20140609_eightCategories_ev200_seeds100-999_semiLikelihoodWrongEfficiencies',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(400,  'runs_20140609_eightCategories_ev400_seeds100-999_semiLikelihoodWrongEfficiencies',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(800,  'runs_20140609_eightCategories_ev800_seeds100-999_semiLikelihoodWrongEfficiencies',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(1600, 'runs_20140609_eightCategories_ev1600_seeds100-999_semiLikelihoodWrongEfficiencies', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(3200, 'runs_20140609_eightCategories_ev3200_seeds100-999_semiLikelihoodWrongEfficiencies', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')
  # createPickleJar(6400, 'runs_20140609_eightCategories_ev6400_seeds100-999_semiLikelihoodWrongEfficiencies', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt')

  # createPickleJar(100,  'runs_20140704_2_eightCategories_ev100_seeds1000-19999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake_eightCategories.txt', rangeStart=1000, rangeStop=20000)
  # createPickleJar(100,  'runs_20140704_mostlyFake_ev100_seeds1000-19999_fixQMuDef_manGrad',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt', rangeStart=1000, rangeStop=20000)
  # createPickleJar(100,  'runs_20140714_mostlyFake_ev100_seeds1000-19999_semiLikelihood',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt', rangeStart=1000, rangeStop=20000)
  # createPickleJar(100,  'runs_20140715_mostlyFake_ev100_seeds1000-19999_methodBOnly',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt', rangeStart=1000, rangeStop=20000)
  createPickleJar(100,  'runs_20140715_mostlyFake_ev100_seeds1000-19999_methodBOnly_remainder',  'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt', \
      seeds=[5386, 5600, 5628, 5630, 5686, 5687, 5688, 5689])

  # createPickleJar(400, 'test', 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt',rangeStart=0,rangeStop=8)
  # print _createRun([400, 0, 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt'])

  # createPickleJar(1600, 'runs_withRealBkg_ev1600_seeds100-999_20140327_manGrad', 'generatorConfig_20140320_withRealBkg.txt')
  # createPickleJar(1600, 'runs_withRealBkg_ev1600_seeds100-999_20140331_manGrad', 'generatorConfig_20140320_withRealBkg.txt')
  # createPickleJar(1600, 'runs_mostlyFake_ev1600_seeds0-9_20140325_fixQMuDef_asimov3', 'generatorConfig_20140320_withRealBkg.txt',rangeStart=0,rangeStop=10)

  # generatorConfig = 'twoLeptonReallyFakeFakeGeneratorConfig.txt'
  # generatorConfig = 'twoLeptonReallyFakeFakeGeneratorConfig_mostlyFake.txt'
  # generatorConfig = 'twoLeptonOnlyFakeFakeGeneratorConfig_mostlyFake.txt'
  # generatorConfig = 'generatorConfig_20140320_withRealBkg.txt'

  # makeAllPlots()

