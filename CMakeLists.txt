cmake_minimum_required(VERSION 2.6.0 FATAL_ERROR)
project(llhMinimising)

list( APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR} )

# set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_BUILD_TYPE Release)

find_package(ROOT REQUIRED)


set( sourceFiles
src/BackgroundComponent.cxx
src/BasicFakeNLL.cxx
src/CLsCalculator.cxx
src/CLsCalculatorFakeNLL.cxx
# FIXME either delete or include ...
# src/CLsCalculatorMatrixMethod.cxx
src/Components.cxx
src/MatrixMethod.cxx
src/MatrixMethodApplicator.cxx
src/MMSampler.cxx
src/MMBaseSampler.cxx
src/MMImportanceSampler.cxx
src/MiniMinimum.cxx
src/Minimiser.cxx
src/utils.cxx
src/ComplexEfficiencyInfo.cxx
)

set( llhMinimisingSource
src/llhMinimising.cxx
)

set( mleRunnerSource
src/mleRunner.cxx
)

set( ssRunnerSource
src/ssRunner.cxx
)

set( samplerSource
src/MatrixMethod.cxx
src/MatrixMethodApplicator.cxx
src/MMSampler.cxx
src/MMImportanceSampler.cxx
src/MMBaseSampler.cxx
src/MMExactPosteriorSampler.cxx
src/MMGibbsPosteriorSampler.cxx
src/utils.cxx
src/ComplexEfficiencyInfo.cxx
src/sampler.cxx
src/TLSpaceSolver.cxx
)

set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Werror" )
site_name(siteName)
if(${siteName} STREQUAL tigger)
  set( CMAKE_CXX_COMPILER "clang++" )
  include_directories("/opt/local/include")
  link_directories("/opt/local/lib")
else()
  set (CMAKE_CXX_COMPILER "g++" )
  set (BOOST_ROOT "/usera/gillam/usr/local/" )
  set (Boost_INCLUDE_DIRS "/usera/gillam/usr/local/include" )
  set (Boost_LIBRARY_DIRS "/usera/gillam/usr/local/lib" )
  include_directories("/usera/gillam/usr/local/include")
  link_directories("/usera/gillam/usr/local/lib")
endif()


find_package( Boost 1.40 COMPONENTS filesystem system REQUIRED )
message(STATUS "Using Boost include dir ${Boost_INCLUDE_DIRS}")
message(STATUS "Using Boost library dir ${Boost_LIBRARY_DIRS}")

include_directories( ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIR} )
link_directories( ${Boost_LIBRARY_DIRS} ${ROOT_LIBRARY_DIR} )


add_executable( llhMinimising ${sourceFiles} ${llhMinimisingSource} )
target_link_libraries( llhMinimising ${Boost_LIBRARIES} ${ROOT_LIBRARIES} Minuit2 )

add_executable( mleRunner ${sourceFiles} ${mleRunnerSource} )
target_link_libraries( mleRunner ${Boost_LIBRARIES} ${ROOT_LIBRARIES} Minuit2 )

add_executable( ssRunner ${sourceFiles} ${ssRunnerSource} )
target_link_libraries( ssRunner ${Boost_LIBRARIES} ${ROOT_LIBRARIES} Minuit2 )

add_executable( sampler ${samplerSource} )
set_target_properties( sampler PROPERTIES COMPILE_DEFINITIONS "NO_MINIMISING" )
target_link_libraries( sampler ${Boost_LIBRARIES} glpk )

add_executable( silly src/silly.cxx )
target_link_libraries( silly ${Boost_LIBRARIES} glpk )
