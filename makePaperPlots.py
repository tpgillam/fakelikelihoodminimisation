#!/usr/bin/env python # vim: tabstop=8 shiftwidth=4 softtabstop=4 expandtab

import ROOT
import numpy as np
import matplotlib.colors as mpcolors
import matplotlib.pyplot as plt
import matplotlib as mpl
import os.path
import math

# Global config
output_dir = 'plots/paperPlots/'
# run_name_simple = 'runs_20140525_mostlyFake_ev100_seeds100-999_fixQMuDef_manGrad'
# run_name_harder = 'runs_20140605_2_eightCategories_ev100_seeds100-999_semiLikelihood'

run_name_simple = 'runs_20140715_mostlyFake_ev100_seeds1000-19999_methodsAB'
run_name_simple_methodC = 'runs_20140714_mostlyFake_ev100_seeds1000-19999_semiLikelihood'
run_name_harder = 'runs_20140704_2_eightCategories_ev100_seeds1000-19999_semiLikelihood'



def main():
    # compare_overall_CLsb_boxplot_for_sample()

    # # NICE
    # # compare_nTsplit_CLsb_boxplot_for_sample(run_name_simple, use_method_C=False, output_postfix='simple')
    # # compare_nTsplit_CLsb_boxplot_for_sample(run_name_harder, use_method_C=True, output_postfix='harder')
    # # TODO temporary commenting out!
    # compare_nTsplit_fakeRate_boxplot_for_sample(run_name_simple, use_method_C=False, 
    #         output_postfix='simple')#, ylim_override=(-1.5, 6))
    # compare_nTsplit_fakeRate_boxplot_for_sample(run_name_harder, use_method_C=True, 
    #         output_postfix='harder')#, ylim_override=(-3, 9))

    # # compare_overall_CLsb_CDF_for_sample()
    # # compare_nTsplit_CLsb_CDF_for_sample()

    # # compare_overall_CLsb_histogram_for_sample(run_name_simple, use_method_C=False, output_postfix='simple')
    # # compare_overall_CLsb_histogram_for_sample(run_name_harder, use_method_C=True, output_postfix='harder')

    # # # YES - we use all of these!
    # compare_nTsplit_CLsb_histogram_for_sample(run_name_simple, use_method_C=False, 
    #         output_postfix='simple', width_scaling=2.7, num_bins=50, min_bin_width=0.1)
    # compare_nTsplit_CLsb_histogram_for_sample(run_name_simple_methodC, use_method_C=True, 
    #         output_postfix='simpleMethodC', width_scaling=2.7, num_bins=50, min_bin_width=0.1)
    # compare_nTsplit_CLsb_histogram_for_sample(run_name_harder, use_method_C=True, 
    #         output_postfix='harder', width_scaling=4.0, num_bins=50, min_bin_width=0.1)
    # compare_nTsplit_CLsb_histogram_for_methodsABC(run_name_simple, run_name_simple_methodC, 
    #         output_postfix='simple', width_scaling=3.0, width_scaling_clsb=1.5, num_bins=50, min_bin_width=0.1)

    # compare_nTsplit_CLs_histogram_for_sample(run_name_simple, use_method_C=False, 
    #         output_postfix='simple', width_scaling=1.6)
    # compare_nTsplit_CLs_histogram_for_sample(run_name_simple_methodC, use_method_C=True, 
    #         output_postfix='simpleMethodC', width_scaling=1.6)
    # compare_nTsplit_CLs_histogram_for_sample(run_name_harder, use_method_C=True, 
    #         output_postfix='harder', width_scaling=1.6)

    nTsplit_limitRatio_boxplot_for_sample(run_name_simple, use_method_C=False, output_postfix='simple', ylim_override=(0,3.0))
    nTsplit_limitRatio_boxplot_for_sample(run_name_simple_methodC, use_method_C=True, output_postfix='simple_method_C', ylim_override=(0,3.0))
    nTsplit_limitRatio_boxplot_for_sample(run_name_harder, use_method_C=True, output_postfix='harder', ylim_override=(0,3.0))

    # compare_MM_fake_rates(run_name_simple, run_name_simple_methodC)
    # fix_MM_information(run_name_simple, run_name_simple_methodC, run_name_simple+'_fixed')


def compare_overall_CLsb_boxplot_for_sample():
    plt.clf()
    plt.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.1)
    runs = _load_pickle_jar(run_name)
    mm_limits = np.sort([run.ul95CLsbToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs])
    data = [mm_limits, nll_limits]
    positions = [0.8, 1.2]

    bp = plt.boxplot(data, positions=positions)
    plt.setp(bp['boxes'], color='black')
    plt.setp(bp['whiskers'], color='black', ls='solid')
    plt.setp(bp['fliers'], color='red', marker='+')

    ylim = list(plt.gca().get_ylim())
    ylim[0] = -0.05
    plt.gca().set_ylim(ylim)
    plt.ylabel('95% $CL_{s+b}$ upper limit', fontsize=16)

    plt.gca().set_xticklabels(['Method A', 'Method B'])

    # Plot truth rate
    xmax = plt.gca().get_xlim()[1]
    plt.hlines(_find_truth_rate(runs), 0, xmax, linestyle='dashed', color='blue')

    # Plot 95% overlay
    index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    y_overlays = [x[index_for_95percent] for x in data]
    plt.plot(positions, y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    plt.savefig(os.path.join(output_dir, 'boxplot_clsb_comparison.pdf'))


def compare_nTsplit_CLsb_boxplot_for_sample(run_name, use_method_C, output_postfix):
    plt.clf()
    plt.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.1)
    runs = _load_pickle_jar(run_name)

    last_nT = 6
    data = []
    positions = []
    base_positions = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True
        mm_limits = np.sort([run.ul95CLsbToysMM for run in runs if acceptable(run)])
        nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs if acceptable(run)])
        data.append(mm_limits)
        data.append(nll_limits)
        base_position = nT + 1
        base_positions.append(base_position)
        positions += [base_position-0.2, base_position+0.2]
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT" (i.e. total) entry
    base_position = base_positions[-1]+1.5
    base_positions.append(base_position)
    positions += [base_position-0.2, base_position+0.2]
    xticklabels.append('Any')
    mm_limits = np.sort([run.ul95CLsbToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs])
    data.append(mm_limits)
    data.append(nll_limits)


    bp = plt.boxplot(data, positions=positions, widths=0.38, patch_artist=True, whis=1.5)
    plt.setp(bp['medians'], color='purple')
    plt.setp(bp['boxes'], color='black')
    plt.setp(bp['whiskers'], color='black', ls='solid')
    plt.setp(bp['fliers'], color='red', marker='+')
    is_method_A = True
    for box in bp['boxes']:
        if is_method_A:
            box.set(color='black', facecolor='forestgreen')
        else:
            if use_method_C:
                box.set(color='black', facecolor='gold')
            else:
                box.set(color='black', facecolor='crimson')
        is_method_A = not is_method_A

    # ylim = list(plt.gca().get_ylim())
    # ylim[0] = -0.05
    # plt.gca().set_ylim(ylim)
    plt.xlabel('Events in signal region, $n_T$', fontsize=16)
    plt.ylabel('95% $CL_{s+b}$ upper limit', fontsize=16)

    plt.gca().set_xticks(base_positions)
    plt.gca().set_xticklabels(xticklabels)

    # Plot truth rate
    xmax = plt.gca().get_xlim()[1]
    plt.hlines(_find_truth_rate(runs), 0, xmax, linestyle='dashed', color='blue')

    # Separate out total
    ymax = plt.gca().get_ylim()[1]
    plt.vlines(0.5*sum(base_positions[-2:]), 0, ymax, linestyle='solid', color='black', linewidth=2)

    # Legend
    if use_method_C:
        plt.legend((bp['boxes'][0], bp['boxes'][1]), ('Method A', 'Method C'), loc='upper left')
    else:
        plt.legend((bp['boxes'][0], bp['boxes'][1]), ('Method A', 'Method B'), loc='upper left')

    # Plot 95% overlay for "any" column
    index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    y_overlays = [x[index_for_95percent] for x in data[-2:]]
    plt.plot(positions[-2:], y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    plt.savefig(os.path.join(output_dir, 'boxplot_clsb_comparison_ntsplit_{}.pdf'.format(output_postfix)))

def compare_nTsplit_fakeRate_boxplot_for_sample(run_name, use_method_C, output_postfix, ylim_override=None):
    plt.clf()
    plt.gcf().set_size_inches(8,4)
    plt.subplots_adjust(left=0.08, right=0.99, top=0.95, bottom=0.15)
    runs = _load_pickle_jar(run_name)

    last_nT = 6
    data = []
    positions = []
    base_positions = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True
        mm_limits = np.sort([run.rateFTMM for run in runs if acceptable(run)])
        nll_limits = np.sort([run.rateFTLikelihood for run in runs if acceptable(run)])
        data.append(mm_limits)
        data.append(nll_limits)
        base_position = nT + 1
        base_positions.append(base_position)
        positions += [base_position-0.2, base_position+0.2]
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT" (i.e. total) entry
    base_position = base_positions[-1]+1.5
    base_positions.append(base_position)
    positions += [base_position-0.2, base_position+0.2]
    xticklabels.append('Any')
    mm_limits = np.sort([run.rateFTMM for run in runs])
    nll_limits = np.sort([run.rateFTLikelihood for run in runs])
    data.append(mm_limits)
    data.append(nll_limits)


    bp = plt.boxplot(data, positions=positions, widths=0.38, patch_artist=True, whis=1.5)
    plt.setp(bp['medians'], color='purple')
    plt.setp(bp['boxes'], color='black')
    plt.setp(bp['whiskers'], color='black', ls='solid')
    plt.setp(bp['fliers'], color='black', marker='.', markersize=3.0)
    is_method_A = True
    for box in bp['boxes']:
        if is_method_A:
            box.set(color='black', facecolor='forestgreen')
        else:
            box.set(color='black', facecolor='gold', edgecolor='crimson', linewidth=2)
        is_method_A = not is_method_A

    # ylim = list(plt.gca().get_ylim())
    # ylim[0] = -0.05
    # plt.gca().set_ylim(ylim)
    plt.xlabel('Events in signal region, $n_T$', fontsize=16)
    plt.ylabel('Estimated fake rate, $\\nu_{TF}$', fontsize=16)

    plt.gca().set_xticks(base_positions)
    plt.gca().set_xticklabels(xticklabels)

    # Plot truth rate
    xmax = plt.gca().get_xlim()[1]
    plt.hlines(0, 0, xmax, linestyle='solid', color='red', linewidth=2)
    plt.hlines(_find_fake_truth_rate(runs), 0, xmax, linestyle='dashed', color='blue')

    # Legend
    plt.legend((bp['boxes'][0], bp['boxes'][1]), ('Method A', 'Method B or C'), loc='upper left', framealpha=0.9)

    # Separate out total
    ymin,ymax = plt.gca().get_ylim()
    plt.vlines(0.5*sum(base_positions[-2:]), ymin, ymax, linestyle='solid', color='black', linewidth=2)
    plt.axis('tight')

    # Override ylim if specified
    if ylim_override is not None:
        plt.gca().set_ylim(ylim_override)

    plt.savefig(os.path.join(output_dir, 'boxplot_fakerate_comparison_ntsplit_{}.pdf'.format(output_postfix)))

def nTsplit_limitRatio_boxplot_for_sample(run_name, use_method_C, output_postfix, ylim_override=None):
    plt.clf()
    plt.gcf().set_size_inches(8,4)
    plt.subplots_adjust(left=0.08, right=0.99, top=0.95, bottom=0.13)
    runs = _load_pickle_jar(run_name)

    last_nT = 6
    data = []
    positions = []
    base_positions = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True
        # TODO - something more sensible for infinite ratios!
        limit_ratios_clsb = np.sort([(run.ul95CLsbToysMM / run.ul95CLsbToysLikelihood) for run in runs if acceptable(run) and run.ul95CLsbToysLikelihood != 0])
        limit_ratios_cls = np.sort([(run.ul95CLsToysMM / run.ul95CLsToysLikelihood) for run in runs if acceptable(run) and run.ul95CLsToysLikelihood != 0])
        data.append(limit_ratios_clsb)
        data.append(limit_ratios_cls)
        base_position = nT + 1
        base_positions.append(base_position)
        positions += [base_position-0.2, base_position+0.2]
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT" (i.e. total) entry
    base_position = base_positions[-1]+1.5
    base_positions.append(base_position)
    positions += [base_position-0.2, base_position+0.2]
    xticklabels.append('Any')
    limit_ratios_clsb = np.sort([(run.ul95CLsbToysMM / run.ul95CLsbToysLikelihood) for run in runs if run.ul95CLsbToysLikelihood != 0])
    limit_ratios_cls = np.sort([(run.ul95CLsToysMM / run.ul95CLsToysLikelihood) for run in runs if run.ul95CLsToysLikelihood != 0])
    data.append(limit_ratios_clsb)
    data.append(limit_ratios_cls)


    bp = plt.boxplot(data, positions=positions, widths=0.38, patch_artist=True, whis=1.5)
    plt.setp(bp['medians'], color='purple')
    plt.setp(bp['boxes'], color='black')
    plt.setp(bp['whiskers'], color='black', ls='solid')
    plt.setp(bp['fliers'], color='black', marker='.', markersize=3.0)
    is_method_A = True
    for box in bp['boxes']:
        if is_method_A:
            box.set(color='black', facecolor='darkorange')
        else:
            box.set(color='black', facecolor='limegreen')
        is_method_A = not is_method_A

    plt.xlabel('Events in signal region, $n_T$', fontsize=16)
    if use_method_C:
        plt.ylabel('Limit ratio Method A / Method C', fontsize=16)
    else:
        plt.ylabel('Limit ratio Method A / Method B', fontsize=16)

    plt.gca().set_xticks(base_positions)
    plt.gca().set_xticklabels(xticklabels)

    # Plot ratio == 1
    xmax = plt.gca().get_xlim()[1]
    plt.hlines(1, 0, xmax, linestyle='dashed', color='blue')

    # Legend
    plt.legend((bp['boxes'][0], bp['boxes'][1]), ('$CL_{s+b}$', '$CL_s$'), loc='upper left', framealpha=0.9)

    # Separate out total
    ymin,ymax = plt.gca().get_ylim()
    plt.vlines(0.5*sum(base_positions[-2:]), ymin, ymax, linestyle='solid', color='black', linewidth=2)
    plt.axis('tight')

    # Override ylim if specified
    if ylim_override is not None:
        plt.gca().set_ylim(ylim_override)

    plt.savefig(os.path.join(output_dir, 'boxplot_limitratio_ntsplit_{}.pdf'.format(output_postfix)))


def compare_overall_CLsb_CDF_for_sample():
    plt.clf()
    runs = _load_pickle_jar(run_name)
    mm_limits = []
    nll_limits = []
    for run in runs:
        mm_limits.append(run.ul95CLsbToysMM)
        nll_limits.append(run.ul95CLsbToysLikelihood)

    mm_limits_sorted = np.sort(mm_limits)
    nll_limits_sorted = np.sort(nll_limits)
    yvals_mm_limits = np.arange(len(mm_limits_sorted)) / float(len(mm_limits_sorted))
    yvals_nll_limits = np.arange(len(nll_limits_sorted)) / float(len(nll_limits_sorted))
    plt.plot(mm_limits_sorted, yvals_mm_limits, 'k', label='Method A')
    plt.plot(nll_limits_sorted, yvals_nll_limits, 'r', label='MethodB')
    plt.xlabel('95% $CL_{s+b}$ upper limit', fontsize=16)
    plt.ylabel('Cumulative frequency', fontsize=16)
    plt.legend(('Method A', 'Method B'))
    plt.vlines(_find_truth_rate(runs), 0.001, 1, linestyle='dashed', color='blue')
    xmax = plt.gca().get_xlim()[1]
    plt.hlines(0.05, 0, xmax, linestyle='dashed', color='purple')
    # plt.semilogy()
    # plt.show()
    plt.savefig(os.path.join(output_dir, 'cdf_clsb_comparison.pdf'))


def compare_nTsplit_CLsb_CDF_for_sample():
    plt.clf()
    runs = _load_pickle_jar(run_name)
    last_nT = 6
    for nT in range(last_nT+1):
        mm_limits = []
        nll_limits = []
        for run in runs:
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                continue
            mm_limits.append(run.ul95CLsbToysMM)
            nll_limits.append(run.ul95CLsbToysLikelihood)
        mm_limits_sorted = np.sort(mm_limits)
        nll_limits_sorted = np.sort(nll_limits)
        yvals_mm_limits = np.arange(len(mm_limits_sorted)) / float(len(mm_limits_sorted))
        yvals_nll_limits = np.arange(len(nll_limits_sorted)) / float(len(nll_limits_sorted))
        plt.plot(mm_limits_sorted, yvals_mm_limits, 'k', label='Method A')
        plt.plot(nll_limits_sorted, yvals_nll_limits, 'r', label='MethodB')

    plt.xlabel('95% $CL_{s+b}$ upper limit', fontsize=16)
    plt.ylabel('Cumulative frequency', fontsize=16)

    plt.legend(('Method A', 'Method B'))
    # plt.vlines(_find_truth_rate(runs), 0.001, 1, linestyle='dashed', color='blue')
    # xmax = plt.gca().get_xlim()[1]
    # plt.hlines(0.05, 0, xmax, linestyle='dashed', color='purple')
    # plt.semilogy()
    # plt.show()
    plt.savefig(os.path.join(output_dir, 'cdf_clsb_comparison_ntsplit.pdf'))


def compare_overall_CLsb_histogram_for_sample(run_name, use_method_C, output_postfix):
    plt.clf()
    plt.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.1)
    runs = _load_pickle_jar(run_name)

    mm_limits = np.sort([run.ul95CLsbToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs])

    fig = plt.figure()

    # Have both histograms share one axis.
    orientation = 'horizontal'
    ax_overall = fig.add_subplot(1,1,1)
    _make_axis_label_only(ax_overall)
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2, sharey=ax1)
    plt.setp(ax2.get_yticklabels(), visible=False)

    # Disable lower labels
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    # Variable width bins
    num_bins = 15
    mm_bins = _get_variable_width_binning(mm_limits, num_bins)
    nll_bins = _get_variable_width_binning(nll_limits, num_bins)

    common_options = {
            # 'bins': 40, 
            'orientation': orientation, 
            'normed': True, 
            'range': (0, max(mm_limits[-1], nll_limits[-1])),
            'histtype': 'stepfilled',
            }
    _,_,h1_patches = ax1.hist(mm_limits, bins=mm_bins, color='forestgreen', label='Method A', **common_options)
    h1 = h1_patches[0]
    if use_method_C:
        _,_,h2_patches = ax2.hist(nll_limits, bins=nll_bins, color='gold', label='Method C', **common_options)
    else:
        _,_,h2_patches = ax2.hist(nll_limits, bins=nll_bins, color='crimson', label='Method B', **common_options)
    h2 = h2_patches[0]
    ax2.legend((h1, h2), (h1.get_label(), h2.get_label()), loc='upper right')

    # Set equal range
    xmax = max(ax1.get_xlim()[1], ax2.get_xlim()[1])
    ax1.set_xlim(ax1.get_xlim()[0], xmax)
    ax2.set_xlim(ax2.get_xlim()[0], xmax)

    # Flip the ax2 histogram vertically.
    ax1.set_xlim(ax1.get_xlim()[::-1])

    # Tighten up the layout.    
    plt.subplots_adjust(wspace=0.0, hspace=0.0)

    # # Plot 95% overlay for "any" column
    # index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    # y_overlays = [x[index_for_95percent] for x in data[-2:]]
    # plt.plot(positions[-2:], y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    ax_overall.set_xlabel('Probability distribution function', fontsize=16)
    ax_overall.set_ylabel('95% $CL_{s+b}$ upper limit', fontsize=16)
    plt.savefig(os.path.join(output_dir, 'histogram_clsb_comparison_{}.pdf'.format(output_postfix)))



def compare_nTsplit_CLsb_histogram_for_sample(run_name, use_method_C, output_postfix, width_scaling=1.0, num_bins=None, min_bin_width=None):
    plt.clf()
    runs = _load_pickle_jar(run_name)

    last_nT = 6
    data = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True
        mm_limits = np.sort([run.ul95CLsbToysMM for run in runs if acceptable(run)])
        nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs if acceptable(run)])
        data.append(mm_limits)
        data.append(nll_limits)
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT" (i.e. total) entry
    xticklabels.append('Any (CLsb)')
    mm_limits = np.sort([run.ul95CLsbToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsbToysLikelihood for run in runs])
    data.append(mm_limits)
    data.append(nll_limits)

    # Add "any nT CLs" (i.e. total for CLs) entry
    xticklabels.append('Any (CLs)')
    mm_limits = np.sort([run.ul95CLsToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsToysLikelihood for run in runs])
    data.append(mm_limits)
    data.append(nll_limits)


    fig = plt.figure()
    fig.set_size_inches(8, 5)

    # Have both histograms share one axis.
    ax_overall = fig.add_subplot(1,1,1, axisbg='none', zorder=len(data)+1)
    _make_axis_label_only(ax_overall)
    axes = []
    axes_including_blanks = []
    blank_indices = [-1, -2, -5, -6, -7, -10]
    vertical_divider_locations = [-6]
    num_subplots = len(data)+len(blank_indices)
    for i in range(num_subplots):
        if i > 0:
            new_axis = fig.add_subplot(1, num_subplots, i+1, sharey=axes[0], axisbg='none', zorder=len(data)-i)
        else:
            new_axis = fig.add_subplot(1, num_subplots, 1, axisbg='none', zorder=len(data))
        new_axis.spines['left'].set_color('none')
        new_axis.spines['right'].set_color('none')
        new_axis.tick_params(top='off', bottom='off', left='off', right='off')
        axes_including_blanks.append(new_axis)
        if not any([i == num_subplots+x for x in blank_indices]):
            axes.append(new_axis)

    # Restore left and right splines
    axes_including_blanks[0].spines['left'].set_color('black')
    axes_including_blanks[0].tick_params(left='on')
    axes_including_blanks[-1].spines['right'].set_color('black')
    axes_including_blanks[-1].tick_params(right='on')

    # Disable y labels except for leftmost 
    for axis in axes_including_blanks[1:]:
        plt.setp(axis.get_yticklabels(), visible=False)
    # Disable x labels
    for axis in axes_including_blanks:
        plt.setp(axis.get_xticklabels(), visible=False)

    # Variable width bins
    if num_bins is None:
        num_bins = 16
    if min_bin_width is None:
        min_bin_width = 0.2
    common_options = {
            'orientation': 'horizontal', 
            'histtype': 'stepfilled',
            }

    is_mm = True
    max_top_edge = 0 
    for axis,datum in zip(axes,data):
        bins = _get_variable_width_binning(datum, num_bins, min_bin_width)
        max_top_edge = max(max_top_edge, bins[-1])
        weights = _weights_for_normalising(bins, datum, target_integral=len(datum))
        # weights = _weights_for_normalising(bins, datum)
        # alpha = math.pow(len(datum) / float(len(data[-1])), 1/6.0)
        alpha = 0.9
        # if len(datum) == len(data[-1]):
        #     alpha = 1.0
        # else:
        #     alpha = 0.9
        is_CLs = (
                axes.index(axis) == (len(axes)-1) or
                axes.index(axis) == (len(axes)-2))
        if is_CLs:
            common_options['hatch'] = '//'

        if is_mm:
            _,_,h1_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('forestgreen', alpha), label='Method A', **common_options)
            if not is_CLs:
                h1 = h1_patches[0]
        elif not is_mm and use_method_C:
            _,_,h2_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('gold', alpha), label='Method C', **common_options)
            if not is_CLs:
                h2 = h2_patches[0]
        else:
            _,_,h2_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('crimson', alpha), label='Method B', **common_options)
            if not is_CLs:
                h2 = h2_patches[0]

        # Important!! Toggle between MM and NLL plots
        is_mm = not is_mm

    axes[0].legend((h1, h2), (h1.get_label(), h2.get_label()), loc='upper left')


    # Set equal range
    xmax = (1.0/width_scaling)*max([axis.get_xlim()[1] for axis in axes])
    ymax = math.ceil(max_top_edge)
    for axis in axes_including_blanks:
        axis.set_xlim(0, xmax)
        axis.set_ylim(0, ymax)
    
    # # Set equal range within each pair
    # for index,mm_axis in list(enumerate(axes))[::2]:
    #     nll_axis = axes[index+1]
    #     # FIXME arbitrary scaling factor
    #     xmax = 0.5*max([axis.get_xlim()[1] for axis in [mm_axis, nll_axis]])
    #     mm_axis.set_xlim(0, xmax)
    #     nll_axis.set_xlim(0, xmax)

    # Fix overall axis to have same yrange
    ax_overall.set_ylim(axes[0].get_ylim())

    # Flip all matrix method histograms vertically.
    for axis in axes[::2]:
        axis.set_xlim(axis.get_xlim()[::-1])

    # # Plot 95% overlay for "any" column
    # index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    # y_overlays = [x[index_for_95percent] for x in data[-2:]]
    # plt.plot(positions[-2:], y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    ax_overall.set_xlabel('Events in signal region, $n_T$', fontsize=16)
    ax_overall.set_ylabel('95% $CL_{s+b}$ or $CL_{s}$ upper limit', fontsize=16)

    # Render nT bin labels
    for label,axis in zip(xticklabels, axes[::2]):
        axis.text(1.0, -0.04, label, horizontalalignment='center', transform=axis.transAxes)

    # Render vertical lines
    for i,vertical_divider_location in enumerate(vertical_divider_locations):
        # WARNING! Hack to pick up CLsb/CLs dividing line
        if i == 0:
            linewidth = 4
        else:
            linewidth = 3
        axes_including_blanks[vertical_divider_location].vlines(0, 0, ymax, 
                linestyle='solid', color='black', linewidth=linewidth)

    # Add coverage info
    coverages,coverage_uncs = _get_coverage_info(runs)
    for i,coverage in enumerate(coverages):
        coverage_unc = coverage_uncs[i]
        axis = axes[i-4]
        is_MM = ((i % 2) == 0)
        if is_MM:
            xcoord = 0.0
        else:
            xcoord = 0.5
        axis.text(xcoord, 0.98, 'coverage: ${0:.1f}\pm{1:.2f}\%$'.format(100*coverage, 100*coverage_unc), 
                horizontalalignment='left', verticalalignment='top',
                transform=axis.transAxes, rotation='vertical')

    # Plot truth rate
    xmax = ax_overall.get_xlim()[1]
    ax_overall.hlines(_find_truth_rate(runs), 0, xmax, linestyle='dashed', color='blue')

    # Tighten up the layout.    
    plt.subplots_adjust(wspace=0.0, hspace=0.0)

    # Adjust margins
    fig.subplots_adjust(left=0.08, right=0.99, top=0.95, bottom=0.12)

    plt.savefig(os.path.join(output_dir, 'histogram_clsb_comparison_ntsplit_{}.pdf'.format(output_postfix)))

def compare_nTsplit_CLsb_histogram_for_methodsABC(run_name_B, run_name_C, output_postfix, width_scaling=1.0, width_scaling_clsb=None, num_bins=None, min_bin_width=None):
    plt.clf()
    runs_B,runs_C = _load_pickle_jar(run_name_B, run_name_C)

    last_nT = 6
    data = []
    data_methodC = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True

        def both_acceptable(run1, run2):
            return (acceptable(run1) and acceptable(run2))

        limits_A = np.sort([run_B.ul95CLsbToysMM for run_B,run_C in zip(runs_B,runs_C) if both_acceptable(run_B, run_C)])
        limits_B = np.sort([run_B.ul95CLsbToysLikelihood for run_B,run_C in zip(runs_B,runs_C) if both_acceptable(run_B, run_C)])
        limits_C = np.sort([run_C.ul95CLsbToysLikelihood for run_B,run_C in zip(runs_B,runs_C) if both_acceptable(run_B, run_C)])
        data.append(limits_A)
        data.append(limits_B)
        data_methodC.append(limits_C)
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT" (i.e. total) entry
    xticklabels.append('Any (CLsb)')
    limits_A = np.sort([run.ul95CLsbToysMM for run in runs_B])
    limits_B = np.sort([run.ul95CLsbToysLikelihood for run in runs_B])
    limits_C = np.sort([run.ul95CLsbToysLikelihood for run in runs_C])
    data.append(limits_A)
    data.append(limits_B)
    data_methodC.append(limits_C)

    # Add "any nT CLs" (i.e. total for CLs) entry
    xticklabels.append('Any (CLs)')
    limits_A = np.sort([run.ul95CLsToysMM for run in runs_B])
    limits_B = np.sort([run.ul95CLsToysLikelihood for run in runs_B])
    limits_C = np.sort([run.ul95CLsToysLikelihood for run in runs_C])
    data.append(limits_A)
    data.append(limits_B)
    data_methodC.append(limits_C)


    fig = plt.figure()
    fig.set_size_inches(8, 5)

    # Have both histograms share one axis.
    ax_overall = fig.add_subplot(1,1,1, axisbg='none', zorder=len(data)+1)
    _make_axis_label_only(ax_overall)
    axes = []
    axes_including_blanks = []
    blank_indices = [-1, -2, -5, -6, -7, -10]
    vertical_divider_locations = [-6]
    num_subplots = len(data)+len(blank_indices)
    for i in range(num_subplots):
        if i > 0:
            new_axis = fig.add_subplot(1, num_subplots, i+1, sharey=axes[0], axisbg='none', zorder=len(data)-i)
        else:
            new_axis = fig.add_subplot(1, num_subplots, 1, axisbg='none', zorder=len(data))
        new_axis.spines['left'].set_color('none')
        new_axis.spines['right'].set_color('none')
        new_axis.tick_params(top='off', bottom='off', left='off', right='off')
        axes_including_blanks.append(new_axis)
        if not any([i == num_subplots+x for x in blank_indices]):
            axes.append(new_axis)

    # Restore left and right splines
    axes_including_blanks[0].spines['left'].set_color('black')
    axes_including_blanks[0].tick_params(left='on')
    axes_including_blanks[-1].spines['right'].set_color('black')
    axes_including_blanks[-1].tick_params(right='on')

    # Disable y labels except for leftmost 
    for axis in axes_including_blanks[1:]:
        plt.setp(axis.get_yticklabels(), visible=False)
    # Disable x labels
    for axis in axes_including_blanks:
        plt.setp(axis.get_xticklabels(), visible=False)

    # Variable width bins
    if num_bins is None:
        num_bins = 16
    if min_bin_width is None:
        min_bin_width = 0.2
    common_options = {
            'orientation': 'horizontal', 
            'histtype': 'stepfilled',
            }

    i = 0
    is_mm = True
    max_top_edge = 0 
    for axis,datum in zip(axes,data):
        bins = _get_variable_width_binning(datum, num_bins, min_bin_width)
        max_top_edge = max(max_top_edge, bins[-1])
        weights = _weights_for_normalising(bins, datum, target_integral=len(datum))
        if not is_mm:
            datum_C = data_methodC[i]
            bins_C = _get_variable_width_binning(datum_C, num_bins, min_bin_width)
            max_top_edge = max(max_top_edge, bins_C[-1])
            weights_C = _weights_for_normalising(bins_C, datum_C, target_integral=len(datum))
        # weights = _weights_for_normalising(bins, datum)
        # alpha = math.pow(len(datum) / float(len(data[-1])), 1/6.0)
        alpha = 0.9
        # if len(datum) == len(data[-1]):
        #     alpha = 1.0
        # else:
        #     alpha = 0.9
        is_CLs = (
                axes.index(axis) == (len(axes)-1) or
                axes.index(axis) == (len(axes)-2))
        if is_CLs:
            common_options['hatch'] = '//'

        if is_mm:
            _,_,h1_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('forestgreen', alpha), label='Method A', **common_options)
            if not is_CLs:
                h1 = h1_patches[0]
        else:
            _,_,h3_patches = axis.hist(datum_C, bins=bins_C, weights=weights_C, clip_on=False, color=_color_name_and_alpha('gold', alpha), label='Method C', **common_options)
            _,_,h2_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('crimson', 0.0), edgecolor=_color_name_and_alpha('crimson', alpha), linewidth=2, label='Method B', **common_options)
            if not is_CLs:
                h2 = h2_patches[0]
                h3 = h3_patches[0]
            # Want next method C datum for next time
            i += 1

        # Important!! Toggle between MM and NLL plots
        is_mm = not is_mm

    axes[0].legend((h1, h2, h3), (h1.get_label(), h2.get_label(), h3.get_label()), loc='upper left')


    # Set equal range
    xmax = (1.0/width_scaling)*max([axis.get_xlim()[1] for axis in axes])
    ymax = math.ceil(max_top_edge)
    for axis in axes_including_blanks:
        axis.set_xlim(0, xmax)
        axis.set_ylim(0, ymax)

    # Separately scale clsb if desired
    if not width_scaling_clsb is None:
        xmax = (1.0/width_scaling_clsb)*max([axis.get_xlim()[1] for axis in axes[:-2]])
        ymax = math.ceil(max_top_edge)
        for axis in axes[:-2]:
            axis.set_xlim(0, xmax)
            axis.set_ylim(0, ymax)

    
    # # Set equal range within each pair
    # for index,mm_axis in list(enumerate(axes))[::2]:
    #     nll_axis = axes[index+1]
    #     # FIXME arbitrary scaling factor
    #     xmax = 0.5*max([axis.get_xlim()[1] for axis in [mm_axis, nll_axis]])
    #     mm_axis.set_xlim(0, xmax)
    #     nll_axis.set_xlim(0, xmax)

    # Fix overall axis to have same yrange
    ax_overall.set_ylim(axes[0].get_ylim())

    # Flip all matrix method histograms vertically.
    for axis in axes[::2]:
        axis.set_xlim(axis.get_xlim()[::-1])

    # # Plot 95% overlay for "any" column
    # index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    # y_overlays = [x[index_for_95percent] for x in data[-2:]]
    # plt.plot(positions[-2:], y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    ax_overall.set_xlabel('Events in signal region, $n_T$', fontsize=16)
    ax_overall.set_ylabel('95% $CL_{s+b}$ or $CL_{s}$ upper limit', fontsize=16)

    # Render nT bin labels
    for label,axis in zip(xticklabels, axes[::2]):
        axis.text(1.0, -0.04, label, horizontalalignment='center', transform=axis.transAxes)

    # Render vertical lines
    for i,vertical_divider_location in enumerate(vertical_divider_locations):
        # WARNING! Hack to pick up CLsb/CLs dividing line
        if i == 0:
            linewidth = 4
        else:
            linewidth = 3
        axes_including_blanks[vertical_divider_location].vlines(0, 0, ymax, 
                linestyle='solid', color='black', linewidth=linewidth)

    # Add coverage info
    coverages_B,coverage_uncs_B = _get_coverage_info(runs_B)
    coverages_C,coverage_uncs_C = _get_coverage_info(runs_C)
    for i,coverage in enumerate(coverages_B):
        coverage_unc = coverage_uncs_B[i]
        axis = axes[i-4]
        is_MM = ((i % 2) == 0)
        if is_MM:
            xcoord = 0.0
            color = 'black'
        else:
            xcoord = 0.5
            color = _color_name_and_alpha('crimson', alpha)
        axis.text(xcoord, 0.98, 'coverage: ${0:.1f}\pm{1:.2f}\%$'.format(100*coverage, 100*coverage_unc), 
                horizontalalignment='left', verticalalignment='top', color=color,
                transform=axis.transAxes, rotation='vertical')
        should_add_coverage_C = not is_MM
        if should_add_coverage_C:
            coverage = coverages_C[i]
            coverage_unc = coverage_uncs_C[i]
            axis.text(1.2, 0.98, 'coverage: ${0:.1f}\pm{1:.2f}\%$'.format(100*coverage, 100*coverage_unc), 
                    horizontalalignment='left', verticalalignment='top', color='black',
                    transform=axis.transAxes, rotation='vertical')

    # Plot truth rate
    xmax = ax_overall.get_xlim()[1]
    ax_overall.hlines(_find_truth_rate(runs_B), 0, xmax, linestyle='dashed', color='blue')

    # Tighten up the layout.    
    plt.subplots_adjust(wspace=0.0, hspace=0.0)

    # Adjust margins
    fig.subplots_adjust(left=0.08, right=0.99, top=0.95, bottom=0.12)

    plt.savefig(os.path.join(output_dir, 'histogram_clsb_comparison_ntsplit_methodsABC_{}.pdf'.format(output_postfix)))

def compare_nTsplit_CLs_histogram_for_sample(run_name, use_method_C, output_postfix, width_scaling=1.0):
    plt.clf()
    runs = _load_pickle_jar(run_name)

    last_nT = 6
    data = []
    xticklabels = []
    for nT in range(last_nT+1):
        def acceptable(run):
            run_nT = run.nRT+run.nFT
            if (run_nT != nT and nT < last_nT) or (run_nT < nT and nT == last_nT):
                return False
            return True
        mm_limits = np.sort([run.ul95CLsToysMM for run in runs if acceptable(run)])
        nll_limits = np.sort([run.ul95CLsToysLikelihood for run in runs if acceptable(run)])
        data.append(mm_limits)
        data.append(nll_limits)
        if nT < last_nT:
            xticklabels.append(r'${}$'.format(nT))
        else:
            xticklabels.append(r'$\geq{}$'.format(last_nT))

    # Add "any nT CLs" (i.e. total for CLs) entry
    xticklabels.append('Any')
    mm_limits = np.sort([run.ul95CLsToysMM for run in runs])
    nll_limits = np.sort([run.ul95CLsToysLikelihood for run in runs])
    data.append(mm_limits)
    data.append(nll_limits)


    fig = plt.figure()
    fig.set_size_inches(8, 5)

    # Have both histograms share one axis.
    ax_overall = fig.add_subplot(1,1,1, axisbg='none', zorder=len(data)+1)
    _make_axis_label_only(ax_overall)
    axes = []
    axes_including_blanks = []
    blank_indices = [-1, -4]
    vertical_divider_locations = [-4]
    num_subplots = len(data)+len(blank_indices)
    for i in range(num_subplots):
        if i > 0:
            new_axis = fig.add_subplot(1, num_subplots, i+1, sharey=axes[0], axisbg='none', zorder=len(data)-i)
        else:
            new_axis = fig.add_subplot(1, num_subplots, 1, axisbg='none', zorder=len(data))
        new_axis.spines['left'].set_color('none')
        new_axis.spines['right'].set_color('none')
        new_axis.tick_params(top='off', bottom='off', left='off', right='off')
        axes_including_blanks.append(new_axis)
        if not any([i == num_subplots+x for x in blank_indices]):
            axes.append(new_axis)

    # Restore left and right splines
    axes_including_blanks[0].spines['left'].set_color('black')
    axes_including_blanks[0].tick_params(left='on')
    axes_including_blanks[-1].spines['right'].set_color('black')
    axes_including_blanks[-1].tick_params(right='on')

    # Disable y labels except for leftmost 
    for axis in axes_including_blanks[1:]:
        plt.setp(axis.get_yticklabels(), visible=False)
    # Disable x labels
    for axis in axes_including_blanks:
        plt.setp(axis.get_xticklabels(), visible=False)

    # Variable width bins
    num_bins = 16
    min_bin_width = 0.05
    common_options = {
            'orientation': 'horizontal', 
            'histtype': 'stepfilled',
            }

    is_mm = True
    max_top_edge = 0 
    for axis,datum in zip(axes,data):
        bins = _get_variable_width_binning(datum, num_bins, min_bin_width)
        max_top_edge = max(max_top_edge, bins[-1])
        weights = _weights_for_normalising(bins, datum, target_integral=len(datum))
        # weights = _weights_for_normalising(bins, datum)
        # alpha = math.pow(len(datum) / float(len(data[-1])), 1/6.0)
        alpha = 0.9
        # if len(datum) == len(data[-1]):
        #     alpha = 1.0
        # else:
        #     alpha = 0.9
        is_CLs = (
                axes.index(axis) == (len(axes)-1) or
                axes.index(axis) == (len(axes)-2))
        if is_CLs:
            common_options['hatch'] = '//'

        if is_mm:
            _,_,h1_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('forestgreen', alpha), label='Method A', **common_options)
            if not is_CLs:
                h1 = h1_patches[0]
        elif not is_mm and use_method_C:
            _,_,h2_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('gold', alpha), label='Method C', **common_options)
            if not is_CLs:
                h2 = h2_patches[0]
        else:
            _,_,h2_patches = axis.hist(datum, bins=bins, weights=weights, clip_on=False, color=_color_name_and_alpha('crimson', alpha), label='Method B', **common_options)
            if not is_CLs:
                h2 = h2_patches[0]

        # Important!! Toggle between MM and NLL plots
        is_mm = not is_mm

    axes[0].legend((h1, h2), (h1.get_label(), h2.get_label()), loc='upper left')


    # Set equal range
    xmax = (1.0/width_scaling)*max([axis.get_xlim()[1] for axis in axes])
    ymax = math.ceil(max_top_edge)
    for axis in axes_including_blanks:
        axis.set_xlim(0, xmax)
        axis.set_ylim(0, ymax)
    
    # # Set equal range within each pair
    # for index,mm_axis in list(enumerate(axes))[::2]:
    #     nll_axis = axes[index+1]
    #     # FIXME arbitrary scaling factor
    #     xmax = 0.5*max([axis.get_xlim()[1] for axis in [mm_axis, nll_axis]])
    #     mm_axis.set_xlim(0, xmax)
    #     nll_axis.set_xlim(0, xmax)

    # Fix overall axis to have same yrange
    ax_overall.set_ylim(axes[0].get_ylim())

    # Flip all matrix method histograms vertically.
    for axis in axes[::2]:
        axis.set_xlim(axis.get_xlim()[::-1])

    # Tighten up the layout.    
    plt.subplots_adjust(wspace=0.0, hspace=0.0)

    # Adjust margins
    fig.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.12)

    # # Plot 95% overlay for "any" column
    # index_for_95percent = int(math.floor(0.05 * len(mm_limits)))
    # y_overlays = [x[index_for_95percent] for x in data[-2:]]
    # plt.plot(positions[-2:], y_overlays, color='blue', marker='*', ls='points', markeredgecolor='blue')

    ax_overall.set_xlabel('Events in signal region, $n_T$', fontsize=16)
    ax_overall.set_ylabel('95% $CL_{s}$ upper limit', fontsize=16)

    # Render nT bin labels
    for label,axis in zip(xticklabels, axes[::2]):
        axis.text(1.0, -0.04, label, horizontalalignment='center', transform=axis.transAxes)

    # Render vertical lines
    for i,vertical_divider_location in enumerate(vertical_divider_locations):
        linewidth = 3
        axes_including_blanks[vertical_divider_location].vlines(0, 0, ymax, 
                linestyle='solid', color='black', linewidth=linewidth)

    # Plot truth rate
    xmax = ax_overall.get_xlim()[1]
    ax_overall.hlines(_find_truth_rate(runs), 0, xmax, linestyle='dashed', color='blue')

    plt.savefig(os.path.join(output_dir, 'histogram_cls_comparison_ntsplit_{}.pdf'.format(output_postfix)))



def _get_variable_width_binning(sorted_data, num_bins, min_bin_width=None):
    num_elements = len(sorted_data)
    elements_per_bin = int(math.floor(num_elements / num_bins))
    bins = [sorted_data[0]]
    count = 0
    for datum in sorted_data:
        count +=1
        if min_bin_width is not None and (datum - bins[-1]) < min_bin_width:
            continue
        if count >= elements_per_bin and datum != bins[-1]:
            bins.append(datum)
            count = 0
    last_datum = sorted_data[-1]
    if bins[-1] != last_datum:
        if (last_datum - bins[-1]) < min_bin_width:
            bins[-1] = last_datum
        else:
            bins.append(sorted_data[-1])
    return bins


def _weights_for_normalising(bin_edges, sorted_data_points, target_integral=1.0):
    widths = np.diff(bin_edges)
    num_data_points = len(sorted_data_points)
    bin_weights = target_integral / (widths * num_data_points)
    weights = []
    for val in sorted_data_points:
        bin_index = 0
        for i,upper_boundary in enumerate(bin_edges[1:]):
            if val > upper_boundary:
                bin_index = i+1
            else:
                break
        weights.append(bin_weights[bin_index])
    return weights


def _make_axis_label_only(ax):
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')


def _color_name_and_alpha(color_name, alpha=1.0):
   converter = mpcolors.ColorConverter()
   return converter.to_rgba(color_name, alpha)



def compare_MM_fake_rates(run_name1, run_name2):
    run1 = _load_pickle_jar(run_name1)
    run2 = _load_pickle_jar(run_name2)
    i = 0; j = 0
    while True:
        try:
            x = run1[i]; y = run2[j]
        except IndexError:
            break
        if x._seed != y._seed:
            if x._seed < y._seed:
                i += 1
            else:
                j += 1
            continue
        print x._seed, y._seed
        print x.rateFTMM , y.rateFTMM 
        print x.rateFTLikelihood, y.rateFTLikelihood
        print
        i += 1
        j += 1


def fix_MM_information(bad_name, good_name, fixed_name):
    runs_good = _load_pickle_jar(good_name, trim_bad_runs=False)
    runs_bad = _load_pickle_jar(bad_name, trim_bad_runs=False)

    runs_fixed = []
    from copy import deepcopy
    for run_good,run_bad in zip(runs_good,runs_bad):
        run_fixed = deepcopy(run_bad)
        for prop_name in run_good.__dict__:
            if not 'MM' in prop_name: continue
            setattr(run_fixed, prop_name, getattr(run_good, prop_name))
        runs_fixed.append(run_fixed)

    output_file = open(fixed_name+'.pickle', 'w')
    import pickle
    pickle.dump(runs_fixed, output_file, protocol=2)
    output_file.close()
    print 'Written fixed pickle file!'


def _load_pickle_jar(run_name, run_name_2=None, trim_bad_runs=True):
    filename = '{}.pickle'.format(run_name)
    import pickle
    inputFile = open(filename)
    runs = pickle.load(inputFile)
    inputFile.close()
    have_second_run = (not run_name_2 is None)
    if have_second_run:
        filename = '{}.pickle'.format(run_name_2)
        inputFile = open(filename)
        runs_2 = pickle.load(inputFile)
        inputFile.close()

    if not trim_bad_runs:
        if have_second_run:
            return (runs, runs_2)
        else:
            return runs

    # Remove dodgy runs
    if have_second_run:
        runs_trimmed,runs_2_trimmed = zip(*[(a,b) for a,b in zip(runs,runs_2) if
                hasattr(a, 'ul95CLsbToysLikelihood') and
                hasattr(a, 'ul95CLsToysLikelihood') and
                hasattr(b, 'ul95CLsbToysLikelihood') and
                hasattr(b, 'ul95CLsToysLikelihood')
                ])
        print 'Runs removed for dodginess:',len(runs)-len(runs_trimmed)
        return (runs_trimmed,runs_2_trimmed)
    else:
        runs_trimmed = [x for x in runs if 
                hasattr(x, 'ul95CLsbToysLikelihood') and
                hasattr(x, 'ul95CLsToysLikelihood') ]
        print 'Runs removed for dodginess:',len(runs)-len(runs_trimmed)
        return runs_trimmed


def _find_truth_rate(runs):
    if hasattr(runs[0], 'rateSignalRTTruth') and runs[0].rateSignalRTTruth > 0:
        rate = runs[0].rateSignalRTTruth
    else:
        rate = runs[0].rateRTTruth
    return rate


def _find_fake_truth_rate(runs):
    return runs[0].rateFTTruth


def _get_coverage_info(runs):
    attributes = ['ul95CLsbToysMM', 'ul95CLsbToysLikelihood', 
            'ul95CLsToysMM', 'ul95CLsToysLikelihood']

    totals = [0]*len(attributes)
    totals_covering = [0]*len(attributes)

    truth_rate = _find_truth_rate(runs) 
    for run in runs:
        from math import isnan
        if isnan(run.ul95CLsMM):
            print 'MOOOO', run.ul95CLsLikelihood
            continue
        if isnan(run.ul95CLsLikelihood):
            print 'BAAAA'
            continue

        for i,attr in enumerate(attributes):
            totals[i] += 1
            if getattr(run, attr) > truth_rate:
                totals_covering[i] += 1

    a = np.array(totals_covering).astype(float)
    b = np.array(totals).astype(float)
    coverages = a / b
    coverage_uncs = (a / b**2) * (1 + a/b)
    return coverages,coverage_uncs


if __name__ == '__main__':
    main()

