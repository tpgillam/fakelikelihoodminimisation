Fake likelihood minimisation
============================

Herein lies code based on Minuit that computes (profiled) likelihoods for the
purpose of determining CLs limits on real & tight rates.

To build, do something like

```bash
mkdir build; cd build
cmake ..
make
```
